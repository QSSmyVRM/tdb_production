<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.	
*
* You should have received a copy of the TrueDaybook license with	
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.en_MemberAllStatus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
  <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Group Details</title>
</head>
<body>
    <form id="form1" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div style="position:static">
    <table align="center">
     <%--Window Dressing--%>
    <tr> <td align="center"><h3><asp:Label ID="GroupName" runat="server" ></asp:Label></h3> </td></tr>
        <tr>
         <td align="center">
           <asp:DataGrid ID="dgViewDetails" ShowHeader="true" Width="100%" CellPadding="5" CellSpacing="0"  BorderStyle="None" BorderWidth="0" runat="server" AutoGenerateColumns="false"> <%--Edited for FF--%>
             <ItemStyle CssClass="tableBody"  />
             <HeaderStyle CssClass="tableHeader" Height="30px" />
            <AlternatingItemStyle HorizontalAlign="left" VerticalAlign="Top" />
            <Columns>
                <asp:BoundColumn DataField="userID" Visible="false"></asp:BoundColumn>
     <%--Window Dressing--%>
               <asp:TemplateColumn HeaderText = "<%$ Resources:WebResources, MemberName%>" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass ="tableHeader" ItemStyle-HorizontalAlign="Left">
                   <ItemTemplate>
                   <asp:Label runat="server" ID="lblUserName"></asp:Label>
                   </ItemTemplate>
                   <ItemTemplate>
                   <asp:Label runat="server" ID="lblUserName" Text='<%# DataBinder.Eval(Container, "DataItem.userFirstName") + "&nbsp;" + DataBinder.Eval(Container, "DataItem.userLastName") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateColumn>
     <%--Window Dressing--%>
                 <asp:BoundColumn ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="tableBody"  DataField="userEmail" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, MemberEmail%>"></asp:BoundColumn>
             </Columns>
        </asp:DataGrid>
         </td>
        </tr>
        <tr>
        <td>
          <center>
        <%--code changed for Soft Edge button--%><%--ZD 100420--%>
        <button id="btnClose" runat="server" onClick='window.close()' class="altLongBlueButtonFormat">
        <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, CloseWindow%>" runat="server"></asp:Literal></button>
        <%--<input type="button" onfocus="this.blur()" name="closewindow" value="Close Window" class="altLongBlueButtonFormat " onClick='window.close()'/>--%>
      </center>
        </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }
</script>
<%--ZD 100428 END--%>