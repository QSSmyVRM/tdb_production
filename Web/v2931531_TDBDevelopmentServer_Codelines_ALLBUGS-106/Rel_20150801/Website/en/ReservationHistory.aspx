<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.
*
* You should have received a copy of the TrueDaybook license with
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%>
<%--ZD 100147 End--%>

<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" Inherits="ReservationHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2050--%>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
    <%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2.Export, Version=10.2.11.0, Culture=neutral,PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<script type="text/javascript">
    
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    function DataLoading(val) 
    {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block';
        else
            document.getElementById("dataLoadingDIV").style.display = 'none';
    }

    function fnCancel() {
        DataLoading(1);
        window.location.replace('SuperAdministrator.aspx');
    }

    function setHiddenValue() {
        document.getElementById('hdnValue').value = '1';
        return true;
    }
    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ReservationHistory</title>
</head>
<body>
    <form id="form1" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input type="hidden" id="hdnValue" runat="server" />
    <table width="100%" border="0"  cellpadding="0" cellspacing="0" >
        <tr>
            <td colspan="2" align="center" height="10px" style="border-bottom-style:none;" >
                <h3 >
                    <asp:Label ID="lblHeading" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ReservationHistory%>" runat="server"></asp:Literal></asp:Label>
                </h3>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-top-style:none;" >
                <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display: none">
                    <img border='0' src='image/wait1.gif' alt='Loading..' />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <table width="90%" align="center">
                    <tr>
                        <td align="center" colspan="5">
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label><br />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td width="7%" class="blackblodtext" nowrap="nowrap">
                            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageUserProfile_MeetingID%>" runat="server"></asp:Literal>
                        </td>
                        <td width="15%">
                            <asp:TextBox ID="ConferenceID" runat="server" CssClass="altText"  MaxLength="9" Width="150px" onkeypress="return (event.keyCode!=13)"  ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="ConferenceID" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="reg1" runat="server" ControlToValidate="ConferenceID" ValidationGroup="Submit" ErrorMessage="<%$Resources:WebResources, Numericonly%>" ValidationExpression="\d+" Display="dynamic"></asp:RegularExpressionValidator>
                        </td>
                        <td width="10%"> 
                            <button id="btnView" runat="server" class="altMedium0BlueButtonFormat" onserverclick="btnOk_Click" type="button"
                            onclick="setHiddenValue();" validationgroup="Submit" ><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, UserHistoryReport_btnSearch%>" runat="server"></asp:Literal></button>
                        </td>
                        <td width="10%">
                            <button id="btnCancel" runat="server" class="altMedium0BlueButtonFormat" type="button"  onclick="javascript:fnCancel();return false;">
                            <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button>
                        </td>
                        <td align="right">
                            <asp:ImageButton ID="btnExcel" ValidationGroup="group" src="image/excel.gif" runat="server"
                            AlternateText="Export to Excel" Style="vertical-align: middle;" OnClick="ExportExcel" ToolTip="<%$ Resources:WebResources, ExporttoExcel%>" />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="90%" border="0" align="center">
                    <tr>
                        <td align="center">
                            <div style="overflow-y: auto; overflow-x: auto; word-break: break-all; height: 500px;">
                                <dx:ASPxGridView ID="MainGrid" ClientInstanceName="MainGrid" Width="100%" runat="server"
                                    EnableCallBacks="false" KeyFieldName="Confid">
                                    <Styles >
                                        <Header ImageSpacing="9px" SortingImageSpacing="9px" Font-Size="9pt"
                                            HorizontalAlign="Center">
                                        </Header>
                                        <Cell Font-Size="10pt" HorizontalAlign="Left">
                                        </Cell>
                                    </Styles>
                                    <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoData%>" HeaderFilterShowAll="false" GroupPanel="Arrastre un encabezamiento de columna aqu� para agrupar por dicha columna"/>
                                    <Settings ShowFilterRow="false" ShowGroupPanel="false" ShowHeaderFilterButton="false"
                                        ShowTitlePanel="True" ShowHeaderFilterBlankItems="False" />
                                    <SettingsPager ShowDefaultImages="False" Mode="ShowPager" AlwaysShowPager="true"
                                        Position="Top" Summary-Text="<%$ Resources:WebResources, GridViewPageText%>">
                                        <AllButton Text="<%$ Resources:WebResources, All%>">
                                        </AllButton>
                                        <NextPageButton Text="<%$ Resources:WebResources, Next1%>">
                                        </NextPageButton>
                                        <PrevPageButton Text="<%$ Resources:WebResources, Prev1%>">
                                        </PrevPageButton>
                                        
                                    </SettingsPager>                                    
                                </dx:ASPxGridView>
                            <dx:ASPxGridViewExporter ID="gridExport" OnRenderBrick="GridExporter_OnRenderBrick" runat="server" GridViewID="MainGrid"></dx:ASPxGridViewExporter>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
<script type="text/javascript">

    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };
</script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
