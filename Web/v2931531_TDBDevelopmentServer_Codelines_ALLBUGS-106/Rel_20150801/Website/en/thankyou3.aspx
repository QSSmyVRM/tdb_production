<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.		
*
* You should have received a copy of the TrueDaybook license with	
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<html>
<head>
<title>Thank you</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0" background="image/background.gif">
  <br>
  <center>
	<br><br><br>
	<font size="4">
      <p>Permission denied to login twice from same account simeltanously.<br>
      Please try login after sometime.</p><br><br>
      <p>Probable reasons for this message could be :<br><br>
      <li><font color="red">The account is already active currently.</font><br>
      <li><font color="red">The session with webserver has been abruptly discontinued or disconnected.</font></p>
      <br><br>
      <p><font size="2">For technical support contact TrueDaybook at <%=Application["contactPhone"]%>.</font></p>
	</font>
  </center>
</body>
</html>