<!--ZD 100147 Start-->
<!--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.
*
* You should have received a copy of the TrueDaybook license with
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/-->
<!--ZD 100147 ZD 100886 End-->
<!DOCTYPE html>

<html>
<head>
<!--  #INCLUDE FILE="BrowserDetect.aspx"  -->
  <title>TrueDaybook</title>
  <meta name="Description" content="TrueDaybook (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment." />
  <meta name="Keywords" content="TrueDaybook, TrueDaybook, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints" />
  

  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="App_Themes/CSS/main.css" /> <%-- FB 1830 - Translation Menu--%>
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="App_Themes/CSS/buttons.css" />
  <script type="text/javascript" src="../script/errorList.js"></script>
</head>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">
</body>

</html>