/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9414.3.7  Starts(2nd Dec 2014)           */
/*                                                                                              */
/* ******************************************************************************************** */

/* **************************** ZD 102356 - 10 Dec 2014  Starts ************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableCalDefaultDisplay int NULL
GO
COMMIT


update Org_Settings_D set EnableCalDefaultDisplay = 2

/* **************************** ZD 102356 - 10 Dec 2014  Ends  ************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9414.3.7  Ends(19th Dec 2014)            */
/*                              Features & Bugs for V2.9414.3.8  Starts(22nd Dec 2014)          */
/*                                                                                              */
/* ******************************************************************************************** */

/* **************************** ZD 102600 - 26 Dec 2014  Starts  ************************** */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Conf_Conference_D ADD
	isRPRMConf smallint NULL
GO
ALTER TABLE dbo.Audit_Conf_Conference_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isRPRMConf smallint NULL
GO
ALTER TABLE dbo.Conf_Conference_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* **************************** ZD 102600 - 26 Dec 2014  Ends  ************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9414.3.8  Ends(30th Dec 2014)            */
/*                              Features & Bugs for V2.9414.3.9  Starts(31st Dec 2014)          */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9414.3.9  Ends(07th Jan 2015)            */
/*                              Features & Bugs for V2.9115.3.0  Starts(07th Jan 2015)          */

/*                              Features & Bugs for V2.9115.3.0  End(10th Jan 2015)          */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9115.3.1  Starts(10th Jan 2015)          */
/*                                                                                              */
/* ******************************************************************************************** */


--ZD 102713
update Loc_Room_D set Name = REPLACE(Name, '&' , ' ') where Name like '%&%' 

update Loc_Room_D set Name = REPLACE(Name, '?' , '') where Name like '%?%'

update Loc_Room_D set Name = REPLACE(Name, '>' , '') where Name like '%>%'

update Loc_Room_D set Name = REPLACE(Name, '<' , '') where Name like '%<%'

update Loc_Room_D set Name = REPLACE(Name, ',' , '') where Name like '%,%'

update Ept_List_D set Name = REPLACE(Name, '&' , ' '), profileName = REPLACE(profileName, '&' , ' ') where Name like '%&%' or profileName like '%&%'

update Ept_List_D set Name = REPLACE(Name, '?' , ''), profileName = REPLACE(profileName, '?' , '')  where Name like '%?%' or profileName like '%?%' 

update Ept_List_D set Name = REPLACE(Name, '>' , ''), profileName = REPLACE(profileName, '>' , '')  where Name like '%>%' or profileName like '%>%' 

update Ept_List_D set Name = REPLACE(Name, '<' , ''), profileName = REPLACE(profileName, '<' , '')  where Name like '%<%' or profileName like '%<%' 

update Ept_List_D set Name = REPLACE(Name, ',' , ''), profileName = REPLACE(profileName, ',' , '')  where Name like '%,%' or profileName like '%,%' 


/* **************************** ZD 101835 / 102738 - 13 Jan 2015   Starts  ************************** */


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Conf_Archive_Settings_D](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ArchiveConfOlderThan] [int] NOT NULL,
	[ArchivePeriod] [int] NOT NULL,
	[ArchiveTime] [datetime] NOT NULL,
	[UserID] [int] NOT NULL,
	[OrgID] [int] NOT NULL,
	[LastRunDate] [datetime] NULL,
	[ConfigurationDate] [datetime] NULL
) ON [PRIMARY]

GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Conf_ArchiveDetails_D](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ArchiveID] [int] NOT NULL,
	[ConfCount] [int] NULL,
	[RunDate] [datetime] NULL
) ON [PRIMARY]

GO

-- To insert default values for All Orgs
Declare  @orgid int

Declare confArchiveCursor Cursor for
select orgId from Org_List_D where deleted = 0

open confArchiveCursor 
Fetch Next from confArchiveCursor into @orgid
WHILE @@FETCH_STATUS = 0
BEGIN
	
insert into Conf_Archive_Settings_D(ArchiveConfOlderThan, ArchivePeriod, ArchiveTime, UserID,  OrgID,LastRunDate, ConfigurationDate) values
(3,2, CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 112)), 11,@orgid,GETDATE(), GETDATE())

FETCH NEXT FROM confArchiveCursor INTO @orgid
End
close confArchiveCursor


CREATE TABLE [dbo].[Archive_Conf_AdvAVParams_D](
	[ConfID] [int] NOT NULL,
	[InstanceID] [int] NOT NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NOT NULL,
	[lineRateID] [smallint] NULL,
	[audioAlgorithmID] [smallint] NULL,
	[videoProtocolID] [smallint] NULL,
	[mediaID] [smallint] NULL,
	[videoLayoutID] [int] NULL,
	[dualStreamModeID] [smallint] NULL,
	[conferenceOnPort] [smallint] NULL,
	[encryption] [smallint] NULL,
	[maxAudioParticipants] [int] NULL,
	[maxVideoParticipants] [int] NULL,
	[videoSession] [int] NULL,
	[LectureMode] [smallint] NULL,
	[videoMode] [int] NULL,
	[singleDialin] [int] NULL,
	[internalBridge] [nvarchar](50) NULL,
	[externalBridge] [nvarchar](50) NULL,
	[feccMode] [smallint] NULL,
	[Layout] [int] NULL,
	[MuteRxaudio] [int] NULL,
	[MuteTxaudio] [int] NULL,
	[MuteRxvideo] [int] NULL,
	[MuteTxvideo] [int] NULL,
	[ConfLockUnlock] [int] NULL,
	[Recording] [int] NULL,
	[Camera] [int] NULL,
	[PacketLoss] [nvarchar](max) NULL,
	[PolycomSendEmail] [int] NULL,
	[PolycomTemplate] [nvarchar](max) NULL
)
GO


CREATE TABLE [dbo].[Archive_Conf_Approval_D](
	[uId] [int] NOT NULL,
	[confid] [int] NOT NULL,
	[instanceid] [int] NOT NULL,
	[entitytype] [smallint] NOT NULL,
	[entityid] [int] NOT NULL,
	[approverid] [int] NULL,
	[decision] [smallint] NOT NULL,
	[responsetimestamp] [datetime] NULL,
	[responsemessage] [nvarchar](2000) NULL
) 

GO

CREATE TABLE [dbo].[Archive_Conf_Bridge_D](
	[ConfID] [bigint] NOT NULL,
	[InstanceID] [int] NOT NULL,
	[BridgeID] [int] NOT NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NULL,
	[BridgeName] [nvarchar](250) NULL,
	[BridgeTypeid] [int] NULL,
	[BridgeIPISDNAddress] [nvarchar](250) NULL,
	[TotalPortsUsed] [int] NULL,
	[BridgeExtNo] [nvarchar](50) NOT NULL,
	[E164Dialnumber] [nvarchar](250) NULL,
	[Synchronous] [int] NULL,
	[RPRMUserid] [int] NULL,
	[CDRFetchStatus] [smallint] NULL,
	[ConfUIDonMCU] [nvarchar](max) NULL,
	[ConfRMXServiceID] [int] NULL,
	SystemLocation int NULL,
	VideoLayoutId int NULL,
	FamilyLayout int NULL
)

GO

CREATE TABLE [dbo].[Archive_Conf_Cascade_D](
	[uId] [int] NOT NULL,
	[confid] [int] NOT NULL,
	[instanceid] [int] NOT NULL,
	[cascadelinkid] [int] NOT NULL,
	[cascadelinkname] [nvarchar](500) NULL,
	[masterorslave] [smallint] NULL,
	[defvideoprotocol] [smallint] NULL,
	[connectiontype] [smallint] NULL,
	[ipisdnaddress] [nvarchar](256) NULL,
	[bridgeid] [int] NULL,
	[bridgeipisdnaddress] [nvarchar](256) NULL,
	[connectstatus] [smallint] NULL,
	[outsidenetwork] [smallint] NULL,
	[mcuservicename] [nvarchar](256) NULL,
	[audioorvideo] [smallint] NULL,
	[deflinerate] [int] NULL,
	[mute] [smallint] NULL,
	[addresstype] [smallint] NULL,
	[bridgeaddresstype] [smallint] NULL,
	[layout] [int] NULL,
	[prefix] [nvarchar](50) NULL,
	[isLecturer] [int] NOT NULL,
	[OnlineStatus] [int] NULL,
	[LastRunDateTime] [datetime] NULL,
	[remoteEndPointIP] [nvarchar](50) NULL,
	[GUID] [nvarchar](max) NULL,
	[RxAudioPacketsReceived] [nvarchar](50) NULL,
	[RxAudioPacketErrors] [nvarchar](50) NULL,
	[RxAudioPacketsMissing] [nvarchar](50) NULL,
	[RxVideoPacketsReceived] [nvarchar](50) NULL,
	[RxVideoPacketErrors] [nvarchar](50) NULL,
	[RxVideoPacketsMissing] [nvarchar](50) NULL,
	[TerminalType] [smallint] NULL,
	[ChairPerson] [smallint] NULL,
	[TxAudioPacketsSent] [nvarchar](50) NULL,
	[TxVideoPacketsSent] [nvarchar](50) NULL
)

CREATE TABLE [dbo].[Archive_Conf_Conference_D](
	[userid] [int] NULL,
	[confid] [int] NOT NULL,
	[externalname] [nvarchar](256) NULL,
	[internalname] [nvarchar](300) NULL,
	[password] [nvarchar](256) NULL,
	[owner] [int] NULL,
	[confdate] [datetime] NULL,
	[conftime] [datetime] NULL,
	[timezone] [int] NULL,
	[immediate] [int] NULL,
	[audio] [int] NULL,
	[videoprotocol] [int] NULL,
	[videosession] [int] NULL,
	[linerate] [int] NULL,
	[recuring] [int] NULL,
	[duration] [int] NULL,
	[description] [nvarchar](2000) NULL,
	[public] [int] NULL,
	[deleted] [smallint] NULL,
	[continous] [int] NULL,
	[transcoding] [int] NULL,
	[deletereason] [nvarchar](2000) NULL,
	[instanceid] [int] NOT NULL,
	[advanced] [int] NULL,
	[totalpoints] [int] NULL,
	[connect2] [int] NULL,
	[settingtime] [datetime] NULL,
	[videolayout] [int] NULL,
	[manualvideolayout] [int] NULL,
	[conftype] [int] NULL,
	[confnumname] [int] NOT NULL,
	[status] [int] NULL,
	[lecturemode] [int] NULL,
	[lecturer] [nvarchar](256) NULL,
	[dynamicinvite] [int] NULL,
	[CreateType] [int] NULL,
	[ConfDeptID] [int] NULL,
	[ConfOrigin] [int] NOT NULL,
	[SetupTime] [datetime] NULL,
	[TearDownTime] [datetime] NULL,
	[orgId] [int] NULL,
	[LastRunDateTime] [datetime] NULL,
	[IcalID] [varchar](max) NULL,
	[confMode] [smallint] NULL,
	[isDedicatedEngineer] [smallint] NULL,
	[isLiveAssistant] [smallint] NULL,
	[isVIP] [smallint] NULL,
	[isReminder] [int] NULL,
	[ServiceType] [smallint] NULL,
	[ConceirgeSupport] [nvarchar](50) NULL,
	[sentSurvey] [int] NOT NULL,
	[isVMR] [smallint] NULL,
	[ESId] [nvarchar](50) NULL,
	[ESType] [nvarchar](50) NULL,
	[PushedToExternal] [int] NULL,
	[isTextMsg] [int] NULL,
	[startmode] [int] NULL,
	[loginUser] [int] NULL,
	[GUID] [nvarchar](max) NULL,
	[OnSiteAVSupport] [smallint] NULL,
	[MeetandGreet] [smallint] NULL,
	[ConciergeMonitoring] [smallint] NULL,
	[DedicatedVNOCOperator] [smallint] NULL,
	[E164Dialing] [int] NULL,
	[H323Dialing] [int] NULL,
	[Secured] [int] NULL,
	[NetworkSwitch] [int] NULL,
	[DialString] [nvarchar](50) NULL,
	[Etag] [nvarchar](50) NULL,
	[HostName] [nvarchar](250) NULL,
	[isPCconference] [smallint] NULL,
	[pcVendorId] [smallint] NULL,
	[CloudConferencing] [smallint] NULL,
	[ProcessStatus] [smallint] NULL,
	[Uniqueid] [int] NULL,
	[Seats] [int] NULL,
	[EnableNumericID] [smallint] NULL,
	[McuSetupTime] [int] NULL,
	[MCUTeardonwnTime] [int] NULL,
	[SetupTimeinMin] [int] NULL,
	[TearDownTimeinMin] [int] NULL,
	[WebExConf] [smallint] NULL,
	[WebExPW] [nvarchar](50) NULL,
	[WebExMeetingKey] [nvarchar](100) NULL,
	[WebExHostURL] [nvarchar](max) NULL
	)

CREATE TABLE [dbo].[Archive_Conf_CustomAttr_D](
	[ConfId] [int] NOT NULL,
	[InstanceId] [int] NOT NULL,
	[CustomAttributeId] [int] NOT NULL,
	[SelectedOptionId] [int] NOT NULL,
	[SelectedValue] [nvarchar](4000) NULL,
	[ConfAttrID] [int] NOT NULL
)
GO

CREATE TABLE [dbo].[Archive_Conf_Message_D](
	[uID] [int] NOT NULL,
	[orgid] [int] NOT NULL,
	[confid] [int] NOT NULL,
	[instanceid] [int] NOT NULL,
	[Languageid] [int] NULL,
	[confMessage] [nvarchar](max) NOT NULL,
	[duration] [int] NOT NULL,
	[controlID] [int] NOT NULL,
	[durationID] [nvarchar](10) NULL
) 
GO

CREATE TABLE [dbo].[Archive_Conf_RecurInfo_D](
	[confid] [int] NOT NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NOT NULL,
	[timezoneid] [int] NULL,
	[duration] [int] NULL,
	[recurType] [int] NULL,
	[subType] [int] NULL,
	[yearMonth] [int] NULL,
	[days] [nvarchar](256) NULL,
	[dayNo] [int] NULL,
	[gap] [int] NULL,
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[endType] [int] NULL,
	[occurrence] [int] NULL,
	[dirty] [smallint] NULL,
	[RecurringPattern] [nvarchar](4000) NULL
) 
GO

CREATE TABLE [dbo].[Archive_Conf_RecurInfoDefunt_D](
	[confid] [int] NOT NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NOT NULL,
	[timezoneid] [int] NULL,
	[duration] [int] NULL,
	[recurType] [int] NULL,
	[subType] [int] NULL,
	[yearMonth] [int] NULL,
	[days] [nvarchar](256) NULL,
	[dayNo] [int] NULL,
	[gap] [int] NULL,
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[endType] [int] NULL,
	[occurrence] [int] NULL,
	[dirty] [smallint] NULL,
	[RecurringPattern] [nvarchar](4000) NULL,
	[SetupDuration] [int] NULL,
	[TeardownDuration] [int] NULL
) 
GO

CREATE TABLE [dbo].[Archive_Conf_Room_D](
	[ConfID] [int] NOT NULL,
	[RoomID] [int] NOT NULL,
	[DefLineRate] [int] NULL,
	[DefVideoProtocol] [int] NULL,
	[StartDate] [datetime] NULL,
	[Duration] [int] NULL,
	[instanceID] [int] NOT NULL,
	[connect2] [smallint] NULL,
	[bridgeIPISDNAddress] [nvarchar](256) NULL,
	[bridgeid] [int] NULL,
	[connectiontype] [smallint] NULL,
	[ipisdnaddress] [nvarchar](256) NULL,
	[connectstatus] [smallint] NULL,
	[outsidenetwork] [smallint] NULL,
	[mcuservicename] [nvarchar](4000) NULL,
	[audioorvideo] [smallint] NULL,
	[mute] [smallint] NULL,
	[addresstype] [smallint] NULL,
	[bridgeaddresstype] [smallint] NULL,
	[layout] [int] NULL,
	[endpointId] [int] NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NOT NULL,
	[profileId] [int] NULL,
	[prefix] [nvarchar](50) NULL,
	[isLecturer] [int] NOT NULL,
	[OnlineStatus] [int] NULL,
	[LastRunDateTime] [datetime] NULL,
	[ApiPortNo] [int] NULL,
	[endptURL] [nvarchar](50) NULL,
	[remoteEndPointIP] [nvarchar](50) NULL,
	[disabled] [smallint] NULL,
	[MultiCodecAddress] [nvarchar](2000) NULL,
	[Extroom] [int] NULL,
	[isTextMsg] [int] NULL,
	[GUID] [nvarchar](max) NULL,
	[Setfocus] [int] NULL,
	[Message] [int] NULL,
	[MuteRxaudio] [int] NULL,
	[MuteRxvideo] [int] NULL,
	[MuteTxvideo] [int] NULL,
	[Camera] [int] NULL,
	[Packetloss] [int] NULL,
	[LockUnLock] [int] NULL,
	[Record] [int] NULL,
	[Stream] [nvarchar](max) NULL,
	[RxAudioPacketsReceived] [nvarchar](50) NULL,
	[RxAudioPacketErrors] [nvarchar](50) NULL,
	[RxAudioPacketsMissing] [nvarchar](50) NULL,
	[RxVideoPacketsReceived] [nvarchar](50) NULL,
	[RxVideoPacketErrors] [nvarchar](50) NULL,
	[RxVideoPacketsMissing] [nvarchar](50) NULL,
	[TerminalType] [smallint] NULL,
	[BridgeExtNo] [nvarchar](50) NOT NULL,
	[Secured] [int] NULL,
	[PartyName] [nvarchar](250) NULL,
	[isDoubleBooking] [smallint] NULL,
	[ChairPerson] [smallint] NULL,
	[GateKeeeperAddress] [nvarchar](50) NULL,
	[activeSpeaker] [int] NULL,
	[TxAudioPacketsSent] [nvarchar](50) NULL,
	[TxVideoPacketsSent] [nvarchar](50) NULL
)
 GO

CREATE TABLE [dbo].[Archive_Conf_User_D](
	[ConfID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[CC] [int] NULL,
	[Status] [int] NULL,
	[Reason] [nvarchar](2000) NULL,
	[ResponseDate] [datetime] NULL,
	[DefLineRate] [int] NULL,
	[Invitee] [int] NULL,
	[RoomID] [int] NULL,
	[DefVideoProtocol] [int] NULL,
	[InstanceId] [int] NOT NULL,
	[IpAddress] [nvarchar](256) NULL,
	[connectionType] [int] NULL,
	[connect2] [smallint] NULL,
	[AudioOrVideo] [smallint] NULL,
	[IPISDN] [smallint] NULL,
	[IPISDNAddress] [nchar](256) NULL,
	[bridgeIPISDNAddress] [nchar](256) NULL,
	[bridgeid] [int] NULL,
	[connectstatus] [smallint] NULL,
	[outsidenetwork] [smallint] NULL,
	[mcuservicename] [nvarchar](256) NULL,
	[emailreminder] [smallint] NULL,
	[mute] [smallint] NULL,
	[addresstype] [smallint] NULL,
	[bridgeaddresstype] [smallint] NULL,
	[layout] [int] NULL,
	[PartyNotify] [smallint] NULL,
	[interfacetype] [smallint] NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NOT NULL,
	[videoEquipment] [int] NULL,
	[prefix] [nvarchar](50) NULL,
	[isLecturer] [int] NOT NULL,
	[ExchangeID] [nvarchar](200) NULL,
	[OnlineStatus] [int] NULL,
	[LastRunDateTime] [datetime] NULL,
	[ApiPortNo] [int] NULL,
	[endptURL] [nvarchar](50) NULL,
	[remoteEndPointIP] [nvarchar](50) NULL,
	[NotifyOnEdit] [smallint] NULL,
	[Survey] [int] NULL,
	[isTextMsg] [int] NOT NULL,
	[GUID] [nvarchar](max) NULL,
	[Setfocus] [int] NULL,
	[Message] [int] NULL,
	[MuteRxaudio] [int] NULL,
	[MuteRxvideo] [int] NULL,
	[MuteTxvideo] [int] NULL,
	[Camera] [int] NULL,
	[Packetloss] [int] NULL,
	[LockUnLock] [int] NULL,
	[Record] [int] NULL,
	[Stream] [nvarchar](max) NULL,
	[RxAudioPacketsReceived] [nvarchar](50) NULL,
	[RxAudioPacketErrors] [nvarchar](50) NULL,
	[RxAudioPacketsMissing] [nvarchar](50) NULL,
	[RxVideoPacketsReceived] [nvarchar](50) NULL,
	[RxVideoPacketErrors] [nvarchar](50) NULL,
	[RxVideoPacketsMissing] [nvarchar](50) NULL,
	[TerminalType] [smallint] NULL,
	[PublicVMRParty] [smallint] NOT NULL,
	[Secured] [int] NULL,
	[PartyName] [nvarchar](250) NULL,
	[ChairPerson] [smallint] NULL,
	[RFIDValue] [nvarchar](250) NULL,
	[Attended] [int] NULL,
	[activeSpeaker] [int] NULL,
	[TxAudioPacketsSent] [nvarchar](50) NULL,
	[TxVideoPacketsSent] [nvarchar](50) NULL,
	[WebEXAttendeeURL] [nvarchar](max) NULL
)
GO

CREATE TABLE [dbo].[Archive_Conf_VNOCOperator_D](
	[uId] [int] NOT NULL,
	[confid] [int] NOT NULL,
	[instanceid] [int] NOT NULL,
	[vnocId] [int] NOT NULL,
	[vnocAssignAdminId] [int] NOT NULL,
	[decision] [smallint] NOT NULL,
	[responsetimestamp] [datetime] NULL,
	[responsemessage] [nvarchar](2000) NULL,
	[confuId] [int] NOT NULL,
	[OperatorName] [nvarchar](250) NULL
)
GO

CREATE TABLE [dbo].[Archive_Inv_WorkOrder_D](
	LastRunDateTime [datetime] NOT NULL,
	[ID] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[CatID] [int] NOT NULL,
	[AdminId] [int] NOT NULL,
	[ConfID] [int] NOT NULL,
	[InstanceID] [int] NOT NULL,
	[Name] [nvarchar](300) NULL,
	[RoomID] [int] NOT NULL,
	[CompletedBy] [datetime] NULL,
	[startBy] [datetime] NULL,
	[woTimeZone] [int] NULL,
	[comment] [nvarchar](2048) NULL,
	[Status] [int] NOT NULL,
	[Reminder] [int] NOT NULL,
	[Notify] [int] NOT NULL,
	[WkOType] [int] NOT NULL,
	[description] [nvarchar](2000) NULL,
	[deliveryType] [int] NULL,
	[deliveryCost] [float] NULL,
	[serviceCharge] [float] NULL,
	[woTax] [float] NULL,
	[woTtlCost] [float] NULL,
	[deleted] [int] NOT NULL,
	[strData] [nvarchar](2000) NULL,
	[orgId] [int] NULL,
	[woTtlDlvryCost] [float] NULL,
	[woTtlSrvceCost] [float] NULL,
	[woCatAdmin] [int] NULL
) 
GO

CREATE TABLE [dbo].[Archive_Inv_WorkItem_D](
	[ID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[WorkOrderID] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[deliveryType] [int] NULL,
	[deliveryCost] [float] NULL,
	[description] [nvarchar](256) NULL,
	[serviceCharge] [float] NULL,
	[deleted] [int] NOT NULL,
	[serviceId] [int] NULL
) 
GO


ALTER TABLE dbo.Archive_Conf_Conference_D ADD
	MeetingId int NULL,
	EnableStaticID int NULL,
	GoogleGUID nvarchar(MAX) NULL,
	GoogleSequence int NULL,
	GoogleConfLastUpdated datetime NULL,
	Permanent smallint NULL,
	PermanentconfName nvarchar(500) NULL,
	VMRPINChange smallint NULL,
	isExpressConference smallint NULL,
	ConfURI nvarchar(MAX) NULL,
	isOBTP tinyint NULL
GO


ALTER TABLE dbo.Archive_Conf_User_D ADD
	PartyNameonMCU nvarchar(MAX) NULL
GO

update Archive_Conf_User_D set PartyNameonMCU=''

ALTER TABLE dbo.Archive_Conf_Room_D ADD
	PartyNameonMCU nvarchar(MAX) NULL,
	ParticipantCode nvarchar(50) NULL
GO

update Archive_Conf_Room_D set PartyNameonMCU=''

ALTER TABLE dbo.Archive_Conf_User_D ADD
	ParticipantCode nvarchar(50) NULL
GO

Update Archive_Conf_User_D set ParticipantCode =''

ALTER TABLE dbo.Archive_Conf_AdvAVParams_D ADD
	Familylayout smallint NULL
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SelectCalendarDetails] 
(
	-- Add the parameters for the stored procedure here
	@timezone			int=null,
	@startTime			datetime=null,
	@endTime			datetime=null,
	@specificEntityCode int=null,
	@codeType			int=null,
	@tablename			varchar(200)=null,
	@dtFormat			varchar(15) = null,
	@tformat			varchar(10)=null
)

As
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@timezone is null)
	begin
		return
	end
	
	if (@codeType is null)
	begin
		return
	end

	/* Temp table for Calendar details */

	CREATE TABLE #TempCalendarDT(
	[Date] datetime,
	Start varchar(50),
	[End] varchar(50), 
	Duration int, 
	Room varchar(500),
	[Meeting Title] varchar(1000),
	[Last Name] varchar(100),
	First varchar(100),
	email varchar(200),
	Phone varchar(50),
	State varchar(100),
	City varchar(100),
	[Conf #] varchar(50), 
	[Meeting Type] varchar(100),
	Category varchar(50),
	Status varchar(50),
	Remarks varchar(2500),
	conftype int,
	connectionType int,
	defVideoProtocol int,
	vidProtocol varchar(20),
	TypeFlag char(1),
	CustomAttributeId int, 
	SelectedOptionId int,
	addresstype int,
	langauge int,
	[Entity Code] varchar(50),
	[Entity Name] varchar(200)
	
)

	declare @ecode as varchar(50)
	declare @optionid as int
	declare @calqry as varchar(4000)
	declare @where as varchar(2000)
	declare @cond as varchar(1000)
	declare @filter as varchar(1000)

	declare @seloptionid as int
	--FB 2045 start	
	declare @syscustattrid as varchar(50)
	SELECT @syscustattrid = CustomAttributeId FROM Dept_CustomAttr_D 
    where CreateType = 1 and DisplayTitle = 'Entity Code'
    --FB 2045 end

	if(@codeType = 1) -- None (Need to identify code)
		begin
			set @seloptionid = ''
			set @ecode = 'NA'			
		end
	else if(@codeType = 2) -- Specific
		begin
			set @seloptionid = @specificEntityCode	
			SELECT top 1 @ecode = Caption FROM Dept_CustomAttr_Option_D where OptionId=@specificEntityCode and Caption is not null
		end
	
	set @filter = ''

	if (@startTime is not null And @endTime is not null)
		begin
			if(@tformat = 'HHmmZ')
				set @filter = @filter + ' AND c.confdate BETWEEN '''+ cast(@startTime as varchar(40)) +''' AND '''+ cast(@endTime as varchar(40)) +''''
			else
				set @filter = @filter + ' AND c.confdate BETWEEN dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +','''+ cast(@startTime as varchar(40)) +''') AND dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +','''+ cast(@endTime as varchar(40)) +''')'
		end

	if (@tablename <> '' AND @tablename is not null)
		begin
			if exists (select * from dbo.sysobjects where [name]=@tablename and xtype='U')
			 begin
				set @filter = @filter +' AND cu.roomid IN (Select roomid from '+ @tablename +')'
			 end
		end

/* Details Part */
	set @calqry = 'Select a.*, isnull(d.[Entity Code],''NA'') as [Entity Code], isnull(d.[Entity Name],''NA'') as [Entity Name] From '
	if(@tformat = 'HHmmZ')
	Begin
		set @calqry = @calqry + '(SELECT c.confdate AS Date,'
	End
	else
	Begin
		set @calqry = @calqry + '(SELECT dbo.changeTime('+ cast(@timezone as varchar(2)) +',c.confdate) AS Date,'
	End
	--FB 2588
	if(@tformat = 'HHmmZ')
	Begin
		set @calqry = @calqry + 'replace(CONVERT(varchar(5),c.confdate,114),'':'','''') + ''Z'' as Start,'
		set @calqry = @calqry + 'replace(CONVERT(varchar(5),dateadd(minute,c.duration, c.confdate),114),'':'','''') + ''Z'' as [End],'
	End
	else if(@tformat = 'HH:mm')
	Begin
		set @calqry = @calqry + 'CONVERT(varchar(5),dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate),108) as Start,'
		set @calqry = @calqry + 'CONVERT(varchar(5),dbo.changeTime('+ cast(@timezone as varchar(2)) +', dateadd(minute,c.duration, c.confdate)),108) as [End],'	
	End
	else
	Begin
		set @calqry = @calqry + 'substring(convert(char(30),dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate) - dateadd(dd,0, datediff(dd,0, dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate)))),13,8) as Start,'
		set @calqry = @calqry + 'substring(convert(char(30),dbo.changeTime('+ cast(@timezone as varchar(2)) +',dateadd(minute,c.duration, c.confdate)) - dateadd(dd,0, datediff(dd,0, dbo.changeTime('+ cast(@timezone as varchar(2)) +',dateadd(minute,c.duration, c.confdate))))),13,8) as [End],'
	End
	set @calqry = @calqry + ' c.Duration, l.name AS Room, c.externalname AS [Meeting Title], u.lastname as [Last Name], '
	set @calqry = @calqry + ' u.firstname as First, u.email,'
	set @calqry = @calqry + ' Case when ((len(isnull(l.AssistantPhone,''''))>0) And (len(isnull(u.Telephone,''''))>0)) Then (l.AssistantPhone + '', '' + u.Telephone) ' 
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))>0) And (len(isnull(u.Telephone,''''))=0)) Then (l.AssistantPhone) '
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))=0) And (len(isnull(u.Telephone,''''))>0)) Then (u.Telephone) ' 
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))=0) And (len(isnull(u.Telephone,''''))=0)) Then ('''')'
	set @calqry = @calqry + ' End   AS Phone ' 
	set @calqry = @calqry + ' ,l3.name AS State, l2.name AS City, '
	set @calqry = @calqry + ' c.confnumname AS [Conf #],'
	set @calqry = @calqry + ' case c.conftype '
	set @calqry = @calqry + ' when 1 then ''Video'' '
	set @calqry = @calqry + ' when 2 then ''AudioVideo'' '
	set @calqry = @calqry + ' when 3 then ''Immediate'' '
	set @calqry = @calqry + ' when 4 then ''Point to Point'' '
	set @calqry = @calqry + ' when 5 then ''Template'' '
	set @calqry = @calqry + ' when 6 then ''AudioOnly'' '
	set @calqry = @calqry + ' when 7 then ''RoomOnly'' '
	set @calqry = @calqry + ' when 8 then ''Hotdesking'' '
	set @calqry = @calqry + ' when 9 then ''OBTP'' '
	set @calqry = @calqry + ' when 11 then ''Phantom'' '
	set @calqry = @calqry + ' else '''''	
	set @calqry = @calqry + ' end AS [Meeting Type],'

/*	set @calqry = @calqry + ' case cu.connectionType'
	set @calqry = @calqry + ' when 0 then ''Outbound'' '
	set @calqry = @calqry + ' else ''Inbound'' '
	set @calqry = @calqry + ' end AS Category,'
*/
	set @calqry = @calqry + ' case cu.connectionType'
	set @calqry = @calqry + ' when 0 then ''Dial-out from MCU'' '
	set @calqry = @calqry + ' when 1 then ''Dial-in to MCU'' '
	set @calqry = @calqry + ' when 3 then ''Direct to MCU'' '
	set @calqry = @calqry + ' end AS Category,'

	set @calqry = @calqry + ' case c.Status '
	set @calqry = @calqry + ' when 0 then ''Confirmed'' '
	set @calqry = @calqry + ' else ''Pending'''
	set @calqry = @calqry + ' end AS ''Status'', c.description  as Remarks'
	set @calqry = @calqry + ',c.conftype,cu.connectionType,cu.defVideoProtocol,'
	
	--set @calqry = @calqry + ' case cu.defVideoProtocol '
	
	set @calqry = @calqry + ' case cu.addresstype '
	set @calqry = @calqry + ' when 4 then ''ISDN'''
	set @calqry = @calqry + ' else ''IP'''
	set @calqry = @calqry + ' end AS vidProtocol, ''D'' as TypeFlag'
	set @calqry = @calqry + ' ,CustomAttributeId, SelectedOptionId, cu.addresstype, u.language'
/* Where Part */

	set @where = ' FROM '
	set @where = @where + ' conf_conference_d c,'
	set @where = @where + ' usr_list_d u, '
	set @where = @where + ' conf_room_d cu, '
	set @where = @where + ' loc_room_d l, '
	set @where = @where + ' loc_tier2_d l2,'
	set @where = @where + ' loc_tier3_d l3,'
	set @where = @where + ' conf_customattr_d ca'
	set @where = @where + ' WHERE (c.confid = cu.confid AND c.instanceid = cu.instanceid)'
	set @where = @where + ' AND (c.deleted = 0)'
	set @where = @where + ' AND c.owner = u.userid'
	set @where = @where + ' AND l.roomid = cu.roomid'
	set @where = @where + ' AND l2.id = l.l2locationid'
	set @where = @where + ' AND l3.id = l.l3locationid'
	set @where = @where + ' AND (c.confid = ca.confid AND c.instanceid = ca.instanceid)'

	if(len(@filter) > 1)
		set @where = @where + @filter

	set @where = @where + ' )a'
	set @where = @where + ' left outer join '
	set @where = @where + ' (Select Caption AS [Entity Code], helpText as [Entity Name], '
	set @where = @where + ' OptionID, CustomAttributeId, languageid from Dept_CustomAttr_Option_D '
	--set @where = @where + ' Where CustomAttributeId=1'
    --set @where = @where + ' Where CustomAttributeId='''+ @syscustattrid +'''' --FB 2045
	--Set @where = @where + ' And OptionType=6'
	--Set @where = @where + ' And DeptId=0'
	--Set @where = @where + ' And OptionValue=0)D '
	set @where = @where + ' )D on a.CustomAttributeId = D.CustomAttributeId'
	set @where = @where + ' AND a.SelectedOptionId=D.OptionID'
	set @where = @where + ' AND a.language = D.Languageid'

	declare @cmdsql as varchar(8000)

	if(@codeType=0) -- All
		Begin
			set @seloptionid = ''
			Declare cursor1 Cursor for
				
				SELECT Distinct Caption AS EntityCode, OptionID as OptionID
				FROM Dept_CustomAttr_Option_D where Caption is not null

			Open cursor1

			Fetch Next from cursor1 into @ecode,@optionid

			While @@Fetch_Status = 0
			Begin
								
				set @cond = ''
				Set @cond = ' Where a.SelectedOptionId ='+ Cast(@optionid as varchar(10))
				
				--Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
				Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' Union All ' + @calqry + replace(replace(replace(@where,'conf_conference_d','Archive_conf_conference_d'),'conf_room_d','Archive_conf_room_d'),'conf_customattr_d','Archive_Conf_CustomAttr_D')  + @cond + ' ORDER BY a.[Date],[Meeting Title], Room'
				--print @cmdsql
				exec (@cmdsql)
				set @cond = ''
				set @cmdsql = ''

				Fetch Next from cursor1 into @ecode,@optionid
			End

			Close cursor1
			Deallocate cursor1

			/* Need to identify the entity codes */
			
			set @cond = ''
			Set @cond = ' Where a.SelectedOptionId ='''' OR a.SelectedOptionId = -1'
						
			--Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
			Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' Union All ' + @calqry + replace(replace(replace(@where,'conf_conference_d','Archive_conf_conference_d'),'conf_room_d','Archive_conf_room_d'),'conf_customattr_d','Archive_Conf_CustomAttr_D') + @cond + ' ORDER BY a.[Date],[Meeting Title], Room'
			--print @cmdsql
			exec (@cmdsql)
			set @cond = ''
			set @cmdsql = ''

		End
	else 
		Begin
			
			set @cond = ''
			if @codetype = 1
				set @cond = ' Where [Entity code] is null or [Entity code] = ''-1'''
			else
				set @cond = ' Where a.SelectedOptionId ='+ cast(@seloptionid as varchar(10))
			
			--Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
			Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' Union All ' + @calqry + replace(replace(replace(@where,'conf_conference_d','Archive_conf_conference_d'),'conf_room_d','Archive_conf_room_d'),'conf_customattr_d','Archive_Conf_CustomAttr_D') + @cond + ' ORDER BY a.[Date],[Meeting Title], Room'
			
			print @cmdsql
			exec (@cmdsql)
			set @cmdsql = ''			
			set @cond = ''
		End

/* ** Updates the duration as per requirement **

conftype = 11 phantom
conftype = 4  point-point
conftype = 7  room only

7. If a conference type is room only then the value posted in the duration field will always be equal to 0
*/
set @cmdsql = @cmdsql + 'Update #TempCalendarDT set Duration=0 where conftype in(7,11) and TypeFlag=''D'''
exec (@cmdsql)
set @cmdsql = ''

/*
8. Due to the method of connectivity by NGC, in a point-to-point defined conference there will be three rooms with the same meeting title, and the same date/time displayed, however,  one of the rooms is a phantom room and should be assigned a duration of 0.
   All point-to-point calls are to be billed as ISDN calls.

 PROGRAMMING NOTE: Any ISDN Room which doesn't belong to State = "ISDN Network" and City = "PacBell PRI's" should have duration as zero (0).
*/
set @cmdsql = @cmdsql + 'Update #TempCalendarDT set Duration=0 where (rtrim(State) <> ''ISDN Network'' AND rtrim(City) <> ''PacBell PRIs'' AND addresstype=4) and TypeFlag=''D'''
exec (@cmdsql)
set @cmdsql = ''

set @calqry = ''

if (@tablename <> '' AND @tablename is not null)
begin
	if exists (select * from dbo.sysobjects where [name]=@tablename and xtype='U')
	 begin
		set @cmdsql = ''
		set @cmdsql = 'drop table '+ @tablename
		exec(@cmdsql)
	 end
end
set @cmdsql = ''

/* Detail */
set @cmdsql = null
--FB 2588
-- ZD 100958
declare @dformat as varchar(10);
if(@dtFormat = 'dd/MM/yyyy')
Begin
	set @dformat = '103';
End
else if(@dtFormat = 'dd MMM yyyy')
Begin
	set @dformat = '106';
End
else
Begin
	set @dformat = '101';
End

set @cmdsql = ' Select ltrim(rtrim(convert(char,[Date],'+ @dformat +'))) as [Date],'

set @cmdsql = @cmdsql +'ltrim(rtrim(Start)) as Start,ltrim(rtrim([End])) as [End],Duration,Room,[Meeting Title],'
set @cmdsql = @cmdsql + ' [Last Name],First,email,Phone,State,City,[Conf #],'
set @cmdsql = @cmdsql + ' [Entity Code],[Entity Name],[Meeting Type] as [Conference Type]'
set @cmdsql = @cmdsql + ' ,vidProtocol as Protocol,Category as [Connection Type],'
set @cmdsql = @cmdsql + ' Status,Remarks from #TempCalendarDT where TypeFlag=''D'';'

/* Consolidated Summary */

set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as Entity_Code, sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql = @cmdsql + ' sum([Connects]) as [Connects],'

set @cmdsql = @cmdsql + ' sum([VOIP Totals]) as [IP(Hours)], '
set @cmdsql = @cmdsql + ' case when( sum([Hours]) > 0 ) Then ((sum([VOIP Totals])/sum([Hours]))*100) '
set @cmdsql = @cmdsql + ' else 0 '
set @cmdsql = @cmdsql + ' End as [IP %],'

set @cmdsql = @cmdsql + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in (Hours)],'

set @cmdsql = @cmdsql + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out (Hours)],'
set @cmdsql = @cmdsql + ' case when( sum([Hours]) > 0 ) Then ((sum([ISDN Outbound Totals])/sum([Hours]))*100)'
set @cmdsql = @cmdsql + ' else 0 '
set @cmdsql = @cmdsql + ' End as [ISDN Dial-out %]'
--ISDN Outbound
set @cmdsql = @cmdsql + ' from '
set @cmdsql = @cmdsql + ' (Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' isnull((sum(isnull(Duration,0))/60),0) as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=0) and  TypeFlag=''D'''
set @cmdsql = @cmdsql + ' group by [Entity Code] '

set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' isnull((sum(isnull(Duration,0))/60),0) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=1) and TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], isnull((sum(isnull(Duration,0))/60),0) as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT where addresstype <> 4 and  TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], count(*) as Connects, 0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT  Where TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT Where TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql = @cmdsql + ' from #TempCalendarDT where TypeFlag=''D'' group by [Entity Code]) d group by [Entity Code];'

/* Entity codewise Summary */

set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql = @cmdsql + ' sum([Pt-Pt Meetings]) as [Point-to-Point Conferences],'
set @cmdsql = @cmdsql + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out connections],'
set @cmdsql = @cmdsql + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in connections],'
set @cmdsql = @cmdsql + ' sum([VOIP Totals]) as [IP Dial-out connections],'
set @cmdsql = @cmdsql + ' sum([Audio-Video Conferences]) as [Audio-Video Conferences],'
set @cmdsql = @cmdsql + ' sum([IP Dial-in connections]) as [IP Dial-in connections],'
set @cmdsql = @cmdsql + ' (sum([Pt-Pt Meetings])+ sum([Audio-Video Conferences])) as [Total Conferences]'
set @cmdsql = @cmdsql + ' from ('
--Pt-Pt Mins
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where ConfType=4 and TypeFlag=''D'' group by [Entity Code]'

--ISDN Outbound Mins
set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=0) and  TypeFlag=''D'' '
set @cmdsql = @cmdsql + ' group by [Entity Code] '

--ISDN Inbound Mins
set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=1) and  TypeFlag=''D'' group by [Entity Code]'

--IP Dial-Out Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], sum(isnull(Duration,0)) as [VOIP Totals],0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype<>4 AND connectionType=0) and  TypeFlag=''D'' group by [Entity Code]'

--Hours
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT Where TypeFlag=''D'' group by [Entity Code]'

--Minutes
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where TypeFlag=''D'' group by [Entity Code]'
--Audio-Video Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,sum(isnull(Duration,0)) as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where ConfType=2 and TypeFlag=''D'' group by [Entity Code]'

--IP Dial-in Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], sum(isnull(Duration,0)) as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype <> 4 AND connectionType=1) and TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ') d group by [Entity Code];'

--ISDN Network - seperate tab
--All point-to-point calls are to be billed as ISDN calls.
-- Conftype = 4 (Point-Point)
--FB 2588
-- ZD 100958
set @cmdsql = @cmdsql + ' Select ltrim(rtrim(convert(char,[Date],'+ @dformat +'))) as [Date],'

set @cmdsql = @cmdsql + ' ltrim(rtrim(Start)) as Start,ltrim(rtrim([End])) as [End],Duration,Room,[Meeting Title],'
set @cmdsql = @cmdsql + ' [Last Name],First,email,Phone,State,City,[Conf #],'
set @cmdsql = @cmdsql + ' [Entity Code],[Entity Name],[Meeting Type] as [Conference Type]'
set @cmdsql = @cmdsql + ' ,vidProtocol as Protocol,Category as [Connection Type],'
set @cmdsql = @cmdsql + ' Status,Remarks from #TempCalendarDT where ((addresstype=4 and TypeFlag=''D'''
set @cmdsql = @cmdsql + ' and State=''ISDN Network'' and City=''PacBell PRIs'') OR (Conftype=4));'


/* ISDN Summary */
declare @condisdn as varchar (1000)
declare @condisdn1 as varchar (1000)
declare @cmdsql2 as varchar(5000)

set @condisdn1 =  'and ((addresstype=4 and State=''ISDN Network'' and City=''PacBell PRIs''))'

set @cmdsql2 = ' Select ''ISDN'' as [Entity Code], sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql2 = @cmdsql2 + ' sum([Pt-Pt Meetings]) as [Point-to-Point Conferences],'
set @cmdsql2 = @cmdsql2 + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out connections],'
set @cmdsql2 = @cmdsql2 + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in connections],'
set @cmdsql2 = @cmdsql2 + ' sum([VOIP Totals]) as [IP Dial-out connections],'
set @cmdsql2 = @cmdsql2 + ' sum([Audio-Video Conferences]) as [Audio-Video Conferences],'
set @cmdsql2 = @cmdsql2 + ' sum([IP Dial-in connections]) as [IP Dial-in connections],'
set @cmdsql2 = @cmdsql2 + ' (sum([Pt-Pt Meetings])+ sum([Audio-Video Conferences])) as [Total Conferences]'

set @cmdsql2 = @cmdsql2 + ' from ('
--Pt-Pt Mins
set @cmdsql2 = @cmdsql2 + ' Select sum(isnull(Duration,0)) as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where ConfType=4 and TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--ISDN Outbound Mins
set @cmdsql2 = @cmdsql2 + ' Union '
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' sum(isnull(Duration,0)) as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where connectionType=0 and  TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--ISDN Inbound Mins
set @cmdsql2 = @cmdsql2 + ' Union '
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' sum(isnull(Duration,0)) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where connectionType=1 and TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--Hours
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT Where TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1

--Minutes
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1
--Audio-Video Mins
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,sum(isnull(Duration,0)) as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where ConfType=2 and TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1

--IP Dial-in Mins
set @cmdsql2 = @cmdsql2 + ')d;'

exec(@cmdsql + @cmdsql2)


END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/* **************************** ZD 101835 / 102738 - 13 Jan 2015  Ends  ************************** */

/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9115.3.1  End(16 th Jan 2015)          */
/*                              Features & Bugs for V2.9115.3.2  Starts(18th Jan 2015)          */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9115.3.2  End(23rd Jan 2015)             */
/*                              Features & Bugs for V2.9115.3.3  Starts(23rd Jan 2015)          */
/*                                                                                              */
/* ******************************************************************************************** */


-- ZD 102473
update Err_LogPrefs_S set modulename='IBM Notes Forms' where modulename='Lotus Plugin' and UID =1

/*                              Features & Bugs for V2.9115.3.3  End(3th Feb 2015)          */
/*                              Features & Bugs for V2.9115.3.4  Start(3th Feb 2015)          */

/* **************************** ZD 102808 - 4 Feb 2015  Starts  ************************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Tier2_D ADD
	TopTierName nvarchar(256)  NULL
GO
COMMIT

Update Loc_Tier2_D set TopTierName = a.Name from Loc_Tier3_D as a where Loc_Tier2_D.L3LocationId = a.Id

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tmp_Room_D ADD
	L2LocationId int NULL,
	L3LocationId int NULL
GO
COMMIT


Update Tmp_Room_D set L2LocationId = a.L2LocationId , L3LocationId = a.L3LocationId from Loc_Room_D as a where Tmp_Room_D.RoomID = a.RoomID


/* **************************** ZD 102808 - 4 Feb 2015  Ends  ************************** */

/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9115.3.4  End(24th Feb 2015)             */
/*                              Features & Bugs for V2.9115.3.5  Starts(24th Feb 2015)          */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9115.3.5  End(25th Feb 2015)             */
/*                              Features & Bugs for V2.9115.3.6  Starts(25th Feb 2015)          */
/*                                                                                              */
/* ******************************************************************************************** */

--ZD 102998
Update MCU_Params_D set confMessage = 0 where BridgeTypeid = 13

/*                              Features & Bugs for V2.9115.3.6  End(4th MARCH 2015)             */
/*                              Features & Bugs for V2.9115.3.7  Starts(5th MARCH 2015)             */

/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9115.3.7  End(9th March 2015)             */
/*                              Features & Bugs for V2.9115.3.8  Starts(10th March 2015)          */
/*                                                                                              */
/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9115.3.8  ENDS(12th March 2015)          */
/*                              Features & Bugs for V2.9115.3.9  STARTS(12th March 2015)          */



/* **************** ZD 103095 - HD  - March 16th 2015 Starts ****************************************************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	WaitList smallint NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_WaitList DEFAULT 0 FOR WaitList
GO
COMMIT

update Conf_Conference_D set WaitList = 0


/******  Table [Loc_FloorPlan_D]   ******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Loc_FloorPlan_D](
	[Floorplanid] [int] IDENTITY(1,1)NOT NULL,
	[FloorPlaneName] [nvarchar](150) NULL,
	[loginUserid] [int] NULL,
	[orgId] [int] NULL,
	[FloorImage] [varbinary](max) NOT NULL,
	[identifiedLocations] [nvarchar](250) NULL,
	[roomids] [nvarchar](250) NOT NULL,
	[createddate] [datetime] NOT NULL,
	[modifieddate] [datetime] NOT NULL,
	[L2LocationId] [int] NULL,
	[L3LocationId] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableWaitList smallint NULL
GO
COMMIT

update Org_Settings_D set EnableWaitList = 0


-- IMPORTANT: Please change the existing custom role's menu mask manually.

update Usr_Roles_D
set roleMenuMask='8*240-4*11+8*254+4*15+4*15+5*31+4*15+5*31+5*31+2*0+2*3+4*11+8*0+5*0+3*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='General User'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+8*255+4*15+4*15+5*31+4*15+5*31+5*31+2*3+2*3+4*11+8*255+5*31+3*7+9*511+2*3+2*3+2*3+1*0-7*93'
where roleName='Organization Administrator 1'

update Usr_Roles_D
set roleMenuMask='8*252-4*15+8*255+4*15+4*15+5*31+4*15+5*31+5*31+2*3+2*3+4*15+8*255+5*31+3*7+9*511+2*3+2*3+2*3+1*1-7*127'
where roleName='Site Administrator'


update Usr_Roles_D
set roleMenuMask='8*136-4*0+8*0+4*0+4*0+5*0+4*0+5*0+5*0+2*0+2*0+4*0+8*2+5*0+3*0+9*0+2*0+2*3+2*0+1*0-7*92'
where roleName='Catering Administrator'

update Usr_Roles_D
set roleMenuMask='8*136-4*0+8*0+4*0+4*0+5*0+4*0+5*0+5*0+2*0+2*0+4*0+8*4+5*0+3*0+9*0+2*3+2*0+2*0+1*0-7*92'
where roleName='Audiovisual Administrator'

update Usr_Roles_D
set roleMenuMask='8*136-4*0+8*0+4*0+4*0+5*0+4*0+5*0+5*0+2*0+2*0+4*0+8*1+5*0+3*0+9*0+2*0+2*0+2*3+1*0-7*92'
where roleName='Facility Administrator'

update Usr_Roles_D
set roleMenuMask='8*96-4*1+8*160+4*15+4*0+5*30+4*0+5*0+5*0+2*0+2*2+4*0+8*0+5*0+3*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile'

update Usr_Roles_D
set roleMenuMask='8*96-4*9+8*160+4*15+4*0+5*30+4*0+5*0+5*0+2*0+2*2+4*0+8*0+5*0+3*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile Manage'

update Usr_Roles_D
set roleMenuMask='8*112-4*9+8*254+4*15+4*15+5*30+4*15+5*30+5*30+2*0+2*2+4*4+8*0+5*0+3*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile Advanced'

update Usr_Roles_D
set roleMenuMask='8*208-4*9+8*254+4*9+4*10+5*16+4*8+5*16+5*16+2*0+2*0+4*11+8*0+5*0+3*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='View-Only'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+8*255+4*15+4*15+5*31+4*15+5*31+5*31+2*3+2*3+4*11+8*255+5*23+3*7+9*381+2*3+2*3+2*3+1*0-7*93'
where roleName='VNOC Operator'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+8*255+4*15+4*15+5*31+4*15+5*31+5*31+2*3+2*3+4*15+8*255+5*31+3*7+9*511+2*3+2*3+2*3+1*0-7*93'
where roleName='Organization Administrator 2'



update list
set list.MenuMask = roles.roleMenuMask
from Usr_List_D as list, Usr_Roles_D as roles
where list.roleID = roles.roleID

update Inactive
set Inactive.MenuMask = roles.roleMenuMask
from Usr_Inactive_D as Inactive, Usr_Roles_D as roles
where Inactive.roleID = roles.roleID



insert into icons_ref_s values('49','../en/img/DirectoryImportIcon.png','LDAP Groups','LDAPGroup.aspx')

insert into icons_ref_s values('50','../en/img/ManageRooms.png','Floor Plans','RoomFloor.aspx')

insert into icons_ref_s values('51','../en/img/OnMCUIcon.png','Wait List','ConferenceList.aspx?t=11')




/* **************** ZD 103095 - HD  - March 16th 2015 Ends ****************************************************** */

/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9115.3.9  ENDS(23rd March 2015)          */
/*                              Features & Bugs for V2.9115.3.10  STARTS(24th March 2015)       */
/*                              Features & Bugs for V2.9115.3.10  Ends  (31th March 2015)       */
/* ******************************************************************************************** */


