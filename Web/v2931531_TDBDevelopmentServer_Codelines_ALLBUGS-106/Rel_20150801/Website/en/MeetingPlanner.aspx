<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.	
*
* You should have received a copy of the TrueDaybook license with	
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MeetingPlanner.MeetingPlanner" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>
<script type="text/javascript" src="script/mousepos1.js"></script> <%-- ZD 102723 --%>
<script type="text/javascript" src="script/showmsg.js"></script> <%-- ZD 102723 --%>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
<link rel="StyleSheet" href="css/divtable.css" type="text/css" />
<!-- Changed Code for Window Dressing start-->
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="Mirror/styles/main.css">  
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/meetingstyleplan.css">  
<!-- Changed Code for Window Dressing End-->
<script>


function mouseoverdiv(confname, confstart, confdur, confloc)
{
	show_message_prompt('image/pen.gif','Conference Name', confname.toString().replace("+","\"").replace("+", ","), confstart, confdur, confloc); //FB 2321
}

function mousemovediv()
{
	move_message_prompt();
}
function mouseoutdiv()
{
  closethis();
}

//ZD 100428 START- Close the popup window using the esc key
document.onkeydown = EscClosePopup;
function EscClosePopup(e) {
    if (e == null)
        var e = window.event;
    if (e.keyCode == 27) {
        window.close();
    }
}
//ZD 100428 END
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Meeting Planner</title>
      <%--  Removed Code for Window Dressing
    <link rel="stylesheet" type="text/css" href="css/meetingstyleplan.css" />--%>
    
</head>
<body>
    <form id="form1" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
    <table align="center" width="100%">
     <tr>
         <td colspan="2" align="center"><h3><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, MeetingPlanner_MeetingPlanner%>" runat="server"></asp:Literal></h3>
         </td>
     </tr>
    <tr>
         <%--ZD 100933 - Start--%>
        <td>
            <table align="center">
                <tr>
                    <td >
                        <asp:RadioButton runat="server" GroupName="ChangeTime"  ID="OfficeHours" Text="<%$ Resources:WebResources, MeetingPlanner_OfficeHours%>" AutoPostBack="true" OnCheckedChanged="BindDataFromSearch"/>
                        <asp:RadioButton runat="server" GroupName="ChangeTime"  ID="ShowAll" Text="<%$ Resources:WebResources, MeetingPlanner_ShowAll%>"  AutoPostBack="true" OnCheckedChanged="BindDataFromSearch"/>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
		            <td width="20" id="tdSuit" runat="server" height="20" class="moresuitSched" >
		            </td>
		            <td class="blackblodtext"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, MeetingPlanner_MoreSuitable%>" runat="server"></asp:Literal></td><!-- Changed Code for Window Dressing -->
		            <td width="20" height="20"  class="conferenceSched" > 			 
		            </td>
		            <td class="blackblodtext"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, MeetingPlanner_Conference%>" runat="server"></asp:Literal></td>
                </tr>
            </table>
        </td>
        <%--ZD 100933 - End--%>
    </tr>
    <tr>
    <td align="center">
        <%--Changed Code for Window Dressing
        <asp:Table Width="100%" runat="server" ID="Table1" class="bordermeetingstyle" border="0" cellpadding="1" cellspacing="1" align="center"></asp:Table></td></tr>--%>
        <asp:Table Width="100%" runat="server" ID="MeetingPlannerTable" border="0" cellpadding="1" cellspacing="1" align="center"></asp:Table></td></tr>
    </table>
    <table align="center">
   <tr>
     <td height="20"></td></tr>
		
	</table>
   </div>
    </form>
</body>
</html>
