/*ZD 100147 Start*/
/* Copyright (C) 2015 TrueDaybook - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.
*
* You should have received a copy of the TrueDaybook license with
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/
/*ZD 100147 ZD 100886 End*/
$(document).ready(function() {

    $('.PCSelected').live('click', function() {


        //$("body").css({ overflow: 'hidden' });
        var imgId = $(this).attr('id');

        var pctype = "";
        if (imgId == "btnBJ")
            pctype = "1";
        else if (imgId == "btnJB")
            pctype = "2";
        else if (imgId == "btnLync")
            pctype = "3";
        else if (imgId == "btnVidtel")
            pctype = "4";
        else if (imgId == "btnVidyo")
            pctype = "5";

        var userID;
        if ($('#hdnApprover4').length > 0)
            userID = $('#hdnApprover4').val();
        else
            userID = "11";

        var dataParam = { userID: userID, pctype: pctype };

        var dataParameter = JSON.stringify(dataParam);
        //debugger;
        var camUrl = "ConferenceSetup.aspx/fnFetchPCDetails";
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: camUrl,
            data: dataParameter,
            dataType: "json",
            success: function(data) {
                //alert(data.d);
                if (data.d != '') {
                    $('#popupdiv').fadeIn();
                    $('#PCHtmlContent').html(data.d);
                    $('#divPCdetails').show();
                    $('#divPCdetails').css('width', "700px");
                    $("#divPCdetails").bPopup({
                        fadeSpeed: 'slow',
                        followSpeed: 1500,
                        modalColor: 'gray'
                    });
                }

            },
            error: function(result) {
                $('#popupdiv').fadeOut();
                //operationError();
            }
        });

    });
    // -----------------------------------------------------------------------------------------
    // ------------------------ btnCancelEventLog ------------------------------------------------
    $('#btnPCCancel').click(function() {        
        $('#popupdiv').fadeOut();
        $('#divPCdetails').fadeOut();
        $('#communStatus', window.parent.document).val("0");
    });
    // -----------------------------------------------------------------------------------------
});
