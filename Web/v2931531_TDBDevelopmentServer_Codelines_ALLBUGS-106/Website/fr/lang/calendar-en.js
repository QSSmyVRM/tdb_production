/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
// ** I18N

// Calendar EN language
// Author: Mihai Bazon, <mishoo@infoiasi.ro>
// Encoding: any
// Distributed under the same terms as the calendar itself.

// For translators: please use UTF-8 if possible.  We strongly believe that
// Unicode is the answer to a real internationalized world.  Also please
// include your contact information in the header, as can be seen above.

// full day names
Calendar._DN = new Array
("Dimanche",
 "Lundi",
 "Mardi",
 "Mercredi",
 "Jeudi",
 "Vendredi",
 "Samedi",
 "Dimanche");

// Please note that the following array of short day names (and the same goes
// for short month names, _SMN) isn't absolutely necessary.  We give it here
// for exemplification on how one can customize the short day names, but if
// they are simply the first N letters of the full name you can simply say:
//
//   Calendar._SDN_len = N; // short day name length
//   Calendar._SMN_len = N; // short month name length
//
// If N = 3 then this is not needed either since we assume a value of 3 if not
// present, to be compatible with translation files that were written before
// this feature.

// short day names
Calendar._SDN = new Array
("Dim",
 "Lun",
 "Mar",
 "Mer",
 "Jeu",
 "Ven",
 "Sam",
 "Dim");

// full month names
Calendar._MN = new Array
("Janvier",
 "F\u00E9vrier",
 "Mars",
 "Avril",
 "Mai",
 "Juin",
 "Juillet",
 "Ao\u00FBt",
 "Septembre",
 "Octobre",
 "Novembre",
 "D\u00E9cembre");

// short month names
Calendar._SMN = new Array
("Jan",
 "F\u00E9v",
 "Mar",
 "Avr",
 "Mai",
 "Jui",
 "Juil",
 "Ao\u00FB",
 "Sep",
 "Oct",
 "Nov",
 "D\u00E9c");

// tooltips
Calendar._TT = {};
Calendar._TT["INFO"] = "A propos du calendrier";

Calendar._TT["ABOUT"] =
//"DHTML Date/Time Selector\n" +
//"(c) dynarch.com 2002-2003\n" + // don't translate this this ;-)
//"For latest version visit: http://dynarch.com/mishoo/calendar.epl\n" +
//"Distributed under GNU LGPL.  See http://gnu.org/licenses/lgpl.html for details." +
//"\n\n" +
"Date de s\u00E9lection\n" +
//ZD 102407 Starts
"- Cliquer sur une date pour la s\u00E9lectionner.\n" +
"- Utilisez les boutons de xbb \ xab et \ se d\u00E9placer entre les ann\u00E9es\n" +
"- Utilisez le " + String.fromCharCode(0x2039) + " et " + String.fromCharCode(0x203a) + " boutons pour passer d'un mois.\n" +
"- Maintenez le bouton gauche de la souris sur l'un des boutons ci-dessus afin d'acc\u00E9der \u00E0 une liste des ann\u00E9es / mois pour une s\u00E9lection rapide.\n";
//ZD 102407 End
Calendar._TT["ABOUT_TIME"] = "\n\n" +
"S\u00E9lection de l'heure\n" +
"- Cliquez sur une des parties de temps pour l'agrandir\n" +
"- ou Maj-cliquez pour diminuer\n" +
"- ou cliquez et faites glisser pour une s\u00E9lection plus rapide.";

Calendar._TT["PREV_YEAR"] = "Pr\u00E9c\u00E9dent. ann\u00E9e (maintenez pour le menu)";
Calendar._TT["PREV_MONTH"] = "Pr\u00E9c\u00E9dent. mois (maintenez pour le menu)";
Calendar._TT["GO_TODAY"] = "Aller \u00E0 aujourd'hui";
Calendar._TT["NEXT_MONTH"] = "Le mois prochain (maintenez pour le menu)";
Calendar._TT["NEXT_YEAR"] = "L'ann\u00E9e prochaine (maintenez pour le menu)";
Calendar._TT["SEL_DATE"] = "S\u00E9lectionnez la date";
Calendar._TT["DRAG_TO_MOVE"] = "Glisser pour d\u00E9placer";
Calendar._TT["PART_TODAY"] = " (aujourd'hui)";

// the following is to inform that "%s" is to be the first day of week
// %s will be replaced with the day name.
Calendar._TT["DAY_FIRST"] = "Affichage %s premier";

// This may be locale-dependent.  It specifies the week-end days, as an array
// of comma-separated numbers.  The numbers are from 0 to 6: 0 means Sunday, 1
// means Monday, etc.
Calendar._TT["WEEKEND"] = "0,6";

Calendar._TT["CLOSE"] = "Fermer";
Calendar._TT["TODAY"] = "Aujourd'hui";
Calendar._TT["TIME_PART"] = "(Maj) Cliquez ou faites glisser pour changer la valeur";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "%Y-%m-%d";
Calendar._TT["TT_DATE_FORMAT"] = "%a, %b %e";

Calendar._TT["WK"] = "sem";
Calendar._TT["TIME"] = "Heure:";
