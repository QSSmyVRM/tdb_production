<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.
*
* You should have received a copy of the TrueDaybook license with
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>

<%@ Page Language="C#" Inherits="ns_EditUserTemplates.UserTemplates" Buffer="true" %>

<%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- FB 2050 -->
<%--FB 2779--%>
<!-- Window Dressing -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<script type="text/javascript">
    var servertoday = new Date();
</script>
<script type="text/javascript" src="inc/functions.js"></script>
<%--FB 1861--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
<%--FB 1861--%>
<%--FB 1982--%>
<script type="text/javascript" src="script/mytreeNET.js"></script>
<script language="javascript">
    var isIE = false; //ZD 100420
    if (navigator.userAgent.indexOf('Trident') > -1)
        isIE = true;
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    //<%--FB 481 Saima--%>
    function DisableButton() {
        if (document.getElementById("txtAccountExpiry").value == "") {
            document.getElementById("lblExpError").style.display = "";
            document.getElementById("lblExpError").innerHTML = "<asp:Literal Text='<%$ Resources:WebResources, Required%>' runat='server'></asp:Literal>";
        }
        else {
            document.getElementById("lblExpError").innerHTML = "";
        }

        if (typeof (Page_ClientValidate) == 'function')
            if (Page_ClientValidate()) {
                if (!CheckDate(document.getElementById("txtAccountExpiry").value))  //Added for FB issue 1493
                    return false;
                DataLoading(1);
                document.getElementById("<%=btnSubmit.ClientID %>").style.display = "none";
                document.getElementById("<%=btnSubmitNew.ClientID %>").style.display = "none";
                return true;
                DataLoading(1); //ZD 100176
            }

    }
    //Code added for FB issue 1493 - Start
    function CheckDate(obj) {
        //debugger;
        //    var licenseExp = GetDefaultDate('<%=licenseDate%>','<%=format%>'); // FB 1747
        var accExpDate = GetDefaultDate(document.getElementById("txtAccountExpiry").value, '<%=format%>');


        if (document.getElementById("txtAccountExpiry").value == "") {
            document.getElementById("lblExpError").style.display = "";
            document.getElementById("lblExpError").innerHTML = "<asp:Literal Text='<%$ Resources:WebResources, Required%>' runat='server'></asp:Literal>";

            return false;
        }
        else {
            document.getElementById("lblExpError").innerHTML = "";
        }
        // FB 1747 - Commented (Start)
        //      if(Date.parse(accExpDate) > Date.parse('<%=licenseDate%>'))
        //      {
        //          document.getElementById("lblExpError").style.display = "";
        //          document.getElementById("lblExpError").innerHTML = "Invalid Date<br>Maximum date allowed is the<br>site license expiry date ("+licenseExp+")";
        //                 
        //          return false;
        //      }
        //      else
        //      {
        //         document.getElementById("lblExpError").innerHTML="";
        //      }
        //      
        // FB 1747 - Commented (End)
        if (Date.parse(accExpDate) <= Date.parse(new Date())) {
            document.getElementById("lblExpError").style.display = "";
            document.getElementById("lblExpError").innerHTML = "Invalid Date";

            return false;

        }
        else {
            document.getElementById("lblExpError").innerHTML = "";
        }

        return true;

    }
    //Code added for FB issue 1493 - End
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176 End
    function fnCancel() //FB 2565
    {
        DataLoading(1); //ZD 100176
        window.location.replace('ManageUserTemplatesList.aspx');  //CSS Project
        return false; //ZD 100420
    }



</script>
<script type="text/javascript" src="script/RoomSearch.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>User Template</title>
</head>
<body>
    <form id="frmInventoryManagement" runat="server" autocomplete="off" method="post"
    onsubmit="return true;DataLoading(1);">
    <%--ZD 100176--%>
    <%--ZD 101190--%>
    <input type="hidden" id="helpPage" value="105">
    <%--Code changed for FB 1425 QA Bug -Start--%>
    <input type="hidden" id="hdntzone" runat="server" />
    <%--Code changed for FB 1425 QA Bug -End--%>
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server" />
    <!--Added room search-->
    <input name="locstrname" type="hidden" id="locstrname" runat="server" />
    <!--Added room search-->
    <b>
        <div>
            <%--ZD 101022 start--%>
            <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
                <Scripts>
                    <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
                </Scripts>
            </asp:ScriptManager>
            <%--ZD 101022 End--%>
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            <asp:Label ID="lblHeader" runat="server" Text=""></asp:Label>
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                        <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display: none">
                            <img border='0' src='image/wait1.gif' alt='Loading..' />
                        </div>
                        <%--ZD 100678--%>
                    </td>
                </tr>
            </table>
            <table border="0" width="90%" style="margin-left: 50px">
                <%--ZD 100393 Start--%>
                <tr>
                    <td align="Left" width="600px"><%--ZD 103673--%>
                        <table width="85%" cellpadding="0" cellspacing="3" border="0" style="padding-left: 12px">
                            <tr>
                                <td width="26%"><%--ZD 103673--%>
                                    <span class="subtitleblueblodtext">
                                        <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_TemplateName%>" runat="server"></asp:Literal></span>
                                </td>
                                <td width="35%"><%--ZD 103673--%>
                                    <%--ZD 100393 End--%>
                                    <asp:TextBox CssClass="altText" runat="server" ID="txtTemplateName" Text="" Width="200"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtTemplateName"
                                        ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regTemplateName" ControlToValidate="txtTemplateName"
                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="left" rowspan="6" valign="top" class="blackblodtext">
                        <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_PreferredMeeti%>"
                            runat="server"></asp:Literal><table>
                                <tr>
                                    <td align="left" valign="top" style="width: 10%">
                                        <%--<input name="opnRooms" type="button" id="opnRooms" onclick="javascript:OpenRoomSearch('frmInventoryManagement');" value="Add Room" class="altMedium0BlueButtonFormat" />--%><%--ZD 100420--%>
                                        <button name="opnRooms" id="opnRooms" onclick="javascript:OpenRoomSearch('frmInventoryManagement');"
                                            class="altMedium0BlueButtonFormat">
                                            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, AddRoom%>" runat="server"></asp:Literal></button>
                                        <%--ZD 100420--%>
                                        <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();"
                                            style="display: none;" /><br />
                                        <span class="blackblodtext"><font size="1">
                                            <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_Doubleclickon%>"
                                                runat="server"></asp:Literal></font></span>
                                    </td>
                                    <td align="left" style="width: 90%">
                                        <select size="4" wrap="false" name="RoomList" id="RoomList" class="treeSelectedNode"
                                            ondblclick="javascript:Delroms(this.value)" onkeydown="if(event.keyCode ==32){javascript:Delroms(this.value)}"
                                            style="height: 350px; width: 100%;" runat="server">
                                        </select>
                                        <%--ZD 100420--%>
                                    </td>
                                </tr>
                            </table>
                        <asp:RadioButtonList Style="display: none;" ID="rdSelView" runat="server" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                            RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow" CssClass="blackblodtext">
                            <asp:ListItem Selected="True" Value="1"><font class="blackblodtext">Level View</font></asp:ListItem>
                            <asp:ListItem Value="2"><font class="blackblodtext">List View</font></asp:ListItem>
                        </asp:RadioButtonList>
                        <br />
                        <asp:Panel Style="display: none;" ID="pnlLevelView" runat="server" Height="550" Width="100%"
                            ScrollBars="Auto" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                            <asp:TreeView ID="treeRoomSelection" runat="server" BorderColor="White" Height="90%"
                                ShowCheckBoxes="Leaf" ShowLines="True" Width="95%" onclick="javascript:getOneRoom(event)">
                                <NodeStyle Font-Size="Smaller" />
                                <RootNodeStyle BorderStyle="None" Font-Size="Smaller" ForeColor="Blue" />
                                <SelectedNodeStyle />
                                <ParentNodeStyle BorderStyle="None" ForeColor="#404040" />
                                <LeafNodeStyle Font-Size="Smaller" />
                            </asp:TreeView>
                        </asp:Panel>
                        <asp:Panel Style="display: none;" ID="pnlListView" runat="server" BorderColor="Blue"
                            BorderStyle="Solid" BorderWidth="1px" Height="500" ScrollBars="Auto" Visible="False"
                            Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True"
                            Font-Names="Verdana" Font-Size="Small" ForeColor="Green">
                            <asp:RadioButtonList ID="lstRoomSelection" runat="server" Height="95%" Width="95%"
                                Font-Size="Smaller" ForeColor="ForestGreen" Font-Names="Verdana" RepeatLayout="Flow">
                            </asp:RadioButtonList>
                        </asp:Panel>
                        <%--Added for Location Issues  - Start--%>
                        <asp:Panel Style="display: none;" ID="pnlNoData" runat="server" BorderColor="Blue"
                            BorderStyle="Solid" BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False"
                            Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Size="Small">
                            <table>
                                <tr align="center">
                                    <td>
                                        <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_YouhavenoRoo%>" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <%--Added for Location Issues  - End--%>
                    </td>
                </tr>
                <tr>
                    <td align="Left">
                        <table cellspacing="5">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <span class="subtitleblueblodtext">
                                        <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_PersonalOption%>"
                                            runat="server"></asp:Literal></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 600px;"><%--ZD 103673--%>
                        <table width="85%" cellpadding="3" cellspacing="3" border="0" style="margin-left: 35px">
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                            <tr id="TzTR" runat="server">
                                <%--Code changed for FB 1425 QA Bug -End--%>
                                <td align="left" width="32%" class="blackblodtext">
                                    <%--ZD 100393--%>
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_TimeZone%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left" width="40%">
                                    <asp:DropDownList ID="lstTimeZone" runat="server" CssClass="altLong0SelectFormat"
                                        DataTextField="timezoneName" DataValueField="timezoneID">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator runat="server" ID="reqTZ" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources, Required%>"
                                        Display="dynamic" ControlToValidate="lstTimeZone"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <%--ZD 103673 Start--%>
                            <tr id="trDateFormat" runat="server">
                                <td align="left" width="22%" class="blackblodtext">
                                    <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ManageUserProfile_DateFormat%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left" nowrap="nowrap"  width="40%">
                                    <asp:DropDownList ID="DrpDateFormat" runat="server" CssClass="altSelectFormat" >
                                        <asp:ListItem Selected="True" Value="MM/dd/yyyy" Text="mm/dd/yyyy"></asp:ListItem>
                                        <asp:ListItem Value="dd/MM/yyyy" Text="dd/mm/yyyy"></asp:ListItem>                                  
                                  </asp:DropDownList>/ 
                                   <asp:DropDownList ID="lstTimeFormat" CssClass="altText" runat="server">
                                        <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, DateFormat_1%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, DateFormat_2%>"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="<%$ Resources:WebResources, DateFormat_3%>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <%--ZD 103673 End--%>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_PreferredPerso%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="lstAddressBook" runat="server" CssClass="altLong0SelectFormat">
                                        <asp:ListItem Value="0" Text="None"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="MS Outlook"></asp:ListItem>
                                        <%--                                    <asp:ListItem Value="2" Text="Lotus Notes 6.x"></asp:ListItem>
                                        --%>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_DefaultGroup%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="lstGroup" runat="server" CssClass="altLong0SelectFormat" OnLoad="GetGroups"
                                        DataTextField="groupName" DataValueField="groupID">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_UserRole%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="lstUserRole" runat="server" CssClass="altLong0SelectFormat"
                                        onchange="javascript:fnChangeSelection()" DataTextField="Name" DataValueField="ID">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_AccountExpirat%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText" runat="server" onfocusout="javascript:CheckDate(this)"
                                        ID="txtAccountExpiry"></asp:TextBox>
                                    <%-- Code Changed for FB issue 1493 --%>
                                    <%-- Code changed by Offshore for FB Issue 1073 -- start --%>
                                    <a href="#" onkeydown="if(event.keyCode == 13){if(!isIE){document.getElementById('cal_triggerd').click();return false;}}"
                                        onclick="if(isIE){this.childNodes[0].click();return false;}">
                                        <img alt="Date Selector" src="image/calendar.gif" border="0" width="20" id="cal_triggerd"
                                            style="cursor: pointer; height: 20; vertical-align: bottom" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>"
                                            onclick="return showCalendar('<%=txtAccountExpiry.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" /></a>
                                    <%--ZD 100420--%>
                                    <%-- Code changed by Offshore for FB Issue 1073 -- End--%>
                                    <%--<asp:RequiredFieldValidator runat="server"  ID="reqExpiryDate" ControlToValidate="txtAccountExpiry" ErrorMessage="Required" Display="dynamic" ></asp:RequiredFieldValidator>--%>
                                    <%-- Code added for FB issue 1493 - Start --%>
                                    <span id="lblExpError" style="color: Red; display: none;"></span>
                                    <%--<asp:RangeValidator ID="rangeExpiryDate" runat="server" Display="dynamic" ErrorMessage="Invalid Date" ControlToValidate="txtAccountExpiry" Type="Date"></asp:RangeValidator>--%>
                                    <%-- Code added for FB issue 1493 - End --%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_InitialWallet%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText" runat="server" ID="txtInitialTime" Text=""></asp:TextBox>
                                    <span style="font-weight:normal">(mins)</span><%--ZD 103053--%>
                                    <asp:RequiredFieldValidator ID="reqMinutes" SetFocusOnError="true" runat="server"
                                        ErrorMessage="<%$ Resources:WebResources, Required%>" CssClass="lblError" ControlToValidate="txtInitialTime"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="ValidatorTimeremaining" SetFocusOnError="true" runat="server"
                                        CssClass="lblError" Display="dynamic" ControlToValidate="txtInitialTime" ErrorMessage="<%$ Resources:WebResources, Reqtimeremaining%>"
                                        MaximumValue="2000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_EmailNotificat%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:CheckBox runat="server" ID="chkEmailNotification" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_PreferredDepar%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <%--<asp:DropDownList ID="lstDepartment" runat="server" CssClass="altLong0SelectFormat" OnInit="LoadDepartments" DataTextField="name" DataValueField="id">
                                </asp:DropDownList>--%>
                                    <asp:ListBox runat="server" ID="lstDepartment" CssClass="altSelectFormat" OnInit="LoadDepartments"
                                        DataTextField="name" DataValueField="id" Rows="6" SelectionMode="Multiple"></asp:ListBox>
                                    <br />
                                    <span style="color: #666666; font-weight:normal;">*
                                        <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, SelectMultiDept%>" runat="server"></asp:Literal></span>
                                </td>
                            </tr>
                            <tr>
                                <%--FB 1830--%>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_PreferredLangu%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="lstLanguage" runat="server" CssClass="altSelectFormat" DataTextField="name"
                                        DataValueField="ID">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reglstLanguage" ControlToValidate="lstLanguage" ErrorMessage="<%$ Resources:WebResources, Required%>"
                                        InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <%--ZD 102052 START AD Integration Phase2--%>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_ParLBL%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="drpParticipant" runat="server" CssClass="altSelectFormat">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_AVLBL%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="drpAVSettings" runat="server" CssClass="altSelectFormat">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EnableAdditionalOptions%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="drpAdditionalOptions" runat="server" CssClass="altSelectFormat">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EnableAudioVisualWO%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="drpAudioVisualWorkOrder" runat="server" CssClass="altSelectFormat">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EnableCateringWO%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="drpCateringWorkOrder" runat="server" CssClass="altSelectFormat">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EnableFacilityWO%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="drpFacilityWorkOrder" runat="server" CssClass="altSelectFormat">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_LblExc%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="drpOutlook" runat="server" CssClass="altSelectFormat">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, ManageUserProfile_LblMob%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="drpMobile" runat="server" CssClass="altSelectFormat">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ManageUserProfile_LblDom%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="drpNotes" runat="server" CssClass="altSelectFormat">
                                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <%--ZD 102052 END--%>
                        </table>
                    </td>
                </tr>
                <tr id="trAudvid" runat="server">
                    <%--Added for MOJ Phase 2 QA--%>
                    <td align="Left">
                        <table cellspacing="5">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <span class="subtitleblueblodtext">
                                        <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_AudioVideoConn%>"
                                            runat="server"></asp:Literal></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" id="trTbAudvid" runat="server" style="width: 600px;"><%--ZD 103673--%>
                        <%--Added for MOJ Phase 2 QA--%>
                        <asp:CustomValidator runat="server" Display="dynamic" ID="cusVal1" SetFocusOnError="true"
                            OnServerValidate="ValidateIPAddress" CssClass="lblError"></asp:CustomValidator>
                        <table width="85%" cellpadding="3" cellspacing="3" border="0" style="margin-right:25px"><%--ZD 103673--%>
                            <tr>
                                <td align="left" width="20%" class="blackblodtext"><%--ZD 103673--%>
                                    <%--ZD 100393--%><asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_DefaultLineRa%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left" width="35%"><%--ZD 103673--%>
                                    <asp:DropDownList ID="lstLineRate" runat="server" CssClass="altSelectFormat" DataTextField="lineRateName"
                                        DataValueField="lineRateID">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_DefaultConnect%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="lstConnectionType" CssClass="altSelectFormat" runat="server"
                                        DataTextField="Name" DataValueField="ID">
                                    </asp:DropDownList>
                                    <%--Fogbugz case 427--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_DefaultProtoco%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="lstProtocol" runat="server" CssClass="altSelectFormat" DataTextField="Name"
                                        DataValueField="ID">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_DefaultIPISDN%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText" runat="server" ID="txtIPISDNAddress" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="req1" Enabled="false" ErrorMessage="<%$ Resources:WebResources, Required%>"
                                        ControlToValidate="txtIPISDNAddress" runat="server" Display="dynamic"></asp:RequiredFieldValidator>
                                    <%--Fogbugz case 375 enabled=false--%>
                                    <%--FB 1972--%>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtIPISDNAddress"
                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters37%>"
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^#$%&()~]*$"></asp:RegularExpressionValidator>
                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtIPISDNAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_AssignedMCU%>" runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="lstMCU" runat="server" CssClass="altLong0SelectFormat" DataTextField="name"
                                        DataValueField="ID">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_DefaultEquipme%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="lstEquipment" runat="server" CssClass="altLong0SelectFormat"
                                        OnInit="LoadEquipment" DataTextField="videoEquipmentName" DataValueField="videoEquipmentID">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditUserTemplate_OutsideNetwork%>"
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="left">
                                    <asp:CheckBox runat="server" ID="chkOutsideNetwork" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%">
                <tr>
                    <td align="center">
                        <table width="90%" cellpadding="0" cellspacing="3" border="0">
                            <tr>
                                <td align="center">
                                    <asp:Button ID="benReset" Text="<%$ Resources:WebResources, Reset%>" CssClass="altLongBlueButtonFormat"
                                        runat="server" OnClientClick="DataLoading(1)" OnClick="ResetTemplate" ValidationGroup="Reset" />
                                    <!-- FB Case 229 - Saima - Validation Group added for reset to avoid triggering validation on controls -->
                                    <%--                                <input type="reset" value="Reset" id="btnReset" class="altLongBlueButtonFormat" runat="server" />
                                    --%>
                                </td>
                                <td align="center">
                                    <asp:Button runat="server" Text="<%$ Resources:WebResources, EditUserTemplate_btnSubmitNew%>"
                                        OnClick="SubmitNew" ID="btnSubmitNew" OnClientClick="javascript:DisableButton()"
                                        Width="280px" /><br />
                                    <%-- FB 2796 ZD 101714--%>
                                </td>
                                <td align="center">
                                    <%--<input type="button" id="btnCancel" class="altLongYellowButtonFormat" value="Cancel" onclick="fnCancel()" />--%>
                                    <%--FB 2565--%>
                                    <%--ZD 100369--%>
                                    <button type="button" id="btnCancel" class="altLongYellowButtonFormat" onclick="fnCancel();"
                                        style="width: 100px">
                                        <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button><%--ZD 100369--%>
                                </td>
                                <%--ZD 100428--%>
                                <td align="center">
                                    <asp:Button runat="server" Text="<%$ Resources:WebResources, EditUserTemplate_btnSubmit%>"
                                        OnClick="SubmitOnly" ID="btnSubmit" OnClientClick="javascript:return DisableButton()"
                                        Width="225px" /><%-- FB issue 1493 --%>
                                    <%--FB 2796--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <input type="hidden" id="hdnLocation" />
        <img src="keepalive.asp" name="myPic" width="1px" height="1px" alt="Keepalive" style="display: none" /><%--ZD 1001419--%>
    </b>
    </form>
    <%--code added for Soft Edge button--%>
    <script type="text/javascript" src="inc/softedge.js"></script>
    <%--ZD 100420 start--%>
    <script type="text/javascript">
        document.onkeydown = function (evt) {
            evt = evt || window.event;
            var keyCode = evt.keyCode;
            if (keyCode == 8) {
                if (document.getElementById("btnCancel") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnCancel").click();
                        return false;
                    }
                }
                if (document.getElementById("btnGoBack") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnGoBack").click();
                        return false;
                    }
                }
            }
            fnOnKeyDown(evt);
        };

        if (document.getElementById('benReset') != null) {
            document.getElementById('chkOutsideNetwork').setAttribute("onblur", "document.getElementById('benReset').focus()");
            document.getElementById('benReset').setAttribute("onfocus", "");
            document.getElementById('benReset').setAttribute("onblur", "document.getElementById('btnSubmitNew').focus()");
            document.getElementById('btnSubmitNew').setAttribute("onfocus", "");
            document.getElementById('btnSubmitNew').setAttribute("onblur", "document.getElementById('btnCancel').focus()");
            document.getElementById('btnCancel').setAttribute("onfocus", "");
            document.getElementById('btnCancel').setAttribute("onblur", "document.getElementById('btnSubmit').focus()");
            document.getElementById('btnSubmit').setAttribute("onfocus", "");
        }


        function fnChangeSelection() {
            var RoleId = document.getElementById("lstUserRole").value;
            var DeptIds = document.getElementById("lstDepartment");

            if (RoleId == 2 || RoleId == 3 || RoleId == 11 || RoleId == 12) {
                DeptIds.disabled = true;
                for (var d = 0; d < DeptIds.options.length; d++) {
                    DeptIds.options[d].selected = true;
                }
            }
            else {
                DeptIds.disabled = false;
            }

            if (RoleId == 2 || RoleId == 3 || RoleId == 4 || RoleId == 5 || RoleId == 6 || RoleId == 11 || RoleId == 12) {
                document.getElementById("drpParticipant").value = "1";
                document.getElementById("drpAVSettings").value = "1";
                document.getElementById("drpAudioVisualWorkOrder").value = "1";
                document.getElementById("drpCateringWorkOrder").value = "1";
                document.getElementById("drpFacilityWorkOrder").value = "1";

                document.getElementById("drpParticipant").disabled = true;
                document.getElementById("drpAVSettings").disabled = true;
                document.getElementById("drpAudioVisualWorkOrder").disabled = true;
                document.getElementById("drpCateringWorkOrder").disabled = true;
                document.getElementById("drpFacilityWorkOrder").disabled = true;
            }
            else {
                document.getElementById("drpParticipant").disabled = false;
                document.getElementById("drpAVSettings").disabled = false;
                document.getElementById("drpAudioVisualWorkOrder").disabled = false;
                document.getElementById("drpCateringWorkOrder").disabled = false;
                document.getElementById("drpFacilityWorkOrder").disabled = false;
            }


        }

        fnChangeSelection();

    </script>
    <%--ZD 100420 End--%>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
