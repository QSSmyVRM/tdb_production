<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.		
*
* You should have received a copy of the TrueDaybook license with	
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%><%--ZD 100147 ZD 100886 End--%>			
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_RequestAccount" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--ZD 101028 --%>
<!--Altered for Window Dressing start-->
 <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="../en/Organizations/Original/Styles/main.css"><%--FB 1830--%>
<!--Altered for Window Dressing End-->
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">  
    <title>TrueDaybook</title>
  <%--<meta name="Description" content="TrueDaybook (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="TrueDaybook, TrueDaybook, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">--%>
  <%--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <META NAME="LANGUAGE" CONTENT="en">
  <META NAME="DOCUMENTCOUNTRYCODE" CONTENT="us">--%>

  
  <script type="text/javascript">      // FB 2790      
      var path = '<%=Session["OrgCSSPath"]%>';
      if (path == "")
          path = "Organizations/Org_11/CSS/Mirror/Styles/main.css";
      path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
      document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>
  <script language="JavaScript" src="inc/functions.js" type="text/javascript"></script>
    
    <script type="text/javascript">

        function fnLoginTransfer() {
            //ZD 103174 - Start
            var browserlang = navigator.userLanguage || navigator.language;

            if (browserlang != "" && browserlang != null)
                window.location.replace('genlogin.aspx?lang=' + browserlang);
            else
                window.location.replace('genlogin.aspx?lang="en-US"');
            //ZD 103174 - Emd
            return false;
    }      
        
    function limitDescriptionLen(obj)
	{
		var iKey;
		var eAny_Event = window.event;
		iKey = eAny_Event.keyCode;
		var re 
		re = new RegExp("\r\n","g")  
		x = obj.value.replace(re,"").length ;
		if (x >= obj.maxlength) {
			alert(EN_140);
			obj.value = (obj.value).substr(0, obj.maxlength)
			window.event.returnValue=false;
		}
	}
	
	function chkURL(srcstr)
	{
		pos = 0;
		if (srcstr.indexOf("</url>", pos) != -1) {
			while (srcstr.indexOf("</url>") != -1) {
				pos=srcstr.indexOf("</url>")
				tmpstr = srcstr.substring (0, pos);
				if (tmpstr.split("<url>").length != 2) {
					return false;
					break;
				}
				srcstr = srcstr.substring (pos+6, srcstr.length);
				pos = 0;
			}
		} else {
			if (srcstr.indexOf("<url>", pos) != -1) {
				return false;
			}
		}
		return true;
	}
	
	function fnValidate()
	{
		if ( (document.frmEmailLogin.TxtFirstName.value == "") ) {
		    alert(ReqAccFName);
				document.frmEmailLogin.TxtFirstName.focus();
				return (false);		
		}
//		else{
//			if(checkInvalidChar(document.frmEmailLogin.TxtFirstName.value) == false){
//				return false;
//			}
//		}
		
		if ( (document.frmEmailLogin.TxtLastName.value == "") ) {
		    alert(ReqAccLName);
				document.frmEmailLogin.TxtLastName.focus();
				return (false);		
		}
//		else{
//			if(checkInvalidChar(document.frmEmailLogin.TxtLastName.value) == false){
//				return false;
//			}
//		}
		
		if ( (document.frmEmailLogin.TxtLoginName.value == "") ) {
		    alert(ReqAccUserLoginName);
				document.frmEmailLogin.TxtLoginName.focus();
				return (false);		
		}
//		else{
//			if(!checkInvalidChar(document.frmEmailLogin.TxtLoginName.value))
//				return false;
//		}
		
		// !! email address
		if(!checkInvalidChar(document.frmEmailLogin.TxtEmail.value))
			return false;
			
		if ( !checkemail(document.frmEmailLogin.TxtEmail.value) ) {
		    alert(ReqAccEmail);
			document.frmEmailLogin.TxtEmail.focus();
			return(false);
		}
	
		if(!checkInvalidChar(document.frmEmailLogin.TxtConfirmEmail.value))
			return false;
			
		if ( !checkemail(document.frmEmailLogin.TxtConfirmEmail.value) ) {
			  alert(ReqAccEmail);
			document.frmEmailLogin.TxtConfirmEmail.focus();
			return(false);
		}
		
		if ( document.frmEmailLogin.TxtEmail.value != document.frmEmailLogin.TxtConfirmEmail.value ) {
		    alert(ReqAccEmailMatch);
			document.frmEmailLogin.TxtConfirmEmail.focus();
			return(false);
		}
		
	
//		if (Trim(document.frmEmailLogin.TxtAdditionalInfo.value) != "") {
//			if(checkInvalidChar(document.frmEmailLogin.TxtAdditionalInfo.value) == false){
//				return false;
//			}
//		}

//		if (!chkURL(document.frmEmailLogin.TxtAdditionalInfo.value)) {
//			 alert("Please enter a valid e-mail address.");
//			document.frmEmailLogin.TxtAdditionalInfo.focus();
//			return false;
//		}


	return true;
	}       
        
    </script>    
</head>
<body>
   <form id="frmEmailLogin" runat="server" autocomplete="off" method="post"><%--ZD 101190--%>
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
     <table width="90%">
        <tr>
            <td align="center">
            <br /><br /><br /><br /><br />
               <h3><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, RequestAccount_RequestaNewm%>" runat="server"></asp:Literal></h3>                 
            </td>
        </tr>  
        <tr>
            <td align="center" style="width: 1168px">
                <asp:Label ID="ErrLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
            </td>
        </tr>              
        </table> 
        <table width="90%" border="0" cellpadding="0" cellspacing="0">
           <tr valign="top">              
            <td>
                <table width="100%" border="0" cellpadding="6">        
                    <tr>                                         
                      <td>
                        <table border="0" cellpadding="2" cellspacing="1" width="700">                
                            <tr>
                                <td style="width:150px"></td>
                                <td colspan="2">
                                    <span style="color:Green"><asp:Label Font-Bold="true" ID="LblSuccess" Visible="False" runat="server" Text="<%$ Resources:WebResources, RequestAccount_LblSuccess%>"></asp:Label></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="height:5">
                                </td>
                            </tr>
                            <tr>
                              <td style="width:100px"></td>
                              <td align="left" style="width:600px" colspan="2">
                                <%--Window Dressing--%>
                                <span class="subtitleblueblodtext">
                                <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, RequestAccount_Pleasefillout%>" runat="server"></asp:Literal></span><%--FB 2579--%>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="3"  style="height:10"><br /></td>
                            </tr>
                            <tr>
                              <td></td>
                              <td align="left">
                                 <%--Window Dressing--%>
                                <label for="FirstName"  class="blackblodtext"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ManageUser_FirstName%>" runat="server"></asp:Literal></label>
                              </td>
                              <td>
                                 <%--FB 1888--%>   
                                <asp:TextBox  ID="TxtFirstName" runat="server" CssClass="altText" style="width: 200px;"  MaxLength="256"></asp:TextBox>      
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="TxtFirstName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters16%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> 
                              </td>
                            </tr>
                            <tr>
                              <td></td>
                              <td align="left">
                                 <%--Window Dressing--%>
                                <label for="LastName"  class="blackblodtext"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ManageUser_LastName%>" runat="server"></asp:Literal></label>
                              </td>
                              <td>
                                 <%--FB 1888--%>   
                                <asp:TextBox ID="TxtLastName" runat="server" CssClass="altText" style="width: 200px;" MaxLength="256" ></asp:TextBox>  
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="TxtLastName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters16%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> 
                              </td>
                            </tr>
                            <tr>
                              <td></td>
                              <td align="left">
                                <%--Window Dressing--%>
                                <label for="LoginName" class="blackblodtext"><asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, EmailSearch_LoginName%>" runat="server"></asp:Literal></label> 
                              </td>
                              <td>
                                <asp:TextBox ID="TxtLoginName" runat="server" CssClass="altText" style="width: 200px;" MaxLength="256"></asp:TextBox>  
                                 <%--FB 1888--%>   
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="TxtLoginName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                              </td>
                            </tr>
                            <tr>
                              <td></td>
                              <td align="left">
                                <%--Window Dressing--%>
                                <label for="Email" class="blackblodtext"><asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, Email%>" runat="server"></asp:Literal></label> 
                              </td>
                              <td>
                                <asp:TextBox ID="TxtEmail" runat="server" CssClass="altText" style="width: 200px;" MaxLength="256"></asp:TextBox>                               
                                 <%--FB 1888--%>   
                                <asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="TxtEmail" ValidationGroup="Submit" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                              </td>
                            </tr>
                            <tr>
                              <td></td>
                              <td align="left">
                               <%--Window Dressing--%>
                                <label for="Email2" class="blackblodtext"><asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ConfirmEmail%>" runat="server"></asp:Literal></label> 
                              </td>
                              <td>
                                <asp:TextBox ID="TxtConfirmEmail" runat="server" CssClass="altText" style="width: 200px;" MaxLength="256"></asp:TextBox>  
                                 <%--FB 1888--%>   
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="TxtConfirmEmail" ValidationGroup="Submit" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                              </td>
                            </tr>
                            <tr>
                              <td></td>
                              <td valign="top" align="left">
                               <%--Window Dressing--%>
                               <label for="AdditionalInfo" class="blackblodtext"><asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, AdditionalInfo%>" runat="server"></asp:Literal></label> 
                              </td>
                              <td>
                               <%--Window Dressing--%>
                                <asp:TextBox ID="TxtAdditionalInfo" class="altText" TextMode="MultiLine" Rows="3" runat="server"  style="width: 300px;" MaxLength="2000"></asp:TextBox> 
                                
                              </td>
                            </tr>
                            <tr>
                              <td colspan="3" style="height:5"></td>
                            </tr>
                            <tr>
                              <td></td>
                              <td colspan="2">
                                <table width="70%">
                                  <tr>
									<%--ZD 103531 - Start--%>
                                    <td align="center" width="20%">
                                        <input runat="server" id="btnback" style="width:90%" type="button" name="BtnBack" value="<%$ Resources:WebResources, Back%>" class="altShortBlueButtonFormat" onclick="javascript:return fnLoginTransfer();" />
                                    </td>
                                    <td align="center" width="25%">
                                        <input runat="server" type="reset" style="width:90%" id="btnReset" name="BtnReset" value="<%$ Resources:WebResources, Reset%>" class="altShortBlueButtonFormat" /><%--ZD 101846--%>                                         
                                    </td>
                                    <td align="center" width="25%">
                                        <%--FB 1888--%>   
                                        <asp:Button ID="BtnSubmit" OnClick="SubmitAccount" Width="90%" runat="server" ValidationGroup="Submit" Text="<%$ Resources:WebResources, Submit%>" CausesValidation="True" CssClass="altShortBlueButtonFormat" />
                                    </td>
									<%--ZD 103531 - End--%>
                                  </tr>
                                </table>
                              </td>
                            </tr>                        
                        </table>           
                     </td>
                  </tr>
             </table>
          </td>
        </tr>
      </table>
    </div>
   </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--FB 2500--%>
<%--<!-- #INCLUDE FILE="inc/mainbottom2.aspx" -->--%>
<script type="text/javascript">

    if (document.getElementById('TxtAdditionalInfo') != null)
        document.getElementById('TxtAdditionalInfo').setAttribute("onblur", "document.getElementById('btnback').focus(); document.getElementById('btnback').setAttribute('onfocus', '');");

    if (document.getElementById('btnback') != null)
        document.getElementById('btnback').setAttribute("onblur", "document.getElementById('btnReset').focus(); document.getElementById('btnReset').setAttribute('onfocus', '');");

    if (document.getElementById('btnReset') != null)
        document.getElementById('btnReset').setAttribute("onblur", "document.getElementById('BtnSubmit').focus(); document.getElementById('BtnSubmit').setAttribute('onfocus', '');");

</script>



