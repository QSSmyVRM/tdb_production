/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147  ZD 100886

using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using ExpertPdf.HtmlToPdf;
using System.Text;
using System.IO;
using System.Web.UI;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Web.UI.HtmlControls; 
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Conference
{
    public partial class ConferenceChangeRequest : System.Web.UI.Page
    {

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblConfName;
        protected System.Web.UI.WebControls.Label lblPassword;
        protected System.Web.UI.WebControls.Label lblConfUniqueID;
        protected System.Web.UI.WebControls.Label lblConfType;
        protected System.Web.UI.WebControls.Label lblLocation;
        protected System.Web.UI.WebControls.Label lblConfDate;
        protected System.Web.UI.WebControls.Label lblConfDuration;
        protected System.Web.UI.WebControls.Label lblTimezone;
        protected System.Web.UI.WebControls.Label lblFiles;
        protected System.Web.UI.WebControls.Label lblDescription;
        
        protected System.Web.UI.WebControls.Label lblUserName;
        protected System.Web.UI.WebControls.Label lblContactName;
        protected System.Web.UI.WebControls.Label lblContactEmail;
        protected System.Web.UI.WebControls.Label lblContactPhone;
        protected System.Web.UI.WebControls.Label lblAccept;
        protected System.Web.UI.WebControls.Label lblBridgeAddress;
        protected System.Web.UI.WebControls.Label lblBridgeName;
        protected System.Web.UI.WebControls.Label lblBridgeAddressType;

        protected System.Web.UI.WebControls.HiddenField txtUserID;
        protected System.Web.UI.WebControls.HiddenField txtConfID;
        protected System.Web.UI.WebControls.HiddenField txtDecision;
        protected System.Web.UI.WebControls.HiddenField hdnLocation;
        protected System.Web.UI.WebControls.HiddenField hdnLcID;

        protected System.Web.UI.WebControls.DropDownList lstTimeZone;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstInterfaceType;
        protected MetaBuilders.WebControls.ComboBox lstDuration;

        protected System.Web.UI.HtmlControls.HtmlTableRow trButtons;
        protected System.Web.UI.HtmlControls.HtmlTableRow trButtonsHeader;

        protected System.Web.UI.WebControls.RadioButton Option1;
        protected System.Web.UI.WebControls.RadioButton Option2;
        protected System.Web.UI.WebControls.RadioButton Option3;
        protected System.Web.UI.HtmlControls.HtmlTableRow trOption1; //ZD 100718
        protected System.Web.UI.HtmlControls.HtmlTableCell tdChangeReq; //ZD 100718
        
        

        myVRMNet.NETFunctions obj;
        MyVRMNet.Util utilObj; 

        String COM_ConfigPath = "C:\\VRMSchemas_v1.8.3\\COMConfig.xml";
        String MyVRMServer_ConfigPath = "C:\\VRMSchemas_v1.8.3\\";
        ns_Logger.Logger log;
        String tformat = "hh:mm tt";
        int encrypted = 0;
        protected String language = "";
        myVRMNet.ImageUtil imageUtilObj = null;
        private String SecImg1Name = "SecImage1.jpg";
        string SecTextAxis = "", SecPhotoAxis = "", SecBarCodeAxis = "", PartyphotoPath = "";
        protected String enableconferencepassword = "0";
        MyVRMNet.LoginManagement loginMgmt;

        public ConferenceChangeRequest()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            utilObj = new MyVRMNet.Util();
			imageUtilObj = new myVRMNet.ImageUtil(); 
            loginMgmt = new MyVRMNet.LoginManagement();
        }

        //ZD 101714
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        private void Page_Init(object sender, System.EventArgs e)
        {
            Application.Add("COM_ConfigPath", COM_ConfigPath);
            Application.Add("MyVRMServer_ConfigPath", MyVRMServer_ConfigPath);
        }
        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (loginMgmt == null)
                    loginMgmt = new MyVRMNet.LoginManagement();
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ConferenceChangeRequest.aspx", Request.Url.AbsoluteUri.ToLower());

                string path = Request.Url.AbsoluteUri.ToLower();
                if (path.IndexOf("%3c") > -1 || path.IndexOf("%3e") > -1)
                    Response.Redirect("ShowError.aspx");

                if (Session["language"] == null)
                    Session["language"] = "en";

                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();

                if (Request.QueryString["r"] != null)
                {
                    String locName = "";
                    locName = Request.QueryString["r"];
                    loginMgmt.simpleDecrypt(ref locName);
                    hdnLocation.Value = locName;
                }
                if (Request.QueryString["rID"] != null)
                {
                    String locID = "";
                    locID = Request.QueryString["rID"];
                    loginMgmt.simpleDecrypt(ref locID);
                    hdnLcID.Value = locID;
                }

                errLabel.Text = "";
                PartyphotoPath = Server.MapPath("../image/SecurityBadge/partyPhoto_" + txtUserID.Value + ".jpg");

                //ZD 100718 Starts
                if (Request.QueryString["cu"] != null && Request.QueryString["cu"].ToString() == "1")
                {
                    trOption1.Visible = false;
                    Option1.Checked = false;
                    Option2.Text = obj.GetTranslatedText("Choose an alternative MCU");
                    tdChangeReq.InnerText = obj.GetTranslatedText("MCU");
                }
                //ZD 100718 End

                if (!IsPostBack)
                {
                    String selTimezone = "26";
                    obj.GetTimezones(lstTimeZone, ref selTimezone);
                    obj.BindAddressType(lstAddressType);
                    //lblHeader.Text = obj.GetTranslatedText("Thank you! Your response has been recorded"); ZD 100642_11jan2014

                    Session.Add("req", Request.QueryString["req"]);
                    Session.Add("id", Request.QueryString["id"]);
                    encrypted = 1;

                    if (Session["req"] == null && Session["id"] == null)
                    {
                        Session.Add("req", Request.QueryString["req"]);
                        Session.Add("id", Request.QueryString["id"]);
                    }
                    if (Session["systemDate"] == null)
                    {
                        obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString());
                    }

                    txtUserID.Value = Session["req"].ToString();
                    txtConfID.Value = Session["id"].ToString();

                    StringBuilder inXML = new StringBuilder();
                    inXML.Append("<login>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<userID>" + txtUserID.Value + "</userID>");
                    inXML.Append("<confID>" + txtConfID.Value + "</confID>");
                    inXML.Append("<encrypted>0</encrypted>");
                    inXML.Append("<type>DR</type>");
                    inXML.Append("</login>");
                    log.Trace(inXML.ToString());

                    String outXML = obj.CallMyVRMServer("ResponseInvite", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    log.Trace("ResponseInvite : OutXML" + outXML);
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument NewXmlDoc = new XmlDocument();
                        NewXmlDoc.LoadXml(outXML);

                        if (Session["userid"] == null)
                            Session["userID"] = "11";

                        if (Session["organizationID"] == null)
                            Session["organizationID"] = "11";

                        Session["confID"] = NewXmlDoc.DocumentElement.SelectSingleNode("conferences/conference/confInfo/confID").InnerText;
                        XmlNode node = NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/lotus");
                        Session["lnLoginName"] = node.SelectSingleNode("lnLoginName").InnerText;
                        Session["lnLoginPwd"] = node.SelectSingleNode("lnLoginPwd").InnerText;
                        Session["lnDBPath"] = node.SelectSingleNode("lnDBPath").InnerText;

                        //ZD 101714
                        node = NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/userName");
                        if (node.SelectSingleNode("usrLanguage") != null)
                        {
                            string usrlanguage = "";
                            string curCulture = "";
                            usrlanguage = node.SelectSingleNode("usrLanguage").InnerText;
                            curCulture = "en-US";
                            if (usrlanguage == "3")
                                curCulture = "es-US";
                            else if (usrlanguage == "5")
                                curCulture = "fr-CA";

                            if (Session["UserCulture"] == null || (Session["UserCulture"] != null && Session["UserCulture"].ToString() != curCulture))
                            {
                                string userID = NewXmlDoc.DocumentElement.SelectSingleNode("//responseInvite/userID").InnerText;
                                Session["userID"] = userID;
                                Session["UserCulture"] = curCulture;
                                loginMgmt.LoadOrgHolidays();
                                Session["languageID"] = usrlanguage;
                                Page.Response.Redirect(Page.Request.Url.AbsoluteUri);
                            }
                        }

                        node = NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/contactDetails");
                        Application["contactName"] = node.SelectSingleNode("name").InnerText;
                        Application["contactEmail"] = node.SelectSingleNode("email").InnerText;
                        Application["contactPhone"] = node.SelectSingleNode("phone").InnerText;
                        Application["contactAddInfo"] = node.SelectSingleNode("additionInfo").InnerText;
                        
                        node = NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/userName");
                        Session["req"] = node.SelectSingleNode("userlogin").InnerText;
                        Session["role"] = node.SelectSingleNode("role").InnerText;

                        DisplayConferenceDetails(outXML);

                    }
                    else
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        trButtonsHeader.Visible = false;
                        trButtons.Visible = false;
                    }
                }
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                        errLabel.Visible = true;
                    }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }

        }
        private void DisplayConferenceDetails(String outXML)
        {
            try
            {
                String SecImgString = "", partyInvite = "";
                FileStream newFile = null;
                byte[] imgArr = null;
                
                XmlDocument xmldoc = new XmlDocument();
                outXML = outXML.Replace("& ", "&amp; ");
                xmldoc.LoadXml(outXML);
                lblConfName.Text = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/confName").InnerText;
                lblPassword.Text = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/confPassword").InnerText;
                txtConfID.Value = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/confID").InnerText;
                lblConfUniqueID.Text = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/confUniqueID").InnerText;
                lblConfType.Text = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/createBy").InnerText;
                //partyInvite = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/partyInfo/partyInvite").InnerText;
                                               
                txtUserID.Value = xmldoc.SelectSingleNode("//responseInvite/userID").InnerText;
                XmlNodeList nodes = xmldoc.SelectNodes("//responseInvite/conferences/conference/confInfo/mainLocation/location");

                if (hdnLocation.Value.Trim().Length.Equals(0))
                    lblLocation.Text = "<font class='blackblodtext'>" + obj.GetTranslatedText("No Locations") + "</font>";
                else
                    lblLocation.Text = hdnLocation.Value;

                Option1.Text = Option1.Text + " (" + hdnLocation.Value + ")";

                switch (lblConfType.Text.ToString())
                {
                    case "7": //ConferenceType.Room.ToString():
                        lblConfType.Text = obj.GetTranslatedText("Room");
                        break;
                    case "6": // ConferenceType.AudioOnly.ToString():
                        lblConfType.Text = obj.GetTranslatedText("Audio Only");
                        break;
                    case "2": // ConferenceType.AudioVideo.ToString():
                        lblConfType.Text = obj.GetTranslatedText("Audio/Video");
                        break;
                    case "4": // ConferenceType.P2P.ToString():
                        lblConfType.Text = obj.GetTranslatedText("Point-To-Point");
                        break;
                    default:
                        lblConfType.Text = "Undefined";
                        break;
                }
                String startDate = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/startDate").InnerText;
                startDate += " " + xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/startHour").InnerText;
                startDate += ":" + xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/startMin").InnerText;
                startDate += " " + xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/startSet").InnerText;

                string[] datetime = DateTime.Parse(startDate).ToString("f").Split(',');
                string date = obj.GetTranslatedText(datetime[0].ToString());
                string[] months = datetime[1].ToString().Split(' ');
                string month = obj.GetTranslatedText(months[1].ToString());
                lblConfDate.Text = date + ", " + month + " " + months[2].ToString() + ", " + datetime[2].ToString();
                
                int dur = Int32.Parse(xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/durationMin").InnerText);
                lblConfDuration.Text = (dur / 60) + " " + obj.GetTranslatedText("hrs") + " " + (dur % 60) + " " + obj.GetTranslatedText("mins");  //ZD 100528
                string tzName = xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/timeZoneName").InnerText;
                lblTimezone.Text = tzName;
                nodes = xmldoc.SelectNodes("//responseInvite/conferences/conference/confInfo/fileUpload/file");
                lblFiles.Text = "";
                foreach (XmlNode node in nodes)
                    if (!node.InnerText.Equals(""))
                    {
                
                        String fileName = getUploadFilePath(node.InnerText);
                        String fPath = node.InnerText;
                        int startIndex = fPath.IndexOf(@"\en\");
                        if ((language == "en" || fPath.IndexOf(@"\en\") > 0) && startIndex > 0)
                        {
                            fPath = fPath.Replace("\\", "/");
                            int len = fPath.Length - 1;
                
                            fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en" + fPath.Substring(startIndex + 3);//FB 1830
                        }
                        else
                            fPath = "../Image/" + fileName;

                        lblFiles.Text += "<a href='" + fPath + "' target='_blank'>" + fileName + "</a>, ";
                    }
                if (lblFiles.Text.Length > 0)
                    lblFiles.Text = lblFiles.Text.Substring(0, lblFiles.Text.Length - 2);
                else
                    lblFiles.Text = obj.GetTranslatedText("N/A");
                lblDescription.Text = utilObj.ReplaceOutXMLSpecialCharacters((xmldoc.SelectSingleNode("//responseInvite/conferences/conference/confInfo/description").InnerText), 2).Trim(); //FB 1964 //FB 2236
                if (lblDescription.Text == "N/A")
                    lblDescription.Text = obj.GetTranslatedText("N/A");
                if(lblPassword.Text.Trim() == "")
                    lblPassword.Text = obj.GetTranslatedText("N/A");
                
                lblContactName.Text = xmldoc.SelectSingleNode("//responseInvite/globalInfo/contactDetails/name").InnerText;
                lblContactEmail.Text = xmldoc.SelectSingleNode("//responseInvite/globalInfo/contactDetails/email").InnerText;
                lblContactPhone.Text = xmldoc.SelectSingleNode("//responseInvite/globalInfo/contactDetails/phone").InnerText;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("Details: " + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
       
        protected string getUploadFilePath(string fpn)
        {
            string fPath = String.Empty;
            if (fpn.Equals(""))
                fPath = "";
            else
            {
                char[] splitter = { '\\' };
                string[] fa = fpn.Split(splitter[0]);
                if (fa.Length.Equals(0))
                    fPath = "";
                else
                    fPath = fa[fa.Length - 1];
            }
            return fPath;
        }

        protected void RejectConference(Object sender, EventArgs e)
        {
            try
            {
                string inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + txtUserID.Value + "</userID><delconference><conference><confID>";
                inXML += txtConfID.Value + "</confID><reason></reason><deny>Delete</deny></conference>";
                //ZD 101255 Start  
                if (Application["Exchange"].ToString().ToUpper() == "YES")
                    inXML += "<isExchange>1</isExchange>";
                else
                    inXML += "<isExchange>0</isExchange>";
                //ZD 101255 Ends
                inXML += "</delconference></login>";
                String outXML = obj.CallMyVRMServer("DeleteConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                else
                {
                    lblHeader.Text = "";
                    errLabel.Text = obj.GetTranslatedText("Operation Successful.");
                    trButtons.Visible = false;
                    trButtonsHeader.Visible = false;
                    txtDecision.Value = "2";
                }
            }
            catch (Exception ex)
            {
                log.Trace("RejectConference" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
       
        #region SpiltAddress

        private string SpiltAddress(String address)
        {

            String[] add = null;
            try
            {
                add = new String[3];
                if (address.Contains("D") && address.Contains("+"))
                {
                    add[0] = address.Split('D')[0];//Address
                    if (address.IndexOf('+') > 0)
                    {
                        add[1] = address.Split('D')[1].Split('+')[0].Trim(); //Conference Code
                        add[2] = address.Split('D')[1].Split('+')[1].Trim(); // Leader Pin
                    }
                    else
                    {
                        add[1] = address.Split('D')[1].Trim(); //Conference Code
                        add[2] = ""; // Leader Pin
                    }
                }
                else if (address.Contains("D") && !address.Contains("+"))
                {
                    add[0] = address.Split('D')[0].Trim();//Address
                    add[1] = address.Split('D')[1].Trim(); //Conference Code
                    add[2] = ""; // Leader Pin
                }
                else if (!address.Contains("D") && !address.Contains("+"))
                {
                    add[0] = address;//Address
                    add[1] = ""; //Conference Code
                    add[2] = ""; // Leader Pin
                }
                return add[0].Trim();

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                throw ex;
            }
        }

        #endregion

        #region SubmitAction

        protected void SubmitAction(Object sender, EventArgs e)
        {
            try
            {
                if (Option1.Checked)
                {
                    String inXML = "<Conference>" + obj.OrgXMLElement() + "<userID>" + txtUserID.Value + "</userID><confID>" + txtConfID.Value + "</confID><RoomID>" + hdnLcID.Value + "</RoomID></Conference>";
                    String outXML = obj.CallMyVRMServer("CheckConferenceApprovalStatus", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                    else
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                        errLabel.Visible = true;
                    }

                }
                else if (Option2.Checked)
                {
                    Response.Redirect("genlogin.aspx?id=" + Session["id"].ToString() + "&req=" + Session["req"].ToString()
                        + "&tp=CR&rl=" + Session["role"].ToString());
                }
                else if (Option3.Checked)
                    RejectConference(null, null);
            }
            catch (Exception ex)
            {
                log.Trace("SubmitAction " + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }

        #endregion

    }
}