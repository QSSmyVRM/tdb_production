<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.		
*
* You should have received a copy of the TrueDaybook license with	
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%><%--ZD 100147 ZD 100886 End--%>

<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ViewBlockedMails" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
  {%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%}
  else
  {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%} %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
<script>

    function FnCancel() {
        window.location.replace('organisationsettings.aspx');
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" id="Head1">
    <title>View Blocked Mails</title>
</head>
<body>
    <form id="frmCustomAttribute" runat="server" method="post">
    <%--ZD 101022 start--%>
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <%--ZD 101022 End--%>
    <input type="hidden" id="helpPage" value="73" />
    <input type="hidden" id="HdnCustOptID" runat="server" value="<%=customAttrID%>" />
    <%-- ZD 102550 start--%>
    <input type="hidden" id="hdnChkDelAll" runat="server" value="0" />
    <input type="hidden" id="hdnChkRelAll" runat="server" value="0" />
    <%-- ZD 102550 End--%>
    <table width="100%">
        <tr>
            <td align="center">
                <h3>
                    <asp:Literal Text="<%$ Resources:WebResources, ViewBlockedMails_ViewBlockedMa%>"
                        runat="server"></asp:Literal></h3>
            </td>
        </tr>
        <tr>
            <td align="center" style="width: 1168px">
                <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
            </td>
        </tr>
        <!--FB 2711 Starts-->
        <tr>
            <td align="right" style="padding-right: 37px">
                <%--ZD 102550--%>
                &nbsp;
                <asp:Label ID="lbsel" class="blackblodtext" Text="<%$ Resources:WebResources, ViewBlockedMails_lbselCurrent%>"
                    runat="server"></asp:Label>
                <asp:CheckBox ID="ChkRel" runat="server" OnClick="javascript:return fnSel(this);" />&nbsp;&nbsp;&nbsp;
                <%--ZD 102550 start--%>
                <asp:Label ID="lblRelAll" class="blackblodtext" Text="<%$ Resources:WebResources, ViewBlockedMails_lbsel%>"
                    runat="server"></asp:Label>
                <asp:CheckBox ID="ChkRelAll" runat="server" OnClick="javascript:return fnSelAll(this); " />
                &nbsp;&nbsp;&nbsp;
                <asp:Label ID="lbdel" class="blackblodtext" Text="<%$ Resources:WebResources, ViewBlockedMails_lbdelCurrent%>"
                    runat="server"></asp:Label>
                <asp:CheckBox ID="Chkdel" runat="server" OnClick="javascript:return fnDel(this);" />
                &nbsp;&nbsp;&nbsp;<%-- FB 2711 --%>
                <asp:Label ID="lbldelAll" class="blackblodtext" Text="<%$ Resources:WebResources, ViewBlockedMails_lbdel%>"
                    runat="server"></asp:Label>
                <asp:CheckBox ID="ChkdelAll" runat="server" OnClick="javascript:return fnDelAll(this);" />
                <%--ZD 102550 End--%>
            </td>
        </tr>
        <!--FB 2711 Starts-->
        <tr style="display: none;">
            <td align="right">
                <asp:LinkButton ID="LnkDeleteAll" runat="server" OnClientClick="javascript:return Deleteall();"
                    OnClick="DeleteAllBlockMail" Text="<%$ Resources:WebResources, ViewBlockedMails_LnkDeleteAll%>"
                    CssClass="tabtreeRootNode"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dgBlockEmail" runat="server" AutoGenerateColumns="False" CellPadding="3"
                    GridLines="None" AllowSorting="true" BorderColor="blue" BorderStyle="solid" BorderWidth="1"
                    ShowFooter="False" Width="95%" CellSpacing="1" OnEditCommand="EditBlockEmail"
                    Visible="true" Style="border-collapse: separate" AllowPaging="true" PageSize="7"
                    OnPageIndexChanged="BindBlockEmail" PagerStyle-HorizontalAlign="Right">
                    <SelectedItemStyle CssClass="tableBody" />
                    <AlternatingItemStyle CssClass="tableBody" />
                    <ItemStyle CssClass="tableBody" />
                    <HeaderStyle CssClass="tableHeader" Height="30px" />
                    <EditItemStyle CssClass="tableBody" />
                    <FooterStyle CssClass="tableBody" />
                    <PagerStyle CssClass="tableBody" Visible="true" Wrap="False" HorizontalAlign="Right"
                        ForeColor="Blue" Mode="NumericPages" Position="Bottom" PageButtonCount="7" />
                    <Columns>
                        <asp:BoundColumn DataField="UUID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                            Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Iscalendar" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                            Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="RowID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                            HeaderText="<%$ Resources:WebResources, SNo%>"></asp:BoundColumn>
                        <asp:BoundColumn DataField="From" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                            HeaderText="<%$ Resources:WebResources, FromImg%>"></asp:BoundColumn>
                        <%--ZD 100300--%>
                        <asp:BoundColumn DataField="To" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                            HeaderText="<%$ Resources:WebResources, EditBlockEmail_To%>"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Subject" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Subject%>">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Message" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, approvalstatus_Message%>">
                        </asp:BoundColumn>
                        <asp:BoundColumn ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, iCal%>">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ViewBlockedMails_btnEdit%>"
                            HeaderStyle-HorizontalAlign="center">
                            <HeaderStyle CssClass="tableHeader" />
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Text="<%$ Resources:WebResources, ViewBlockedMails_btnEdit%>"
                                    ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Release%>" HeaderStyle-HorizontalAlign="center">
                            <HeaderStyle CssClass="tableHeader" />
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkRelease" onClick="javascript:ChangeRelease(this)" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Delete%>" HeaderStyle-HorizontalAlign="center">
                            <HeaderStyle CssClass="tableHeader" />
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkDelete" onClick="javascript:ChangeDelete(this)" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <asp:Table runat="server" ID="tblNoBlockMail" Visible="false" Width="90%">
                    <asp:TableRow CssClass="lblError">
                        <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                               <font class="lblError"> <asp:Literal Text="<%$ Resources:WebResources, ViewBlockedMails_NoblockeMails%>" runat="server"></asp:Literal></font>  
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" align="center">
                    <tr>
                        <td align="right">
                            <%--ZD 100420--%>
                            <button runat="server" id="btnCancel" class="altMedium0BlueButtonFormat" onserverclick="RedirectToTargetPage">
                                <asp:Literal Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button>
                        </td>
                        <td width="2%">
                        </td>
                        <td>
                            <button runat="server" style="width: 100pt" id="btnSubmit" onserverclick="DeleteBlockMail">
                                <asp:Literal Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button>
                            <%--FB 2796--%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
<script type="text/javascript">
    // Release Email
    //FB2711 starts
    function fnSel(drpdown) {
        var str = document.getElementById("ChkRel");
        var s = document.getElementById("Chkdel");
        //ZD  102550 starts
        var ChkdelAll = document.getElementById("ChkdelAll");
        var ChkRelAll = document.getElementById("ChkRelAll");
        document.getElementById("hdnChkRelAll").value = "0";
        document.getElementById("hdnChkDelAll").value = "0";
        var i = 3;
        if (str.checked) {
            s.checked = false;
            ChkdelAll.checked = false;
            ChkRelAll.checked = false;
        }
        //ZD  102550 End
        var ird;
        while (i < 10) {
            ird = "dgBlockEmail_ctl0" + i + "_chkRelease";
            isd = "dgBlockEmail_ctl0" + i + "_chkDelete";
            if (idd = document.getElementById(ird) == null)
                break;
            var idd = document.getElementById(ird);
            var ifd = document.getElementById(isd);
            if (str.checked) {
                idd.checked = true;
                ifd.checked = false;
            }
            else {
                idd.checked = false;

            }
            i++;
        }
    }

    function fnDel(drpdown) {
        var str = document.getElementById("Chkdel");
        var s = document.getElementById("ChkRel");
        //ZD 102550 starts
        var ChkdelAll = document.getElementById("ChkdelAll");
        var ChkRelAll = document.getElementById("ChkRelAll");
        document.getElementById("hdnChkRelAll").value = "0";
        document.getElementById("hdnChkDelAll").value = "0";
        var i = 3;
        if (str.checked) {
            s.checked = false;
            ChkdelAll.checked = false; //ZD  102550
            ChkRelAll.checked = false; //ZD  102550
        }
        //ZD 102550 End
        var ird;
        if (str.checked)
            s.checked = false;
        while (i < 10) {
            ird = "dgBlockEmail_ctl0" + i + "_chkDelete";
            isd = "dgBlockEmail_ctl0" + i + "_chkRelease";
            if (idd = document.getElementById(ird) == null)
                break;
            var idd = document.getElementById(ird);
            var ifd = document.getElementById(isd);
            if (str.checked) {
                idd.checked = true;
                ifd.checked = false;
            }
            else
                idd.checked = false;
            i++;
        }
        //FB2711 ends
    }

    //ZD 102550 start
    function fnSelAll(drpdown) {
        var str = document.getElementById("ChkRelAll");
        var s = document.getElementById("ChkdelAll");
        var Chkdel = document.getElementById("Chkdel"); //ZD  102550
        var ChkRel = document.getElementById("ChkRel"); //ZD  102550
        var i = 3;
        if (str.checked) {
            s.checked = false;
            Chkdel.checked = false; //ZD  102550
            ChkRel.checked = false; //ZD  102550
            document.getElementById("hdnChkRelAll").value = "1"; //ZD 102550
            document.getElementById("hdnChkDelAll").value = "0"; //ZD 102550
        }
        else {
            document.getElementById("hdnChkRelAll").value = "0"; //ZD 102550
            document.getElementById("hdnChkDelAll").value = "0"; //ZD 102550
        }
        var ird;
        while (i < 10) {
            ird = "dgBlockEmail_ctl0" + i + "_chkRelease";
            isd = "dgBlockEmail_ctl0" + i + "_chkDelete";
            if (idd = document.getElementById(ird) == null)
                break;
            var idd = document.getElementById(ird);
            var ifd = document.getElementById(isd);
            if (str.checked) {
                idd.checked = true;
                ifd.checked = false;
            }
            else {
                idd.checked = false;

            }
            i++;
        }
    }

    function fnDelAll(drpdown) {
        var str = document.getElementById("ChkdelAll");
        var s = document.getElementById("ChkRelAll");
        var Chkdel = document.getElementById("Chkdel"); //ZD  102550
        var ChkRel = document.getElementById("ChkRel"); //ZD  102550
        var i = 3;
        if (str.checked) {
            s.checked = false;
            Chkdel.checked = false; //ZD  102550
            ChkRel.checked = false; //ZD  102550
            document.getElementById("hdnChkDelAll").value = "1"; //ZD 102550
            document.getElementById("hdnChkRelAll").value = "0"; //ZD 102550
        }
        else {
            document.getElementById("hdnChkRelAll").value = "0"; //ZD 102550
            document.getElementById("hdnChkDelAll").value = "0"; //ZD 102550
        }
        var ird;
        while (i < 10) {
            ird = "dgBlockEmail_ctl0" + i + "_chkDelete";
            isd = "dgBlockEmail_ctl0" + i + "_chkRelease";
            if (idd = document.getElementById(ird) == null)
                break;
            var idd = document.getElementById(ird);
            var ifd = document.getElementById(isd);
            if (str.checked) {
                idd.checked = true;
                ifd.checked = false;
            }
            else {
                idd.checked = false;
            }
            i++;
        }
    }
    // ZD 102550 End
    function ChangeRelease(drpdwm) {
        var str = drpdwm.id;
        var flag = 0; //FB 2711
        var ird; //FB 2711
        var i; //FB 2711
        str = str.replace("chkRelease", "chkDelete");
        var dropdwn = document.getElementById(str);
        //FB 2711 Start
        var s = document.getElementById("ChkRel"); //FB2711
        var s2s = document.getElementById("Chkdel"); //FB2711
        //ZD 102550 starts
        var Delall = document.getElementById("ChkdelAll");
        var RelAll = document.getElementById("ChkRelAll");
        document.getElementById("hdnChkRelAll").value = "0"; //ZD 102550
        document.getElementById("hdnChkDelAll").value = "0"; //ZD 102550
        //ZD 102550 End
        if (drpdwm.checked) {
            for (i = 3; i < 10; i++) {
                var isd = "dgBlockEmail_ctl0" + i + "_chkRelease";
                var idd = document.getElementById(isd);
                if (idd != null && idd.checked)//ZD 102550
                    flag++;
            }
        }

        if (flag == 7)
            s.checked = true;

        //FB 2711 Ends
        if (dropdwn) {
            if (drpdwm.checked) {
                if (dropdwn.checked) {
                    dropdwn.checked = false;
                    s2s.checked = false; //FB2711
                    Delall.checked = false; //ZD 102550
                }
            }
            else {
                s.checked = false; //FB2711
                RelAll.checked = false; //ZD 102550
            }

        }

    }

    function ChangeDelete(drpdwm) {
        var str = drpdwm.id;
        var flag = 0; //FB 2711
        var i; //FB 2711
        str = str.replace("chkDelete", "chkRelease");
        var dropdwn = document.getElementById(str);
        //FB 2711 Starts
        var s = document.getElementById("Chkdel");
        var s2s = document.getElementById("ChkRel");
        var Delall = document.getElementById("ChkdelAll"); //ZD 102550
        var RelAll = document.getElementById("ChkRelAll"); //ZD 102550
        document.getElementById("hdnChkRelAll").value = "0"; //ZD 102550
        document.getElementById("hdnChkDelAll").value = "0"; //ZD 102550
        if (drpdwm.checked) {
            for (i = 3; i < 10; i++) {
                var isd = "dgBlockEmail_ctl0" + i + "_chkDelete";
                var idd = document.getElementById(isd);
                if (idd != null && idd.checked)//ZD 102550
                    flag++;
            }
        }

        if (flag == 7)
            s.checked = true;



        //FB 2711 Ends
        if (dropdwn) {
            if (drpdwm.checked) {
                if (dropdwn.checked) {
                    dropdwn.checked = false;
                    s2s.checked = false; //FB2711
                    RelAll.checked = false; //ZD 102550
                }
            }
            else {
                s.checked = false; //FB2711
                Delall.checked = false; //ZD 102550
            }

        }
    }

    function Deleteall(drpdown) {
        if (confirm("Are you sure to delete all blocked emails displayed. This operation is irreversible."))
            return true;

        return false;

    }

    //ZD 102550 start
    function Releaseall(drpdown) {
        if (confirm("Are you sure to Release all blocked emails displayed. This operation is irreversible."))
            return true;
        return false;
    }
    //ZD 102550 End

    document.onkeydown = function (evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };
    // Release Email
</script>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
