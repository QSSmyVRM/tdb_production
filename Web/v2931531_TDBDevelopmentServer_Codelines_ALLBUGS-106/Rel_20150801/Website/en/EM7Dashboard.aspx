<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.
*
* You should have received a copy of the TrueDaybook license with
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_EM7Dashboard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--FB 2779--%>
<!--window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
</script>

<script type="text/javascript" src="script/mousepos.js"></script>

<script type="text/javascript" src="script/managemcuorder.js"></script>



<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, EM7Dashboard_EM7Dashboard%>" runat="server"></asp:Literal></title>
</head>
<body>
    <form id="form1" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div style="height: 600px">
        <table>
            <tr>
                <td>
                    <h3>
                        <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, EM7Dashboard_EM7Dashboard%>" runat="server"></asp:Literal></h3>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LblError" ForeColor="red" runat="server" Font-Bold="true" Font-Size="small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">
                
                  <asp:Image ID="Image1" src="image/info.png" AlternateText ="Info" runat="server" style="cursor:default" /> <%--ZD 100419--%><%-- ZD 102590 --%>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <iframe id="Ifrm_EM7" runat="server" width="990" height="500" scrolling="yes">
                    </iframe>
                </td>
            </tr>
        </table>
    </div>
    </form>
    <%--code added for Soft Edge button--%>

    <script type="text/javascript" src="inc/softedge.js"></script>

    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
