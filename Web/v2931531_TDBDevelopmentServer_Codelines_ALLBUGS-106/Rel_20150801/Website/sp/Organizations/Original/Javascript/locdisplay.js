/*ZD 100147 Start*/
/* Copyright (C) 2015 TrueDaybook - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.
*
* You should have received a copy of the TrueDaybook license with
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/
/*ZD 100147 ZD 100866 End*/
var RoomDisplayMod = 1;

function chgRoomDisplay(locpghref, mod)
{
	mod = parseInt(mod);
	if (mod != RoomDisplayMod) {
		RoomDisplayMod = mod;
		ifrmLocation.location.href = locpghref + "&mod=" + mod
	}
}
