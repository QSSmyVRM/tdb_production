<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.	
*
* You should have received a copy of the TrueDaybook license with	
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ManageMessages" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->

<script type="text/javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    function fnDelete(arg)
    {  
        if(arg == true)
            return false;

        var msg = RSconfirmDelMsg;
        if (confirm(msg))
            return true;
        else
            return false;            
    }

    function fnClear()
    {
        var lblError = document.getElementById("lblError");
        lblError.innerText = "";
    }

    ExpandCollapse(document.getElementById("imgLangsOption"), "<%=LangsOptionRow.ClientID %>", true);

    function maxCharRowShow() 
    {
        document.getElementById("txtMaxChar").style.display = "block";
    }

    function ExpandCollapse(img, str, frmCheck) 
    {
        obj = document.getElementById(str);
        if (obj != null) {
            if (frmCheck == true) {
                img.src = img.src.replace("minus", "plus");
                obj.style.display = "none";
            }
            if (frmCheck == false) {
                if (img.src.indexOf("minus") >= 0) {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                }
                else {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                }
            }
        }
    }
    
    function frmValidator() {
        document.getElementById("txtMaxChar").style.display = "none";
        var txtentityname = document.getElementById('<%=txtTxtMsg.ClientID%>');
        if (txtentityname.value == "") {
            reqEntityName.style.display = 'block';
            txtentityname.focus();
            return false;
        } 
        else if (txtentityname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/) == -1) {
            regItemName1.style.display = 'block';
            txtentityname.focus();
            return false;
        }
        return (true);
    }

    function fnCancel() //FB 2565
    {
        DataLoading(1); //ZD 100176
        window.location.replace('OrganisationSettings.aspx');  //CSS Project
    }
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176 End

    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Messages</title>

    <script type="text/javascript" src="script/mousepos.js"></script>

    <script type="text/javascript" src="script/managemcuorder.js"></script>

    <script type="text/javascript" src="inc/functions.js"></script>

    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />--%>
</head>
<body>
    <form id="form1" runat="server" onsubmit="DataLoading(1);"><%--ZD 100176--%> 
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
        <input type="hidden" id="helpPage" value="65" />
        <table width="50%" align="center">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="<%$ Resources:WebResources, ManageMessages_lblHeader%>"></asp:Label>
                        <div id="dataLoadingDIV" style="display:none" align="center" >
                            <img border='0' src='image/wait1.gif'  alt="<asp:Literal Text='<%$ Resources:WebResources, Loading%>' runat='server' />" />
                        </div> <%--ZD 100678 End--%>
                    </h3><%--FB 2934--%>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblError" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr id="trTextMsg" runat="server" align="center">
                <td align="center">
                    <table width="755px" cellspacing="0" cellpadding="2" border="0">
                        <tr style="display:none;">
                            <td align="left" class="subtitleblueblodtext" colspan="4"><asp:Literal Text="<%$ Resources:WebResources, ManageMessages_TextMessages%>" runat="server"></asp:Literal><br />
                            </td>
                        </tr>
                        <tr style="display:none;">
                            <td colspan="4"  >
                                <table class="tableHeader" border="0" width="100%">
                                    <tr>
                                        <td style="width: 135px" align="left" valign="top" class="blackblodtext">
                                            <b><asp:Literal Text="<%$ Resources:WebResources, ManageMessages_LanguageName%>" runat="server"></asp:Literal></b>
                                        </td>
                                        <td align="left" valign="top" style="width: 230px" class="blackblodtext">
                                            <b><asp:Literal Text="<%$ Resources:WebResources, ManageMessages_Message%>" runat="server"></asp:Literal></b><span class="reqfldstarText">*</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 13px; display:none;">
                                <br />
                                <asp:ImageButton ID="imgLangsOption" runat="server" ImageUrl="image/loc/nolines_minus.gif"  AlternateText="<%$ Resources:WebResources, SettingSelect2_Expand%>"
                                    Height="20" Width="20" vspace="0" hspace="0" ToolTip="<%$ Resources:WebResources, ViewOtherLanguages%>" /> <%--ZD 100419--%>
                            </td>
                            <td style="width: 130.5px" align="left" valign="top">
                                <br />
                                <a class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageMessages_EnterMessage%>" runat="server"></asp:Literal></a>
                            </td>
                            <td align="left" valign="top" >
                                <br />
                                <asp:TextBox ID="txtTxtMsgID" Text="<%$ Resources:WebResources, ManageMessages_txtTxtMsgID%>" Visible="false" runat="server" Width="32px"></asp:TextBox>
                                <asp:TextBox ID="txtTxtMsg" runat="server" Width="470px" CssClass="altText" ValidationGroup="Upload" MaxLength="180"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqTxtMs" ValidationGroup="Upload" runat="server"
                                    ControlToValidate="txtTxtMsg" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regTxtMs" ControlToValidate="txtTxtMsg" Display="dynamic"
                                   ValidationGroup="Upload" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                    ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                            </td>                           
                        </tr>
                        <tr id="LangsOptionRow" runat="server" style="display:none;">
                            <td>
                            </td>
                            <td colspan="2" style="text-align: left">
                                <asp:DataGrid ID="dgLangOptionlist" runat="server" AutoGenerateColumns="false" GridLines="None"
                                    CellPadding="1" CellSpacing="0" Style="border-collapse: separate" ShowHeader="false">
                                    <ItemStyle Height="2" VerticalAlign="Top" />
                                    <Columns>
                                        <asp:TemplateColumn ItemStyle-CssClass="blackblodtext" ItemStyle-Width="131px" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblLangID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ID") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblLangName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn ItemStyle-Width="230px" ItemStyle-Height="30px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtMsgID" Text="<%$ Resources:WebResources, ManageMessages_txtMsgID%>" Visible="false" runat="server" Width="32px"></asp:TextBox>
                                                <asp:TextBox ID="txtMsg" runat="server" Width="470px" CssClass="altText" MaxLength="180"
                                                    Text=""></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtMsg" Display="dynamic"
                                                   ValidationGroup="Upload" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                                    ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                 <br />
                                <asp:Button id="btnNewMessage" runat="server" validationgroup="Upload" onclientclick="javascript:fnClear()" text="<%$ Resources:WebResources, ManageMessages_btnNewMessage%>" width="180px" onclick="SetMessages"></asp:Button> <%--FB 2796--%>
                            </td>
                        </tr>
                        <tr>
                            <td id="txtMaxChar" runat="server" colspan="4" style="font-size: xx-small; color: Red;display: none" align="right"><asp:Literal Text="<%$ Resources:WebResources, ManageMessages_txtMaxChar%>" runat="server"></asp:Literal></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="width: 100%" align="center">
                <td align="center" style="width: 100%">
                    <table border="0" style="width: 755px" cellpadding="2" cellspacing="0">
                        <tr id="trTxtMsgDisplay" align="left" runat="server" style="width: 100%">
                            <td align="left" style="width: 100%">
                                <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgTxtMsg" OnPageIndexChanged="dgTxtMsg_PageIndexChanged"
                                    AllowPaging="true" PageSize="10" PagerStyle-HorizontalAlign="Right" PagerStyle-Mode="NumericPages" 
                                    AutoGenerateColumns="false" OnEditCommand="EditItem" OnDeleteCommand="DeleteItem"
                                    OnCancelCommand="CancelItem" OnUpdateCommand="UpdateItem" runat="server" Width="750Px"
                                    GridLines="None" Style="border-collapse: separate" OnItemCreated="BindRowsDeleteMessage"
                                    OnItemDataBound="BindMessages" >
                                    <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Left" /> <%--ZD 100425--%>
                                    <AlternatingItemStyle CssClass="tableBody" />
                                    <ItemStyle CssClass="tableBody" />
                                    <FooterStyle CssClass="tableBody" />
                                    <Columns>
                                        <asp:TemplateColumn>
                                             <ItemTemplate>            
                                                 <asp:Label ID="lblID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>   
                                                 <asp:Label ID="lblMsgID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MsgID") %>' Visible="false"></asp:Label>   
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn ItemStyle-CssClass="blackblodtext" HeaderStyle-CssClass="tableHeader"
                                            ItemStyle-Width="143" HeaderText="<%$ Resources:WebResources, ManageCustomAttribute_LanguageName%>" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                            <ItemTemplate>            
                                                 <asp:Label ID="lblLangID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LangID") %>'
                                                    Visible="false"></asp:Label>                                    
                                                <asp:Label ID="lblLangName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LangName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ManageMessages_lblHeader%>" HeaderStyle-CssClass="tableHeader"
                                            ItemStyle-CssClass="tableBody"><%--FB 2934--%>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTxtMsg" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TxtMsg") %>'></asp:Label>                                                
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTxtMsg" Width="300px" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TxtMsg") %>'
                                                MaxLength="180" > </asp:TextBox>                                                
                                                <asp:RequiredFieldValidator ID="reqName" ValidationGroup="Update" runat="server"
                                                    ControlToValidate="txtTxtMsg" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtTxtMsg" Display="dynamic"
                                                    ValidationGroup="Update" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                                    ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                            </EditItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"
                                        ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:LinkButton id="btnEdit" text="<%$ Resources:WebResources, ManageMessages_btnEdit%>" commandname="Edit" runat="server"></asp:LinkButton>
                                                <asp:LinkButton id="btnDelete" text="<%$ Resources:WebResources, ManageMessages_btnDelete%>" commandname="Delete" runat="server"></asp:LinkButton>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:LinkButton id="btnUpdate" text="<%$ Resources:WebResources, ManageMessages_btnUpdate%>" commandname="Update" runat="server" validationgroup="Update"></asp:LinkButton>
                                                <asp:LinkButton id="btnCancel" text="<%$ Resources:WebResources, ManageMessages_btnCancel%>" commandname="Cancel" runat="server"></asp:LinkButton>
                                            </EditItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            <td align="right">
            <input type="button" id="btnGoBack" class="altMedium0BlueButtonFormat" value="<%$ Resources:WebResources, ManageTier2_btnGoBack%>" runat="server" onclick="fnCancel()" runat="server" /> <%--FB 2565--%><%--ZD 100428--%>
            </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
