/*ZD 100147 Start*/
/* Copyright (C) 2015 TrueDaybook - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.
*
* You should have received a copy of the TrueDaybook license with
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/
/*ZD 100147 ZD 100886 End*/
// ** I18N

// Calendar EN language
// Author: Mihai Bazon, <mishoo@infoiasi.ro>
// Encoding: any
// Distributed under the same terms as the calendar itself.

// For translators: please use UTF-8 if possible.  We strongly believe that
// Unicode is the answer to a real internationalized world.  Also please
// include your contact information in the header, as can be seen above.

// full day names
Calendar._DN = new Array
("Domingo",
 "Lunes",
 "Martes",
 "Miercoles",
 "Jueves",
 "Viernes",
 "Sabado",
 "Domingo");

// Please note that the following array of short day names (and the same goes
// for short month names, _SMN) isn't absolutely necessary.  We give it here
// for exemplification on how one can customize the short day names, but if
// they are simply the first N letters of the full name you can simply say:
//
//   Calendar._SDN_len = N; // short day name length
//   Calendar._SMN_len = N; // short month name length
//
// If N = 3 then this is not needed either since we assume a value of 3 if not
// present, to be compatible with translation files that were written before
// this feature.

// short day names
Calendar._SDN = new Array
("Sol",
 "Lun",
 "Mar",
 "Cas",
 "Jue",
 "Vie",
 "S\u00e1b",
 "Sol");

// full month names
Calendar._MN = new Array
("Enero",
 "Febrero",
 "Marzo",
 "Abril",
 "Mayo",
 "Junio",
 "Julio",
 "Agosto",
 "Septiembre",
 "Octubre",
 "Noviembre",
 "Diciembre");

// short month names
Calendar._SMN = new Array
("Jan",
 "Feb",
 "Mar",
 "Abr",
 "May",
 "Jun",
 "Jul",
 "Ago",
 "Sep",
 "Oct",
 "Nov",
 "Dic");

// tooltips
Calendar._TT = {};
Calendar._TT["INFO"] = "Acerca del calendario";

Calendar._TT["ABOUT"] =
//"DHTML Date/Time Selector\n" +
//"(c) dynarch.com 2002-2003\n" + // don't translate this this ;-)
//"For latest version visit: http://dynarch.com/mishoo/calendar.epl\n" +
//"Distributed under GNU LGPL.  See http://gnu.org/licenses/lgpl.html for details." +
//"\n\n" +
"Fecha de selecci\u00f3n:\n" +
//ZD 102407 Starts
"- Haga clic en una fecha para seleccionarla.\n"+
"- Utilice los botones \xab y \xbb para desplazarse entre a\u00f1os.\n" +
"- Utilice los botones " + String.fromCharCode(0x2039) + " y " + String.fromCharCode(0x203a) + " para desplazarse entre meses.\n" +
"- Mantenga pulsado el bot\u00f3n izquierdo del rat\u00f3n sobre cualquiera de los botones de arriba para acceder a una lista de a\u00f1os / meses para la selecci\u00f3n r\u00e1pida.\n" 
//ZD 102407 End
Calendar._TT["ABOUT_TIME"] = "\n\n" +
"Tiempo de selecci\u00f3n:\n" +
"- Haga clic en cualquiera de las partes de la hora para aumentar\n" +
"- o May\u00fas y haga clic para disminuirlo\n" +
"- o haga clic y arrastre para una selecci\u00f3n m\u00e1s r\u00e1pida.";

Calendar._TT["PREV_YEAR"] = "Prev. a\u00f1o (mantener para men\u00fa)";
Calendar._TT["PREV_MONTH"] = "Prev. meses (mantener para men\u00fa))";
Calendar._TT["GO_TODAY"] = "Ir a Hoy";//ZD 102421
Calendar._TT["NEXT_MONTH"] = "Mes siguiente (mantener para men\u00fa)";
Calendar._TT["NEXT_YEAR"] = "A\u00f1o siguiente (mantener para men\u00fa)";
Calendar._TT["SEL_DATE"] = "Seleccione la fecha de";
Calendar._TT["DRAG_TO_MOVE"] = "Mostrar% s primero";
Calendar._TT["PART_TODAY"] = " (hoy)";

// the following is to inform that "%s" is to be the first day of week
// %s will be replaced with the day name.
Calendar._TT["DAY_FIRST"] = "Mostrar %s Primero";

// This may be locale-dependent.  It specifies the week-end days, as an array
// of comma-separated numbers.  The numbers are from 0 to 6: 0 means Sunday, 1
// means Monday, etc.
Calendar._TT["WEEKEND"] = "0,6";

Calendar._TT["CLOSE"] = "Cerrar";
Calendar._TT["TODAY"] = "Hoy";
Calendar._TT["TIME_PART"] = "(Shift-)Click or drag to change value";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "%Y-%m-%d";
Calendar._TT["TT_DATE_FORMAT"] = "%a, %b %e";

Calendar._TT["WK"] = "sem";
Calendar._TT["TIME"] = "Tiempo:";
