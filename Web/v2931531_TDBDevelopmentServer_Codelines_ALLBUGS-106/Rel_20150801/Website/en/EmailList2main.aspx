<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.
*
* You should have received a copy of the TrueDaybook license with
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="ns_EmailListMain.en_EmailList2main" ValidateRequest="false"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--ZD 101028 --%>

<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830

    }
    
%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <title>TrueDaybook</title>
   <meta name="Description" content="TrueDaybook (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment." />
  <meta name="Keywords" content="TrueDaybook, TrueDaybook, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <script type="text/javascript">      // FB 2790
      var path = '<%=Session["OrgCSSPath"]%>';
      path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
      document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
  </script>
    <script type="text/javascript" src="script/errorList.js"></script>
    <script type="text/javascript" src="extract.js"></script>
    <script type="text/javascript" src="inc/functions.js"></script> <%-- ZD 102723 --%>
    <script type="text/javascript" src="sorttable.js"></script> 
    <script language="JavaScript" src="script/group2.js"></script>
    <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
 <script type="text/javascript" language="javascript">
    
<!--
var tabs=new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","ALL")

function chgpageoption (totalpagenum)
{
	tpn = parseInt(totalpagenum, 10)
	cb = document.frmEmaillist2main.SelPage;
		
	RemoveAllOptions (cb);
		
	for (var i=1; i<=tpn; i++) {
		addopt(cb, i, i, false, false);
	}
}	

function nameimg(prename, sel)
{
	for (i in tabs) {
		document.getElementById(prename + tabs[i]).style.backgroundColor = ""; 
	}
	document.getElementById(prename + sel.toUpperCase()).style.backgroundColor = "#FF6699"; 
}	
	
function pageimg(selectedpn)
{
	document.frmEmaillist2main.pageno.value = selectedpn;
		
	if (document.frmEmaillist2main.SelPage.options.length == 1)
		document.frmEmaillist2main.SelPage.options.selected = true;
	else
		document.frmEmaillist2main.SelPage.options[parseInt(selectedpn, 10)-1].selected = true;
}

function seltype(ifrmname, sb)
{
    	
	document.frmEmaillist2main.sortby.value = sb;
		
	document.frmEmaillist2main.pageno.value = 1;
	if (document.frmEmaillist2main.SelPage.options.length>0)
		document.frmEmaillist2main.SelPage.options[0].selected = true; 

	cmd = ( (queryField("srch") == "y") ? "RetrieveUsers" : ( (queryField("t") == "g") ? "GetGuestList" : "GetEmailList" ) );
	url = "emaillist2.aspx?cmd=" + cmd + "&audioid=" + queryField("audioid") + "&frm=" + document.frmEmaillist2main.frm.value + "&frmname=" + queryField("fn") + "&fn=" + queryField("fn")+ "&n=" + queryField("n"); //FB 1779	//ZD 101547
	url += "&sb=" + document.frmEmaillist2main.sortby.value + "&a=" + document.frmEmaillist2main.alphabet.value + "&pn=" + document.frmEmaillist2main.pageno.value;
	url += "&fname=" + document.frmEmaillist2main.FirstName.value + "&lname=" + document.frmEmaillist2main.LastName.value + "&t=" + document.frmEmaillist2main.t.value;


	//eval(ifrmname).window.location.href = url; // FB 2050
	document.getElementById(ifrmname).src = url; // FB 2050
	
}

function selname(ifrmname, sel)
{
	document.frmEmaillist2main.alphabet.value = sel;
	document.frmEmaillist2main.pageno.value = 1;
	if (document.frmEmaillist2main.SelPage.options.length>0)
		document.frmEmaillist2main.SelPage.options[0].selected = true;
	cmd = ( (queryField("srch") == "y") ? "RetrieveUsers" : ( (queryField("t") == "g") ? "GetGuestList" : "GetEmailList" ) );
	url = "emaillist2.aspx?cmd=" + cmd + "&audioid=" + queryField("audioid") + "&frm=" + document.frmEmaillist2main.frm.value + "&frmname=" + queryField("fn") + "&fn=" + queryField("fn") + "&n=" + queryField("n");//FB 1779
	url += "&sb=" + document.frmEmaillist2main.sortby.value + "&a=" + document.frmEmaillist2main.alphabet.value + "&pn=" + document.frmEmaillist2main.pageno.value;
	url += "&fname=" + document.frmEmaillist2main.FirstName.value + "&lname=" + document.frmEmaillist2main.LastName.value + "&t=" + document.frmEmaillist2main.t.value;
	//eval(ifrmname).window.location.href = url; // FB 2050
	document.getElementById(ifrmname).src = url; // FB 2050
}

function selpage(ifrmname, cb)
{
	document.frmEmaillist2main.pageno.value = cb.options[cb.selectedIndex].value;

	cmd = ( (queryField("srch") == "y") ? "RetrieveUsers" : ( (queryField("t") == "g") ? "GetGuestList" : "GetEmailList" ) );
	//url  = "dispatcher/conferencedispatcher.asp?cmd=" + cmd + "&frm=" + document.frmEmaillist2main.frm.value + "&frmname=" + queryField("fn") + "&n=" + queryField("n");
	url = "emaillist2.aspx?cmd=" + cmd + "&audioid=" + queryField("audioid") + "&frm=" + document.frmEmaillist2main.frm.value + "&frmname=" + queryField("fn") + "&fn=" + queryField("fn") + "&n=" + queryField("n"); //FB 1779 //ZD 101547
	url += "&sb=" + document.frmEmaillist2main.sortby.value + "&a=" + document.frmEmaillist2main.alphabet.value + "&pn=" + document.frmEmaillist2main.pageno.value;
	//Code Modified on 12Mar09 -FB 412- start
	url += "&fname=" + document.frmEmaillist2main.FirstName.value + "&lname=" + document.frmEmaillist2main.LastName.value + "&t=" + document.frmEmaillist2main.t.value + "&srch=" + queryField("srch");
	//Code Modified on 12Mar09 -FB 412- End

	//eval(ifrmname).window.location.href = url; // FB 2050
	document.getElementById(ifrmname).src = url; // FB 2050
}


function tabAllshow(needshow)
{
	if (queryField("srch") != "y")
		document.getElementById ("tabAll").style.display =(parseInt(needshow,10)) ? "" : "none"; // ZD 101722
}

if (opener && opener.closed) {
	alert(EN_88);
	window.close;
} else {
	var isCreate, isFu, isRm, isFuCreate, isRmCreate;
	if (opener.document.frmSettings2) {
		isCreate = ( (opener.document.frmSettings2.ConfID.value == "new") ? true : false ) ;
		isFu = ( (opener.document.frmSettings2.CreateBy.value == "1") ? true : false ) ;
		isRm = ( ((parent.opener.document.frmSettings2.CreateBy.value == "2") || (parent.opener.document.frmSettings2.CreateBy.value == "7")) ? true : false ) ;

		isFuCreate = (isFu && isCreate)
		isRmCreate = (isRm && isCreate)
	} else {
		if (opener.document.location.href.indexOf(".aspx") <= 0)
          if ((!opener.document.frmManagegroup2) && (!opener.document.frmReplaceuser) && 
		    (!opener.document.frmMainroom) && (!opener.document.frmBridgesetting) && 
		    (!opener.document.frmMainsuperadministrator) && (!opener.document.frmManagedept) && (!opener.document.frmSettings2Report)) {
			alert(EN_88);
			window.close();
		}
		
	}
}
function viewVRM ()
{
	if (opener.getYourOwnEmailList) {
	
		opener.getYourOwnEmailList(999);
	} else {
		alert(EN_88);
		window.close();
	}
}
//ZD 102587 - Start
function viewGuest() 
{
    if (opener.getGuest != null)
    {	
		opener.getGuest();
    }
    if (parent.getGuest != null) 
    {    
        parent.getGuest();
    }
    else 
    {    
        alert(EN_88);
        window.close();
    }
}
//ZD 102587 - End
function Sort(id)
{
   
	ifrmList.sortlist(id);
}
function Reset()
{
	ifrmList.Reset();
}

function GoToSearch()
{
var url = "";

//url = "emailsearch.aspx?t="+ document.frmEmaillist2main.t.value +"&frm="+ document.frmEmaillist2main.frm.value +"&fn="+queryField('fn')+"&n=" + queryField('n');		
//url = "emailsearch.aspx?t="+ document.frmEmaillist2main.t.value +"&frm="+ document.frmEmaillist2main.frm.value +"&fn="+queryField('fn')+"&n=";
url = "../emailsearch.aspx?t=" + document.frmEmaillist2main.t.value + "&frm=" + document.frmEmaillist2main.frm.value + "&fn=" + queryField('fn') + "&n="+ queryField('n');
//alert(url);
//window.open(url+self.location,"TrueDaybook","width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
window.location.reload(url,"TrueDaybook","width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
}
//-->
</script>

</head>
<body>

  <form id="frmEmaillist2main" method="POST" runat="server">
   <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
  <input type="hidden" name="sortby" value="<%=sortBy%>" />
  <input type="hidden" name="alphabet" value="<%=alphabet%>" />
  <input type="hidden" name="pageno" value="<%=pageNo%>" />
  <input type="hidden" name="mt" value="" />
  <input type="hidden" name="fromSearch" id="fromSearch"  runat="server" />
  <input type="hidden" name="FirstName" id="FirstName"  runat="server" />
  <input type="hidden" name="LastName" id="LastName"  runat="server" />
  <input type="hidden" name="LoginName" id="LoginName"  runat="server" />
<%
if (Request.QueryString["frm"]!= "")
	Response.Write ("<input type='hidden'id='frm' name='frm' value='" + Request.QueryString["frm"] + "'>");
else
    Response.Write("<input type='hidden' id='frm' name='frm' value='" + Request.Form["frm"] + "'>");

%>
  <center>
  
  <h3><asp:Label ID="LblHeading" runat="server"></asp:Label></h3>
  
  <table width="800" border="0" cellpadding="2" cellspacing="4">
    <tr>
        <td align="center" style="width: 1168px">
            <asp:Label ID="LblError" runat="server" Text="" CssClass="lblError"></asp:Label>
        </td>
    </tr> 
    <tr>
<%
if (Request.QueryString ["srch"] != "y" ){
%>
      <td align="left" width="590">
      <%--Window Dressing--%>
        <font name="Verdana" size="2"  class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EmailList2main_StartWith %>" runat="server"></asp:Literal></font><%--FB 2579--%>
        <a href="" onclick="JavaScript: selname('ifrmList', 'A');return false;"><span class="tabtext" id="A">0-A</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'B');return false;"><span class="tabtext" id="B">B</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'C');return false;"><span class="tabtext" id="C">C</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'D');return false;"><span class="tabtext" id="D">D</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'E');return false;"><span class="tabtext" id="E">E</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'F');return false;"><span class="tabtext" id="F">F</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'G');return false;"><span class="tabtext" id="G">G</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'H');return false;"><span class="tabtext" id="H">H</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'I');return false;"><span class="tabtext" id="I">I</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'J');return false;"><span class="tabtext" id="J">J</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'K');return false;"><span class="tabtext" id="K">K</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'L');return false;"><span class="tabtext" id="L">L</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'M');return false;"><span class="tabtext" id="M">M</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'N');return false;"><span class="tabtext" id="N">N</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'O');return false;"><span class="tabtext" id="O">O</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'P');return false;"><span class="tabtext" id="P">P</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'Q');return false;"><span class="tabtext" id="Q">Q</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'R');return false;"><span class="tabtext" id="R">R</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'S');return false;"><span class="tabtext" id="S">S</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'T');return false;"><span class="tabtext" id="T">T</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'U');return false;"><span class="tabtext" id="U">U</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'V');return false;"><span class="tabtext" id="V">V</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'W');return false;"><span class="tabtext" id="W">W</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'X');return false;"><span class="tabtext" id="X">X</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'Y');return false;"><span class="tabtext" id="Y">Y</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'Z');return false;"><span class="tabtext" id="Z">Z-</span></a>
        <a href="" onclick="JavaScript: selname('ifrmList', 'ALL');return false;"><span class="tabtext" id="ALL"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, All%>" runat="server"></asp:Literal></span></a>
      </td>
      <td align="left" id="tabAll" width="20">
      </td>
      
<%
}
else 
%>
      <td align="left" width="590"></td>
<%

%>

      <!-- <td align="right" width="90"-->
      <!--Code Modified. Changed width to 20% from 90 For myVrmLookUP  on 20Mar09 - FB 412 � Start-->
      <%--Window Dressing--%>
      <td align="right" width="20%" class="blackblodtext">
<!--Code Modified. Changed width to 20% from 90 For myVrmLookUP  on 20Mar09 - FB 412 � End-->
        <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, GoTo %>" runat="server"></asp:Literal><select size="1" name="SelPage" onchange="selpage('ifrmList', this)" class="altText"></select> <%--Edited for FF--%><%--FB 2579--%>
      </td>
    </tr>
  </table>
  
    <table width="800" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="<%=titlealign%>">

<script type="text/javascript" >
<!--   

_d = document;
var mt = "", mt1 = "", mt2 = "";
var varFN = "<asp:Literal Text='<%$ Resources:WebResources, FirstName%>' runat='server'></asp:Literal>";
var varLN = "<asp:Literal Text='<%$ Resources:WebResources, LastName%>' runat='server'></asp:Literal>";
var varLoN = "<asp:Literal Text='<%$ Resources:WebResources, EmailSearch_LoginName%>' runat='server'></asp:Literal>";
var varEmail = "<asp:Literal Text='<%$ Resources:WebResources, Email%>' runat='server'></asp:Literal>";
var varSelected = "<asp:Literal Text='<%$ Resources:WebResources, Selected%>' runat='server'></asp:Literal>";
var varExternalAttendees = "<asp:Literal Text='<%$ Resources:WebResources, ExternalAttendees%>' runat='server'></asp:Literal>";
var varRoomAttendees = "<asp:Literal Text='<%$ Resources:WebResources, RoomAttendees%>' runat='server'></asp:Literal>";
var varNotify = "<asp:Literal Text='<%$ Resources:WebResources, NOTIFY_1%>' runat='server'></asp:Literal>";
var varAudio = "<asp:Literal Text='<%$ Resources:WebResources, Audio%>' runat='server'></asp:Literal>";
var varVideo = "<asp:Literal Text='<%$ Resources:WebResources, Video%>' runat='server'></asp:Literal>";


if (queryField("frm") == "party2")  
{
	mt +="   <thead><tr>";
//	<%--Window Dressing start--%>
	mt += "     <td align='center' width='90' class='tableHeader' height='20'>" + ((queryField("srch") == "y") ? varFN : "<a href='javascript:parent.seltype(\"ifrmList\", 1);'>" + varFN + "</a>") + "</td>";
	mt += "     <td align='center' width='100' class='tableHeader' height='20'>" + ((queryField("srch") == "y") ? varLN : "<a href='javascript:parent.seltype(\"ifrmList\", 2);'>" + varLN + "</a>") + "</td>";
	mt += "     <td align='center' width='90' class='tableHeader' height='20'>" + ((queryField("srch") == "y") ? varLoN : "<a href='javascript:parent.seltype(\"ifrmList\", 3);'>" + varLoN + "</a>") + "</td>";
	mt += "     <td align='center' width='190' class='tableHeader' height='20'>" + ((queryField("srch") == "y") ? varEmail : "<a href='javascript:parent.seltype(\"ifrmList\", 4);'>" + varEmail + "</a>") + "</td>";

	mt += "     <td align='center' width='40' class='tableHeader' height='20'>" + varSelected + "</td>";

	mt1 += "    <td align='center' width='40' class='tableHeader'>";
	mt1 += "      <a title='External Attendees have their own individual physical endpoints'>" + varExternalAttendees + "</a>";
	mt1 += "    </td>";
	mt1 += "    <td align='center' width='40' class='tableHeader'>";
	mt1 += "      <a title='Invitee participants are room-based participants.Each room is associated with only one physical endpoint'>Room Attendees</a>";
	mt1 += "    </td>";

	mt2 += "    <td align='center' width='40' class='tableHeader'>" + (isFuCreate ? varExternalAttendees : RoomAttendees) + "</td>";

	mt += ( (isFuCreate || isRm) ?  mt2 : mt1);
	
	mt +="     <td align='center' width='15' class='tableHeader'>CC</td>";
	mt += "     <td align='center' width='25' class='tableHeader'>" + varNotify + "</td>";
	mt += "     <td align='center' width='30' class='tableHeader'>" + varAudio + "</td>";
	mt += "     <td align='center' width='30' class='tableHeader'>" + varVideo + "</td>";
	mt +="   </tr></thead>";
//	<%--Window Dressing end--%>
}
//Code Added by Offshore FB issue no:412-Start
else if (queryField("frm") == "party2Aspx")  
{	
	mt +="   <thead><tr>";
	//Class Added,changed width on 27Mar08 start
	mt +="     <td align='center' width='110' class='tableHeader' height='20'>" + ( (queryField("srch") == "y") ? varFN : "<a href='javascript:parent.seltype(\"ifrmList\", 1);'>"+ varFN +"</a>") + "</td>";
	mt += "     <td align='center' width='110' class='tableHeader' height='20'>" + ((queryField("srch") == "y") ? varLN : "<a href='javascript:parent.seltype(\"ifrmList\", 2);'>" + varLN + "</a>") + "</td>";
	mt += "     <td align='center' width='200' class='tableHeader' height='20'>" + ((queryField("srch") == "y") ? varLoN : "<a href='javascript:parent.seltype(\"ifrmList\", 3);'>" + varLoN + "</a>") + "</td>";
	mt += "     <td align='center' width='300' class='tableHeader' height='20'>" + ((queryField("srch") == "y") ? varEmail : "<a href='javascript:parent.seltype(\"ifrmList\", 4);'>" + varEmail + "</a>") + "</td>";
    mt +="     <td align='center' width='100' height='20' class='tableHeader'>"+varSelected+"</td>";
	//Class Added,changed width on 27Mar08 End
//Code Commented For myVrmLookUP From templates on 20Mar09 - FB 412 - Start
	//mt1 += "    <td align='center' width='40'>";
    //mt1 += "      <a title='External Attendees have their own individual physical endpoints'>External Attendees</a>";
	//mt1 += "    </td>";
	//mt1 += "    <td align='center' width='40'>";
	//mt1 += "      <a title='Invitee participants are room-based participants.Each room is associated with only one physical endpoint'>Room Attendees</a>";
	//mt1 += "    </td>";
	
	//mt2 += "    <td align='center' width='40'>" + (isFuCreate ? "External Attendees" : "Room Attendees") + "</td>";

	//mt += ( (isFuCreate || isRm) ?  mt2 : mt1);
	
	//mt +="     <td align='center' width='15'>CC</td>";
	//mt +="     <td align='center' width='25'>NOTIFY</td>";
	//mt +="     <td align='center' width='30'>Audio</td>";
	//mt +="     <td align='center' width='30'>Video</td>";
	//mt +="   </tr></thead>";
//Code Commented For myVrmLookUP From templates on 20Mar09 - FB 412 - End		

}
//Code Added by Offshore FB issue no:412-End

else
    if  (queryField("frm") == "party2NET")
    {
        
        //Changed width  on 30Mar08 Start 
        mt += "   <thead><tr>";
        mt += "     <td align='left' width='110' height='20' class='tableHeader'>" + ((queryField("srch") == "y") ? varFN : "<a href='javascript:parent.seltype(\"ifrmList\", 1);'>" + varFN + "</a>") + "</td>";
        mt += "     <td align='left' width='110' height='20' class='tableHeader'>" + ((queryField("srch") == "y") ? varLN : "<a href='javascript:parent.seltype(\"ifrmList\", 2);'>" + varLN + "</a>") + "</td>";
        mt += "     <td align='left' width='200' height='20' class='tableHeader'>" + ((queryField("srch") == "y") ? varLoN : "<a href='javascript:parent.seltype(\"ifrmList\", 3);'>" + varLoN + "</a>") + "</td>";
        mt += "     <td align='left' width='300' height='20' class='tableHeader'>" + ((queryField("srch") == "y") ? varEmail : "<a href='javascript:parent.seltype(\"ifrmList\", 4);'>" + varEmail + "</a>") + "</td>";
        mt += "     <td align='left' width='100' height='20' class='tableHeader'>" + varSelected + "</td>";
        mt +="   </tr></thead>";
        //Changed width  on 30Mar08 End
    }
else 
{    
	mt +="   <thead><tr>";
	//Code Modified For myVrmLookUP From templates on 20Mar09- FB 412 - Start	
	mt +="     <td align='center' width='110' height='20' class='tableHeader'>" + ( (queryField("srch") == "y") ? varFN : "<a href='javascript:parent.seltype(\"ifrmList\", 1)'>"+ varFN +"</a></b>") + "</td>";
	//mt +="     <td align='center' width='110' height='20' class='tableHeader'>" + ( (queryField("srch") == "y") ? "First Name" : "<a href='javascript:seltype(\"ifrmList\", 1)'>First Name</a></b>") + "</td>";
	//Code Modified For myVrmLookUP From templates on 20Mar09- FB 412 - End	
	mt += "     <td align='center' width='110' height='20' class='tableHeader'>" + ((queryField("srch") == "y") ? varLN : "<a href='javascript:parent.seltype(\"ifrmList\", 2)'>" + varLN + "</a></b>") + "</td>";
	mt += "     <td align='center' width='200' height='20' class='tableHeader'>" + ((queryField("srch") == "y") ? varLoN : "<a href='javascript:parent.seltype(\"ifrmList\", 3)'>" + varLoN + "</a></b>") + "</td>";
	//Changed width Added on 30Mar08 Start 
	mt += "     <td align='center' width='300' class='tableHeader'>" + ((queryField("srch") == "y") ? varEmail : "<a href='javascript:parent.seltype(\"ifrmList\", 4)'>"+ varEmail +"</a></b>") + "</td>";
	mt +="     <td align='center' width='100' class='tableHeader'>"+ varSelected +"</td>";
	//mt +="     <td align='center' width='216' class='tableHeader'>" + ( (queryField("srch") == "y") ? "Email" : "<a href='javascript:parent.seltype(\"ifrmList\", 4)'>Email</a></b>") + "</td>";
   //mt +="     <td align='center' width='118' class='tableHeader'>Selected</td>";
   //Changed width Added on 30Mar08 End
	mt +="   </tr><thead>";
}
document.frmEmaillist2main.mt.value = mt;

//-->
</script>
        </td>
      </tr>
      
      <tr>  
        <td> <%--//FB 1779--%>
         <iframe src="emaillist2.aspx?wintype=ifr&audioid=<%=Request.QueryString["audioid"]%>&frm=<%=Request.QueryString["frm"]%>&sn=0&fn=<%=Request.QueryString["fn"]%>&n=<%=Request.QueryString["n"]%>&t=<%=Request.QueryString["t"]%>&srch=<%=Request.QueryString["srch"]%>&lname=<%=Request.QueryString["lname"]%>&fname=<%=Request.QueryString["fname"]%>&gname=<%=Request.QueryString["gname"]%>"
         name="ifrmList" id="ifrmList" width="100%" height="220" frameborder="0" align="left" valign="top" SCROLLING="auto" >
            <p><asp:Literal ID="Literal2" Text='<%$ Resources:WebResources, EmailList2main_goto%>' runat='server'></asp:Literal><a href="emaillist2.aspx?wintype=ifr">
            <asp:Literal ID="Literal3" Text='<%$ Resources:WebResources, EmailList2main_EmailList%>' runat='server'></asp:Literal></a></p>
         </iframe>
        </td>
      </tr>
    </table>

    <table cellpadding="4" cellspacing="6">
      <tr>
        <td height="3">
        </td>
      </tr>
      <tr>
      
<%
//ZDLatestIssue
if (Request.QueryString ["frm"] != "users" && Request.QueryString ["frm"] != "approver" && Request.QueryString ["frm"] != "approverNET" && Request.QueryString ["frm"] != "roomassist" && Request.QueryString["chk"] != "frmbridge"){
%>
        <td>
          <button name="EmaillistSubmit" class="altMedium0BlueButtonFormat" onclick="javascript:Reset();" ><asp:Literal ID="Literal4" Text='<%$ Resources:WebResources, Reset%>' runat='server'></asp:Literal></button>
        </td>
<%
}
%>
        <td>
            <Button ID="Emaillist2Submit" runat="server" class="altMedium0BlueButtonFormat" onserverclick="GoToSearch"><asp:Literal ID="Literal5" Text='<%$ Resources:WebResources, Search%>' runat='server'></asp:Literal></Button>
        </td>

<%
//ZDLatestIssue
if (Request.QueryString ["frm"] != "approver" && Request.QueryString ["frm"] != "approverNET" && Request.QueryString ["frm"] != "roomassist" && Request.QueryString["chk"] != "frmbridge")
	if (Request.QueryString ["t"] == "g" ){
%>
<%--FB 1985 - Starts--%>
<%if (Application["Client"].ToString().ToUpper() == "DISNEY")
  { %>
      <td align="right">
          <input type="button" name="Settings2Submit" value="<%$ Resources:WebResources, Return%>" runat="server" class="altMedium0BlueButtonFormat" onclick="viewVRM();" language="JavaScript" style="width:150px" />
        </td>
   <% }
  else
  { %>
        
        <td align="right">
          <input type="button" name="Settings2Submit" value="<%$ Resources:WebResources, ViewmyVRM%>" runat="server" class="altMedium0BlueButtonFormat" onclick="viewVRM();" language="JavaScript"  style="width:150px" />
        </td>
  <% } %>
<%--FB 1985 - End--%>
<%
}
%>
       
<% else if (Application["Client"] != "PNG" )%>

<%
{
%>

    <td align="right">
      <input type="button" name="Settings2Submit" value="<%$ Resources:WebResources, ViewGuest%>" runat="server" class="altMedium0BlueButtonFormat" onclick="viewGuest();" language="JavaScript" />
    </td>
        
<% 
}
%>
    <td>
    <%--code added for Soft Edge button--%>
      <button name="EmaillistSubmit" class="altMedium0BlueButtonFormat" onclick="JavaScript: window.close();" ><asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button>
    </td>
  </tr>
</table>
    
  </center>
  <input type="hidden" name="fn" value="<%=Request.QueryString["fn"] %>" />
  <input type="hidden" name="n" value="<%=Request.QueryString["n"] %>" />
  <input type="hidden" name="t" value="<%=Request.QueryString["t"] %>" />
  <input type="hidden" name="cmd" value="" />
</form>
<script type="text/javascript" language="JavaScript">
//<!-- FB 2596
//window.resizeTo (950, 450)
////-->
</script>

</body>

</html>
<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }
</script>
<%--ZD 100428 END--%>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>