<!--ZD 100147 Start-->
<!--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.
*
* You should have received a copy of the TrueDaybook license with
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/-->
<!--ZD 100147 ZD 100886 End-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<!--  #INCLUDE FILE="BrowserDetect.aspx"  -->
  <title>TrueDaybook</title>
  <meta name="Description" content="TrueDaybook (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="TrueDaybook, TrueDaybook, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="../en/Organizations/Org_11/CSS/Mirror/Styles/main.css" /> <%-- FB 1830--%>
  <script type="text/javascript" src="../script/errorList.js"></script>
</head>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">

