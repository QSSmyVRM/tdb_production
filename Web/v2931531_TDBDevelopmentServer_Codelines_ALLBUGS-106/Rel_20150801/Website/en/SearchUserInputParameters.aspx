<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.		
*
* You should have received a copy of the TrueDaybook license with	
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%><%--ZD 100147 ZD 100886 End--%>

<%@ Page Language="C#" Inherits="ns_SearchUserInputParameters.SearchUserInputParameters" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<%--FB 2779--%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<script type="text/javascript">
    function SelectAll(obj) {
        if (obj.tagName == "INPUT") {
            var chkstatus = "";
            chkstatus = obj.checked;
            var elements = document.getElementsByTagName('input');
            for (i = 0; i < elements.length; i++)
                if ((elements.item(i).id.indexOf("ChkSelect") >= 0)) {
                    if (elements.item(i).id != obj.id) {
                        elements.item(i).checked = chkstatus;
                    }
                }
        }
    }

    function CheckSelectAll() {
        var elements = document.getElementsByTagName('input');
        for (i = 0; i < elements.length; i++)
            if ((elements.item(i).id.indexOf("ChkSelectAll") >= 0)) {
                elements.item(i).checked = false;
            }
    }


    function getYourOwnEmailList(i) {

        url = "emaillist2main.aspx?t=e&frm=approverNET&fn=Setup&n=" + i;

        if (!window.winrtc) {
            winrtc = window.open(url, "", "width=950,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
        else if (!winrtc.closed) {
            winrtc.close();
            winrtc = window.open(url, "", "width=950,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
        else {
            winrtc = window.open(url, "", "width=950ss,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2" align="center" height="10px">
                    <h3>
                        <asp:Label ID="lblHeading" runat="server" Text="<%$ Resources:WebResources, SearchUser_UserSearch %>"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label><br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" id="tblFilter" runat="server" align="center">
                        <tr>
                            <td align="center">
                                <table width="60%" runat="server" id="tblSelecterUsr" align="center" border="0">
                                    <tr>
                                        <td align="left">
                                            <span class="blackblodtext">
                                                <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, SearchUser_SelectedUser%>"
                                                    runat="server"></asp:Literal></span>
                                            <asp:Label ID="SelectedUserlabel" runat="server" Text="" CssClass="blackblodtext"></asp:Label><br />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table width="60%" runat="server" id="tblUsrSelection" align="center" border="0">
                                    <tr>
                                        <td align="right">
                                            <span class="blackblodtext">
                                                <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, NewUser%>" runat="server"></asp:Literal></span>
                                            <asp:TextBox ID="txtApprover1" runat="server" CssClass="altText" Enabled="False"
                                                Style="width: 25%"></asp:TextBox>
                                            &nbsp; <a id="ImgApprover1" href="" onclick="this.childNodes[0].click();return false;"
                                                onmouseover="window.status='';return true;">
                                                <img id="Img1" onclick="javascript:getYourOwnEmailList(0);return false;" border="0"
                                                    src="image/edit.gif" alt="Edit" style="cursor: pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>" />
                                            </a><a href="javascript: deleteApprover(0);" onmouseover="window.status='';return true;">
                                                <img id="Img2" border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16"
                                                    style="cursor: pointer;" runat="server" title="<%$ Resources:WebResources, Delete%>" />
                                            </a>
                                            <asp:RequiredFieldValidator ID="AssistantValidator" runat="server" ControlToValidate="hdnApprover1"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                                ValidationGroup="SubmitUserSearch"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="hdnApprover1" runat="server" Width="0px" BackColor="White" BorderColor="White"
                                                BorderStyle="None" Style="display: none"></asp:TextBox>
                                            <input type="hidden" id="txtSortBy" runat="server" /><%--FB 2822--%>
                                            <input type="hidden" id="hdnSortingOrder" runat="server" value="0" /><%--FB 2822--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trConference" runat="server">
                <td align="center" colspan="2">
                    <asp:DataGrid ID="dgConferenceList" runat="server" ItemStyle-Height="40" AutoGenerateColumns="False"
                        CellPadding="0" OnItemDataBound="dgInventoryList_ItemDataBound" GridLines="None"
                        BorderColor="Black" BorderStyle="None" BorderWidth="1" ShowFooter="false" Width="90%"
                        Visible="true" AllowSorting="true" Style="border-collapse: collapse">
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:TemplateColumn HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <table id="tdID">
                                        <tr>
                                            <td>
                                                <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ConferenceList_tdID%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="btnSortID" runat="server" CommandArgument="1" OnCommand="SortGrid">
                                                    <%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT"))
                                                      { %>
                                                    <asp:Label ID="spnAscIDIE" runat="server" Style="font-family: Wingdings; font-weight: bolder;">&#233;</asp:Label>
                                                    <asp:Label ID="spnDescIDIE" runat="server" Style="font-family: Wingdings; font-weight: bolder;
                                                        display: none;">&#234;</asp:Label><% }
                                                      else
                                                      { %><asp:Label ID="spnAscID" runat="server" Style="font-family: Wingdings; font-weight: bolder;
    font-size: x-large;">&#8593;</asp:Label>
                                                    <asp:Label ID="spnDescID" runat="server" Style="font-family: Wingdings; font-weight: bolder;
                                                        display: none; font-size: x-large;">&#8595;</asp:Label><% } %>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span id="lblUniqueID" cssclass="altText" runat="server">
                                        <%#DataBinder.Eval(Container, "DataItem.ConfereceUniqueId")%></span>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderStyle-Width="30%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <table id="tdConfName">
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$ Resources:WebResources, ConferenceList_tdName%>" runat="server"></asp:Literal>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="btnSortConfName" runat="server" CommandArgument="2" OnCommand="SortGrid">
                                                    <%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT"))
                                                      { %>
                                                    <asp:Label ID="spnAscConfNameIE" runat="server" Style="font-family: Wingdings; font-weight: bolder;">&#233;</asp:Label>
                                                    <asp:Label ID="spnDescConfNameIE" runat="server" Style="font-family: Wingdings; font-weight: bolder;
                                                        display: none;">&#234;</asp:Label><% }
                                                      else
                                                      { %><asp:Label ID="spnAscConfName" runat="server" Style="font-family: Wingdings;
    font-weight: bolder; font-size: x-large;">&#8593;</asp:Label>
                                                    <asp:Label ID="spnDescConfName" runat="server" Style="font-family: Wingdings; font-weight: bolder;
                                                        display: none; font-size: x-large;">&#8595;</asp:Label><% } %>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span id="lblConfName" cssclass="altText" runat="server">
                                        <%#DataBinder.Eval(Container, "DataItem.ConferenceName") %></span>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <table id="tdOrgName">
                                        <tr>
                                            <td>
                                                <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, ConferenceList_tdOrgName%>"
                                                    runat="server"></asp:Literal>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="btnSortOrgName" runat="server" CommandArgument="3" OnCommand="SortGrid">
                                                    <%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT"))
                                                      { %>
                                                    <asp:Label ID="spnAscOrgNameIE" runat="server" Style="font-family: Wingdings; font-weight: bolder;">&#233;</asp:Label>
                                                    <asp:Label ID="spnDescOrgNameIE" runat="server" Style="font-family: Wingdings; font-weight: bolder;
                                                        display: none;">&#234;</asp:Label><% }
                                                      else
                                                      { %><asp:Label ID="spnAscOrgName" runat="server" Style="font-family: Wingdings; font-weight: bolder;
    font-size: x-large;">&#8593;</asp:Label>
                                                    <asp:Label ID="spnDescOrgName" runat="server" Style="font-family: Wingdings; font-weight: bolder;
                                                        display: none; font-size: x-large;">&#8595;</asp:Label><% } %>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span id="lblOrgName" cssclass="altText" runat="server">
                                        <%#DataBinder.Eval(Container, "DataItem.organizationName") %></span>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <table id="tdConfDate">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblDtTimeHeader" runat="server" Text="<%$ Resources:WebResources, ConferenceList_lblDtTimeHeader%>"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="btnSortConfDate" runat="server" CommandArgument="4" OnCommand="SortGrid">
                                                    <%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT"))
                                                      { %>
                                                    <asp:Label ID="spnAscConfDateIE" runat="server" Style="font-family: Wingdings; font-weight: bolder;">&#233;</asp:Label>
                                                    <asp:Label ID="spnDescConfDateIE" runat="server" Style="font-family: Wingdings; font-weight: bolder;
                                                        display: none">&#234;</asp:Label><% }
                                                      else
                                                      { %><asp:Label ID="spnAscConfDate" runat="server" Style="font-family: Wingdings;
    font-weight: bolder; font-size: x-large;">&#8593;</asp:Label>
                                                    <asp:Label ID="spnDescConfDate" runat="server" Style="font-family: Wingdings; font-weight: bolder;
                                                        display: none; font-size: x-large;">&#8595;</asp:Label><% } %>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span id="lblConfDate" cssclass="altText" runat="server" visible='<%# !DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>'>
                                        <%#DataBinder.Eval(Container, "DataItem.ConferenceDateTime") %></span>
                                    <asp:LinkButton OnClientClick="javascript:return fnShowHideInstance(this.className)"
                                        CssClass='<%# DataBinder.Eval(Container, "DataItem.Id") %>' runat="server" Text="<%$ Resources:WebResources, ConferenceList_btnGetInstances%>"
                                        Visible='<%# DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="isOBTP" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Width="10%" DataField="ConferenceType" ItemStyle-CssClass="tableBody"
                                HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ConferenceList_Type%>">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ConferenceSetup_SelectAll%>"
                                        runat="server"></asp:Literal><br />
                                    <asp:CheckBox runat="server" ID="ChkSelectAll" onclick="javascript:SelectAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="ChkSelect" onclick="javascript:CheckSelectAll(this);" /></ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <tr>
                                        <td colspan="100%">
                                            <div id="div<%# Eval("Id") %>" style="position: relative; overflow: auto; width: 100%;
                                                display: none">
                                                <asp:DataGrid ID="dgInstanceList" ShowHeader="false" runat="server" AutoGenerateColumns="False"
                                                    CellPadding="0" OnItemDataBound="dgInventoryList_ItemDataBound" GridLines="None"
                                                    BorderColor="Black" BorderStyle="solid" BorderWidth="1" ShowFooter="false" Width="100%"
                                                    Visible="true" Style="border-collapse: separate">
                                                    <SelectedItemStyle CssClass="tableBody" />
                                                    <EditItemStyle CssClass="tableBody" />
                                                    <AlternatingItemStyle CssClass="tableBody" />
                                                    <ItemStyle CssClass="tableBody" />
                                                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                                                    <Columns>
                                                        <asp:BoundColumn ItemStyle-Width="10%" DataField="ConfereceUniqueId" ItemStyle-CssClass="tableBody"
                                                            HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                        <asp:BoundColumn ItemStyle-Width="30%" DataField="ConferenceName" ItemStyle-CssClass="tableBody"
                                                            HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                        <asp:BoundColumn ItemStyle-Width="20%" DataField="organizationName" ItemStyle-CssClass="tableBody"
                                                            HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                        <asp:BoundColumn ItemStyle-Width="20%" DataField="ConferenceDateTime" ItemStyle-CssClass="tableBody"
                                                            HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                        <asp:BoundColumn ItemStyle-Width="20%" DataField="ConferenceType" ItemStyle-CssClass="tableBody"
                                                            HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="isOBTP" Visible="false"></asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr id="trRooms" runat="server">
                <td align="center" colspan="2">
                    <asp:DataGrid ID="dgUserRooms" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        OnItemDataBound="dgInventoryList_ItemDataBound" GridLines="None" BorderColor="blue"
                        BorderStyle="solid" BorderWidth="1" ShowFooter="false" Width="90%" Visible="true"
                        Style="border-collapse: separate">
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="Tier1" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Tier1%>">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Tier2" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Tier2%>">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RoomName" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, RoomName%>">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Role" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Role%>">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderStyle-Width="30%" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ConferenceSetup_SelectAll%>"
                                        runat="server"></asp:Literal><br />
                                    <asp:CheckBox runat="server" ID="ChkSelectAll" onclick="javascript:SelectAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="ChkSelect" onclick="javascript:CheckSelectAll(this);" /></ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr id="trMCU" runat="server">
                <td align="center" colspan="2">
                    <asp:DataGrid ID="dgUserMCUs" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        OnItemDataBound="dgInventoryList_ItemDataBound" GridLines="None" BorderColor="blue"
                        BorderStyle="solid" BorderWidth="1" ShowFooter="false" Width="90%" Visible="true"
                        Style="border-collapse: separate">
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="MCUName" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Name%>">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="InterfaceType" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ResponseConference_InterfaceType%>">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Status" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ManageConference_Status%>">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Role" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Role%>">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderStyle-Width="30%" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ConferenceSetup_SelectAll%>"
                                        runat="server"></asp:Literal><br />
                                    <asp:CheckBox runat="server" ID="ChkSelectAll" onclick="javascript:SelectAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="ChkSelect" onclick="javascript:CheckSelectAll(this);" /></ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr id="trInventory" runat="server">
                <td align="center" colspan="2">
                    <asp:DataGrid ID="dgInventoryList" ShowFooter="false" AutoGenerateColumns="false"
                        Width="60%" runat="server" ItemStyle-BorderColor="Gray" OnItemDataBound="dgInventoryList_ItemDataBound"
                        ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1" BorderStyle="Solid" BorderColor="Gray"
                        BorderWidth="1" GridLines="None">
                        <HeaderStyle CssClass="tableHeader" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" />
                        <Columns>
                            <asp:BoundColumn HeaderText="<%$ Resources:WebResources, Name%>" DataField="IName"
                                HeaderStyle-Width="30%"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="<%$ Resources:WebResources, EditInventory_Adminincharge%>"
                                DataField="Incharge"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ConferenceSetup_SelectAll%>"
                                        runat="server"></asp:Literal><br />
                                    <asp:CheckBox runat="server" ID="ChkSelectAll" onclick="javascript:SelectAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="ChkSelect" onclick="javascript:CheckSelectAll(this);" /></ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr id="trWorkOrder" runat="server">
                <td align="center">
                    <asp:DataGrid ID="dgWorkOrder" ShowFooter="false" AutoGenerateColumns="false" Width="70%"
                        runat="server" ItemStyle-BorderColor="Gray" OnItemDataBound="dgInventoryList_ItemDataBound"
                        ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1" BorderStyle="Solid" BorderColor="Gray"
                        BorderWidth="1" GridLines="None">
                        <HeaderStyle CssClass="tableHeader" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" />
                        <Columns>
                            <asp:BoundColumn HeaderText="<%$ Resources:WebResources, Name%>" DataField="IName"
                                HeaderStyle-Width="30%"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="<%$ Resources:WebResources, CompletionDateTime%>" DataField="EndDate">
                            </asp:BoundColumn>
                            <asp:BoundColumn HeaderText="<%$ Resources:WebResources, EditInventory_Adminincharge%>"
                                DataField="Incharge"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ConferenceSetup_SelectAll%>"
                                        runat="server"></asp:Literal><br />
                                    <asp:CheckBox runat="server" ID="ChkSelectAll" onclick="javascript:SelectAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="ChkSelect" onclick="javascript:CheckSelectAll(this);" /></ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Table runat="server" ID="tblNoRecords" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                <asp:Label ID="lblNoItems" runat="server" Text="<%$ Resources:WebResources, logList_lblNoRecords%>"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <%--ZD 101525 Start--%>
            <tr>
                <td align="center">
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="tc1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue"
                                CssClass="subtitleblueblodtext" runat="server">
                                <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, ManageUser_Pages%>"
                                    runat="server"></asp:Literal>
                            </asp:TableCell>
                            <asp:TableCell ID="tc2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <%--ZD 101525 End--%>
        </table>
    </div>
    <br />
    <div style="float: none">
        <table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td align="center" colspan="2">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <button type="button" id="btnCancel" runat="server" class="altLongYellowButtonFormat"
                                    onserverclick="GoBack" style="width: 100px">
                                    <asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_btnGoBack%>" runat="server"></asp:Literal></button>&nbsp;&nbsp;
                                <asp:Button ID="btnDelete" runat="server" class="altLongYellowButtonFormat" OnClick="ReassignUser"
                                    Width="170px" Text="<%$ Resources:WebResources, SearchUser_btnDelete%>" OnClientClick="javascript:return fnCheckUser('1');" />
                                &nbsp;&nbsp;
                                <asp:Button ID="btnReassign" runat="server" Text="<%$ Resources:WebResources, SearchUser_btnReassigned%>"
                                    CssClass="altLongYellowButtonFormat" OnClick="ReassignUser" ValidationGroup="SubmitUserSearch"
                                    style="width: 200px" OnClientClick="javascript:return fnCheckUser('2');" />
                            </td>
                        </tr>
                    </table>
                    <br />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<script language="javascript">

    function deleteApprover(id) {
        eval("document.getElementById('hdnApprover" + (id + 1) + "')").value = "";
        eval("document.getElementById('txtApprover" + (id + 1) + "')").value = "";
    }

    function ChangeSortingOrder() {

        if (document.getElementById('txtSortBy').value == "1")
            document.getElementById('tdID').style.textDecoration = "underline";

        if (document.getElementById('txtSortBy').value == "2")
            document.getElementById('tdConfName').style.textDecoration = "underline";

        if (document.getElementById('txtSortBy').value == "3")
            document.getElementById('tdOrgName').style.textDecoration = "underline";

        if (document.getElementById('txtSortBy').value == "4")
            document.getElementById('tdConfDate').style.textDecoration = "underline";

        if (navigator.userAgent.indexOf('Trident') > -1) {
            if (document.getElementById('hdnSortingOrder').value == 0) {
                if (document.getElementById("dgConferenceList_ctl01_spnDescIDIE") != null) {
                    document.getElementById("dgConferenceList_ctl01_spnDescIDIE").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnDescConfNameIE").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnDescOrgNameIE").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnDescConfDateIE").style.display = 'none';

                    document.getElementById("dgConferenceList_ctl01_spnAscIDIE").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnAscConfNameIE").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnAscOrgNameIE").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnAscConfDateIE").style.display = 'block'

                    document.getElementById('hdnSortingOrder').value = "1";
                }
            }
            else {
                if (document.getElementById("dgConferenceList_ctl01_spnAscIDIE") != null) {
                    document.getElementById("dgConferenceList_ctl01_spnAscIDIE").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnAscConfNameIE").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnAscOrgNameIE").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnAscConfDateIE").style.display = 'none'

                    document.getElementById("dgConferenceList_ctl01_spnDescIDIE").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnDescConfNameIE").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnDescOrgNameIE").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnDescConfDateIE").style.display = 'block';

                    document.getElementById('hdnSortingOrder').value = "0";
                }
            }
        }
        else {
            if (document.getElementById('hdnSortingOrder').value == 0) {
                if (document.getElementById("dgConferenceList_ctl01_spnDescID") != null) {
                    document.getElementById("dgConferenceList_ctl01_spnDescID").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnDescConfName").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnDescOrgName").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnDescConfDate").style.display = 'none';

                    document.getElementById("dgConferenceList_ctl01_spnAscID").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnAscConfName").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnAscOrgName").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnAscConfDate").style.display = 'block'
                    document.getElementById('hdnSortingOrder').value = "1";
                }
            }
            else {
                if (document.getElementById("dgConferenceList_ctl01_spnAscID") != null) {
                    document.getElementById("dgConferenceList_ctl01_spnAscID").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnAscConfName").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnAscOrgName").style.display = 'none';
                    document.getElementById("dgConferenceList_ctl01_spnAscConfDate").style.display = 'none'

                    document.getElementById("dgConferenceList_ctl01_spnDescID").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnDescConfName").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnDescOrgName").style.display = 'block';
                    document.getElementById("dgConferenceList_ctl01_spnDescConfDate").style.display = 'block';
                    document.getElementById('hdnSortingOrder').value = "0";
                }
            }
        }
    }

    ChangeSortingOrder();
    function fnCheckUser(arg) {

        if (arg == 2) {
            if (document.getElementById("errLabel") != null) document.getElementById("errLabel").innerHTML = "";
            if (!Page_ClientValidate())
                return Page_IsValid;
        }

        var userId = document.getElementById("txtApprover1");

        if (arg == 1 && userId != null && userId.value == "") {
            if (document.getElementById("errLabel") != null) {
                document.getElementById("errLabel").style.display = "";
                document.getElementById("errLabel").style.color = "red";
                document.getElementById("errLabel").innerHTML = "A user must be assigned to the deleted conferences.";
            }
            return false;
        }

        if (document.getElementById("errLabel") != null) document.getElementById("errLabel").innerHTML = "";

        var rowSelected = false;
        var elements = document.getElementsByTagName('input');
        for (i = 0; i < elements.length; i++)
            if ((elements.item(i).id.indexOf("ChkSelect") >= 0)) {
                if (elements.item(i).checked == true) {
                    rowSelected = true;
                    break;
                }
            }

        if (rowSelected == false) {
            if (document.getElementById("errLabel") != null) {
                document.getElementById("errLabel").style.display = "";
                document.getElementById("errLabel").style.color = "red";
                if (arg == 1) {
                    document.getElementById("errLabel").innerHTML = RSDelRec; //ZD 101714
                }
                if (arg == 2) {
                    document.getElementById("errLabel").innerHTML = RSReassRec; //ZD 101714
                }
            }
            return false;
        }

        return true;
    }

    function fnShowHideInstance(elemId) {
        var div = document.getElementById('div' + elemId);

        if (div.style.display == "none") {
            div.style.display = "block";
        }
        else {
            div.style.display = "none";
        }

        return false;
    }
    //ZD 101525 - End
    if (document.getElementById('tc1') != null)//ZD 101525
        document.getElementById('tc1').style.display = 'none';

    if (document.getElementById('btnCancel') != null)
        document.getElementById('btnCancel').setAttribute("onblur", "document.getElementById('btnDelete').focus(); document.getElementById('btnDelete').setAttribute('onfocus', '');");

    if (document.getElementById('btnDelete') != null)
        document.getElementById('btnDelete').setAttribute("onblur", "document.getElementById('btnReassign').focus(); document.getElementById('btnReassign').setAttribute('onfocus', '');");


    document.onkeydown = function (evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

</script>
<script type="text/javascript" src="inc/softedge.js"></script>
