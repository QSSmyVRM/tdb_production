<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 TrueDaybook - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.	
*
* You should have received a copy of the TrueDaybook license with	
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.Template" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>


<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/script/managemcuorder.js"></script>

<script language="javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
	function ManageOrder ()
	{
		change_mcu_order_prompt('image/pen.gif', ManageTemplateOrder, document.frmManagebridge.Bridges.value, "Templates");
	}
	function CreateNewConference(confid)
    {
//        window.location.href = "aspToAspNet.asp?tp=ConferenceSetup.aspx&t=t&confid=" + confid; //Login Management
          window.location.href = "ConferenceSetup.aspx&t=t&confid=" + confid;
    }
	function showTemplateDetails(tid)
	{
//		popwin = window.open("dispatcher/conferencedispatcher.asp?cmd=GetTemplate&f=td&tid=" + tid,'templatedetails','status=yes,width=750,height=400,scrollbars=yes,resizable=yes')
        popwin = window.open("TemplateDetails.aspx?nt=1&f=td&tid=" + tid,'templatedetails','status=yes,width=750,height=400,scrollbars=yes,resizable=yes') //Login Management
		if (popwin)
			popwin.focus();
		else
			alert(EN_132);
	}
	function frmsubmit(save, order)
	{
	    document.getElementById("__EVENTTARGET").value="ManageOrder";
	    document.frmManagebridge.submit();
	}
	//ZD 100176 start
	function DataLoading(val) {
	    if (val == "1")
	        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
	    else
	        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
	}
	//ZD 100176 End
</script>
<head runat="server">
 
    <title>My Templates</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
      <input type="hidden" id="helpPage" value="73">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label id="lblHeader" runat="server" text="<%$ Resources:WebResources, ManageTemplate_lblHeader%>"></asp:Label><!-- FB 2570 -->
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV" style="display:none" align="center" >
                <img border='0' src='image/wait1.gif'  alt='Loading..' />
            </div> <%--ZD 100678 End--%>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate_ExistingTempla%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgTemplates" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="true" OnSortCommand="SortTemplates"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteTemplate" OnEditCommand="EditTemplate" OnCancelCommand="CreateConference" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                        <SelectedItemStyle  CssClass="tableBody"/>
                         <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                         <%--Window Dressing--%>
                        <FooterStyle CssClass="tableBody" />
                        <Columns><%--ZD 100425 Starts--%>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="name" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Name%>" SortExpression="1"></asp:BoundColumn> <%-- FB 2050 --%> <%--FB 2922--%>
                            <asp:BoundColumn DataField="description" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ManageGroup2_Description%>"></asp:BoundColumn> <%-- FB 2050 --%>  <%--FB 2922--%>
                            <asp:BoundColumn DataField="administrator" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ManageGroup_Owner%>" SortExpression="2"></asp:BoundColumn> <%-- FB 2050 --%>  <%--FB 2922--%>
                            <asp:BoundColumn DataField="public" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, ManageGroup_PrivatePublic%>" SortExpression="3" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>  <%--FB 2922--%>
                            <%--ZD 100425 End--%>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ViewDetails%>" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center"> <%--FB 2922--%>
                                <ItemTemplate>
                                    <asp:Button ID="btnViewDetails" Text="<%$ Resources:WebResources, ManageTemplate_btnViewDetails%>" runat="server" CssClass="altMedium0BlueButtonFormat" Width="42%"/> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" ItemStyle-CssClass="tableBody"  ItemStyle-Width="25%" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="center">   <%--FB 2922--%>
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td style="width:145px";><%--ZD 100425--%>
                                                <%--Code Changed for FB 1428--%>
                                                <asp:LinkButton runat="server" ID="btnCreateConf" CommandName="Cancel"  OnClientClick="DataLoading(1)">
                                                    <span id="Field1"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate_Field1%>" runat="server"></asp:Literal></span>
                                                </asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="<%$ Resources:WebResources, ManageTemplate_btnEdit%>" ID="btnEdit" OnClientClick="DataLoading(1)" CommandName="Edit"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="<%$ Resources:WebResources, ManageTemplate_btnDelete%>" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate_TotalTemplates%>" runat="server"></asp:Literal></span><asp:Label id="lblTotalRecords" runat="server" text=""></asp:Label> <%-- FB 2579 --%>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoTemplates" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageTemplate_TotalTemplates%>" runat="server"></asp:Literal>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td align="center">
                                <%--<asp:Button ID="btnManageOrder" runat="server" OnClientClick="javascript:ManageOrder();return false;" Text="Manage Template Order" CssClass="altLongBlueButtonFormat" />--%><%--ZD 100420--%>
                                <button ID="btnManageOrder" style="width:250px;" runat="server" Class="altLongBlueButtonFormat" OnClick="javascript:ManageOrder();return false;">
								<asp:Literal Text="<%$ Resources:WebResources, ManageTemplate_btnManageOrder%>" runat="server"></asp:Literal></button> <%--ZD 100420--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate_SearchTemplate%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                           <%--Removed Class for Window Dressing                      --%>
                            <td class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate_Entersearchcr%>" runat="server"></asp:Literal>  
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%"> 
                        <tr>
                            <td align="center">
                                <table width="100%">
                                    <tr>
                                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate_TemplateName%>" runat="server"></asp:Literal></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSTemplateName" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtSTemplateName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%if(!(Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))){%> <%--Added for FB 1425 MOJ--%>
                                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate_IncludedPartic%>" runat="server"></asp:Literal></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSParticipant" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSParticipant" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ /  ? | ^ = ! ` [ ] { } # $ @ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                        </td> 
                                        <%}%> <%--Added for FB 1425 MOJ--%>                                      
                                    </tr>
                                    <tr>
                                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageTemplate_Description%>" runat="server"></asp:Literal></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSDescription" TextMode="multiline" Rows="2" Width="200" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtSDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <%--<asp:Button ID="btnSearch" OnClick="SearchTemplate" runat="server" CssClass="altLongBlueButtonFormat" Text="Submit"  OnClientClick="DataLoading(1)"/> <%--ZD 100176--%> <%--ZD 100420--%>
                                <button ID="btnSearch"  runat="server" Class="altLongBlueButtonFormat" onserverclick="SearchTemplate" onClientClick="DataLoading(1)">
								<asp:Literal Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button><%--ZD 100420--%>
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <!--<SPAN class=subtitleblueblodtext></SPAN>--><%--Commented for FB 2094--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
               <td align="center">
                    <table width="90%">
                        <tr>
                            <td align="right">
                               <%--<asp:Button ID="btnCreate" OnClick="CreateNewTemplate" runat="server" CssClass="altLongBlueButtonFormat" Text="Create New Template" OnClientClick="DataLoading(1)" /><%--FB 2094--%><%-- ZD 100176--%><%--ZD 100420--%>
                               <button ID="btnCreate" runat="server"  Class="altLongBlueButtonFormat" OnClientClick="DataLoading(1)"  Onserverclick="CreateNewTemplate">
							<asp:Literal Text="<%$ Resources:WebResources, ManageTemplate_btnCreate%>" runat="server"></asp:Literal>			</button><%--ZD 100420--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:TextBox ID="Bridges" runat="server" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>
    <asp:TextBox ID="txtSortBy" runat="server" Visible="false"></asp:TextBox>
</form>
<script language="javascript">

    //ZD 100420 Start //ZD 100369
    if (document.getElementById('btnManageOrder') != null)
        document.getElementById('btnManageOrder').setAttribute("onblur", "fnManageOrderFocus();");
    //ZD 100369
    function EscClosePopup() {
            cancelthis();
    }
    //ZD 100420 End

</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--ZD 100420 start--%>
<script type="text/javascript">
    if (document.getElementById('btnDelete') != null)
        document.getElementById('btnDelete').setAttribute("onblur", "document.getElementById('btnManageOrder').focus(); document.getElementById('btnManageOrder').setAttribute('onfocus', '');");               

</script>
<%--ZD 100420 End--%>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

