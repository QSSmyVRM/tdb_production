/*ZD 100147 Start*/
/* Copyright (C) 2015 TrueDaybook - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the TrueDaybook license.
*
* You should have received a copy of the TrueDaybook license with
* this file. If not, please write to: sales@TrueDaybook.com, or visit: www.TrueDaybook.com
*/
/*ZD 100147 ZD 100886 End*/
function save_in_group_prompt(promptpicture, prompttitle) 
{ 
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	w = 300;
	promptbox.position = 'absolute';
	promptbox.backgroundColor = '#E1E1E1';//ZD 100426
	document.getElementById('prompt').style.top = mousedownY-50 + 'px'; // FB 2050
	document.getElementById('prompt').style.left = mousedownX + 'px'; // FB 2050
//	document.getElementById('prompt').style.top = mousedownY; 
//	document.getElementById('prompt').style.left = mousedownX - w;
	
	promptbox.width = w;
	promptbox.border = 'outset 1 #bbbbbb'; 

    //Window Dressing - start
	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='tableHeader'><img src='" + promptpicture + "' height='18' width='18' alt='prompt'></td><td class='tableHeader'>" + prompttitle + "</td></tr></table>" //ZD 100419
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='tableBody'><tr><td id='sigdlgcontent'>";
    //Window Dressing - end
	
	m += "<table border=0>";
	m += "  <tr><td class='blackblodtext'>";
	m += "Seuls les caract&#232;res alphanum&#233;riques sont autoris&#233;s.";
	m += "  </td></tr>";
	m += "  <tr><td class='blackblodtext'>";
	m += "    Nom du groupe <input type='text' name='gname' tabindex='1' id='gname' class='altText' value='' style='width: 80pt'>";
	m += "  </td></tr>"
	m += "  <tr><td class='blackblodtext'>";
	m += "    Groupe priv&#233; <input type='checkbox' tabindex='2' name='gprivate' id='gprivate' value='1' checked>";
	m += "  </td></tr>"
	m += "  <tr><td align='right'>"
	//-code changed for Softedge button
//	m += "    <input type='button' name ='Cancel' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
//	m += "    <input type='button' name = 'submit' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveGroup(" + "document.getElementById(\"gname\").value" + "," + "document.getElementById(\"gprivate\").checked" + ");'>"
	m += "    <button class='altMedium0BlueButtonFormat' tabindex='3' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>Annuler</button>"
	m += "    <button id='btnGroupSubmit' class='altMedium0BlueButtonFormat' tabindex='4' onClick='saveGroup(" + "document.getElementById(\"gname\").value" + "," + "document.getElementById(\"gprivate\").checked" + ");'>Soumettre</button>"
	m += "  </td></tr>"
	m += "</table>" 

	m += "</td></tr></table>"

	document.getElementById('prompt').innerHTML = m;
	document.getElementById("gname").focus();
	if (document.getElementById('gname') != null)
	    document.getElementById('gname').setAttribute("onblur", "document.getElementById('gprivate').focus(); document.getElementById('gprivate').setAttribute('onfocus', '');");
	if (document.getElementById('btnGroupSubmit') != null)
	    document.getElementById('btnGroupSubmit').setAttribute("onblur", "document.getElementById('gname').focus(); document.getElementById('gname').setAttribute('onfocus', '');");

} 


function saveGroup(gname, gprivate) 
{
	if (Trim(gname) == "") {
		alert(EN_10);
		document.getElementById("gname").focus();
		return false;
	}

	if (ifrmSaveingroup == null) {
		alert("Erreur�: ne peut pas prendre en charge la mise en ligne de fichiers maintenant. Veuillez informer l''administrateur � ce sujet.");
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
		return -1;
	}
	
	var gID;
	var grpExists = false;
	if (document.location.href.indexOf(".aspx") < 0)
	    for(var i=0; i< document.frmSettings2.Group.options.length; i++)
	    {
			    if (gname.toLowerCase() == document.frmSettings2.Group.options(i).text.toLowerCase())
			    {	
				    grpExists = true;
				    gID = document.frmSettings2.Group.options(i).value;
			    }
	    }
	else
	    for(var i=0; i < document.frmSettings2.Group.options.length; i++)
	    {
			    if (gname.toLowerCase() == document.frmSettings2.Group.options[i].text.toLowerCase())
			    {	
				    grpExists = true;
				    gID = document.frmSettings2.Group.options[i].value;
			    }
	    }
    allValid = isAlphanumeric(gname);
	if (grpExists == true) //if group already exists
	{
		alert("Le groupe existe d&#233;j�. Veuillez saisir un autre nom.");
		return false;
	}
	if (allValid == true) {
		ifrmSaveingroup.document.frmSaveingroup.GroupName.value = gname;
		ifrmSaveingroup.document.frmSaveingroup.GroupPrivate.value = (gprivate ? 1 : 0);
		ifrmSaveingroup.document.frmSaveingroup.GroupPublic.value = (gprivate ? "" : 1); //Code Added  for Aspx Conversion
		if (document.location.href.indexOf(".aspx") < 0)
		    ifrmSaveingroup.document.frmSaveingroup.PartysInfo.value = document.frmSettings2.PartysInfo.value;
		else
		    ifrmSaveingroup.document.frmSaveingroup.PartysInfo.value = document.frmSettings2.txtPartysInfo.value;
		ifrmSaveingroup.document.frmSaveingroup.submit(); 
	}
	else 
	{
		alert("Seuls les caract&#232;res alphanum&#233;riques sont autoris&#233;s pour les noms de groupe.");
		return false; 
	}

} 


function saveGroupSucc() 
{
	m = "<table width=100%>"
	m += "  <tr><td class='blackblodtext'>Enregistrer dans le groupe : <span class=succdonetxt>R&#233;ussi!</span></td></tr>"
	m += "  <tr><td class='blackblodtext'><i>[ Un nouveau groupe s''affiche apr&#232;s la soumission de cette page. ]</i></td></tr>"
	m += "  <tr><td align='right'>"
	//-code changed for Softedge button
//	m += "    <input type='button' class='prompt' value='Close' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
	m += "    <button id='btnClose1' class='altMedium0BlueButtonFormat' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>Fermer</button>s"
	m += "  </td></tr>"
	m += "</table>"
	document.getElementById('sigdlgcontent').innerHTML = m;
	document.getElementById('btnClose1').focus();
} 


function saveGroupFail(em) 
{
	m = "<table width=100%>"
	m += "  <tr><td class='blackblodtext'>Enregistrer dans le groupe : <span class=faildonetxt>&#201;chec!</span></td></tr>"
	m += "  <tr><td class='faildonetxt'>Erreur: <u><b>" + em + "</b></u></td></tr>"
	m += "  <tr><td class='blackblodtext'> Tous les participants d''un groupe doivent avoir des adresses e-mail uniques.</td></tr>"
	m += "  <tr><td class='blackblodtext'>Veuillez essayer de nouveau ou en informer l''administrateur.</td></tr>"
	m += "  <tr><td align='right'>"
	//Window Dressing
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='Fermer' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
	m += "  </td></tr>"
	m += "</table>"

	document.getElementById('sigdlgcontent').innerHTML = m;
	promptbox = document.getElementById('prompt'); 
	promptbox.style.width = 300;
	//Code commented by Offshore FB issue no:412-Start
	    //ifrmSaveingroup.document.frmSaveingroup.submit();
	//Code commented by Offshore FB issue no:412-End
} 
